# SET FOREIGN_KEY_CHECKS = 0;
# TRUNCATE new_tickets_notification;
# TRUNCATE attached_file_ticket;
# TRUNCATE comment_file;
# TRUNCATE comment;
# TRUNCATE tickets;
# TRUNCATE project;
# TRUNCATE `user`;
# TRUNCATE difficulties;
# TRUNCATE status;
# TRUNCATE critical_levels;
# TRUNCATE categories;
# TRUNCATE project_status;
# TRUNCATE project_role;
# TRUNCATE system_level;
# TRUNCATE `rank`;
# SET FOREIGN_KEY_CHECKS = 1;
#
# INSERT INTO difficulties (name) VALUES ('hard'), ('medium'), ('easy');
INSERT INTO status (name) VALUES ('open'), ('unassigned'), ('in progress'), ('closed');
# INSERT INTO critical_levels (name) VALUES ('urgent'), ('high'), ('normal'), ('low');
# INSERT INTO categories (name) VALUES ('layout'), ('function'), ('login'), ('others');
# INSERT INTO project_status (name) VALUES ('development'), ('staging'), ('production');
INSERT INTO project_role (name) VALUES ('owner'),('project manager'),('senior developer'),('junior developer');
INSERT INTO system_level (name) VALUES ('admin'), ('manager'), ('staff');
INSERT INTO `rank` (name) VALUES ('manager'), ('supervisor'), ('senior'), ('junior');
# INSERT INTO user (username, name, password, system_level_id, rank_id) VALUES ('admin', 'Admin', 'password', 3, 4);
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE new_tickets_notification;
TRUNCATE attached_file_ticket;
TRUNCATE comment_file;
TRUNCATE comment;
TRUNCATE tickets;
TRUNCATE project;
TRUNCATE `user`;
TRUNCATE difficulties;
TRUNCATE status;
TRUNCATE critical_levels;
TRUNCATE categories;
TRUNCATE project_status;
TRUNCATE project_role;
TRUNCATE system_level;
TRUNCATE project_user;
TRUNCATE `rank`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO difficulties (name) VALUES ('easy');
# INSERT INTO status (name) VALUES ('open');
INSERT INTO status (name) VALUES ('open'), ('unassigned'), ('in progress'), ('closed');
INSERT INTO critical_levels (name) VALUES ('urgent');
INSERT INTO categories (name) VALUES ('login');
INSERT INTO project_status (name) VALUES ('development'), ('staging'), ('production');
# INSERT INTO project_role (name) VALUES ('owner'),('senior'),('junior developer');
# INSERT INTO system_level (name) VALUES ('manager');
# INSERT INTO `rank` (name) VALUES ('senior');
INSERT INTO project_role (name) VALUES ('owner'),('project manager'),('senior developer'),('junior developer');
INSERT INTO system_level (name) VALUES ('admin'), ('manager'), ('staff');
INSERT INTO `rank` (name) VALUES ('manager'), ('supervisor'), ('senior'), ('junior');

INSERT INTO user (username, name, password, system_level_id, rank_id) VALUES ('testUserName', 'abcName', 'testPassword', 1, 1);
INSERT INTO user (username, name, password, system_level_id, rank_id) VALUES ('number2', 'i am 2', 'number2', 1, 1);
INSERT INTO user (username, name, password, system_level_id, rank_id) VALUES ('number3', 'i am 3', 'number3', 1, 1);
INSERT INTO user (username, name, password, system_level_id, rank_id) VALUES ('admin', 'Admin', 'password', 3, 4);


INSERT INTO project (user_id, project_name, description, status_id, created_at, updated_at) VALUES (1, 'testPJNAME', 'TPD', 1, '2020-11-11','2020-11-11');
INSERT INTO project (user_id, project_name, description, status_id, created_at, updated_at) VALUES (1, 'project 2', 'project 2 description', 2, '2020-11-11','2020-11-11');

INSERT INTO project_user (user_id, project_id, role_id) VALUES (1,1,1);
INSERT INTO project_user (user_id, project_id, role_id) VALUES (2,1,2);

INSERT INTO tickets (user_id, project_id, title, description, status_id, due_date, ticket_assign_to_id, ticket_assign_time, category_id, critical_level_id, ticket_assigned_by_id) VALUES (1,1,'this is title', 'this is description', 1, '2020-11-11', 1, '2020-11-11', 1, 1, 1);
INSERT INTO tickets (user_id, project_id, title, description, status_id, due_date, ticket_assign_to_id, ticket_assign_time, category_id, critical_level_id, ticket_assigned_by_id) VALUES (1,1,'testing 2', 'OMG', 1, '2020-11-11', 1, '2020-11-11', 1, 1, 1);
INSERT INTO tickets (user_id, project_id, title, description, status_id, due_date, category_id, critical_level_id, ticket_assign_to_id, ticket_assign_time, ticket_assigned_by_id) VALUES (1,1,'testing 3', 'OMG3333333', 1, '2020-11-11', 1, 1, 1, '2020-11-11',1);
INSERT INTO tickets (user_id, project_id, title, description, status_id, due_date, category_id, critical_level_id, ticket_assign_to_id, ticket_assign_time, ticket_assigned_by_id) VALUES (1,2,'testing 4', 'OMG44444', 1, '2020-11-11', 1, 1, 1, '2020-11-11',1);
INSERT INTO tickets (user_id, project_id, title, description, status_id, due_date, category_id, critical_level_id, ticket_assign_to_id, ticket_assign_time, ticket_assigned_by_id) VALUES (2,2,'testing 5', 'OMG5555', 1, '2020-11-11', 1, 1, 1, '2020-11-11',1);

INSERT INTO comment (ticket_id, user_id, content, comment_id, created_at, updated_at) VALUES
(1, 1, 'testing', null, '2020-11-11', '2020-11-11'),
(1, 2, 'testing2', 1, '2020-11-11', '2020-11-11'),
(4, 1, 'testing3', null, '2020-11-11', '2020-11-11'),
(5, 2, 'testing4', null, '2020-11-11', '2020-11-11'),
(1, 3, 'Replying to id 2', 2, '2020-11-11', '2020-11-11'),
(1, 3, 'Replying to parent, id 1', 1, '2020-11-11', '2020-11-11'),
(1, 3, 'new comments, not replying to anyone', null, '2020-11-11', '2020-11-11');

INSERT INTO comment_file (comment_id, file_name) VALUES
(1, 'asd'),
(3, 'no delete'),
(3, 'no delete too'),
(2, 'no delete 1'),
(2, 'no delete 2');

INSERT INTO attached_file_ticket (ticket_id, file_name) VALUES
(1, 'testingtesting'),
(4, 'testingtesting ticket 4'),
(5, 'testingtesting ticket 5');