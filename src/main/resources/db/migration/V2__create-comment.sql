DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `ticket_id` int(11) NOT NULL,
      `user_id` int(11) NOT NULL,
      `content` text NOT NULL,
      `comment_id` int(11) NULL,
      CONSTRAINT `fk_comment_comment_id`
          foreign key (comment_id)
              REFERENCES `comment`(`id`)
              ON DELETE CASCADE,
      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
      updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `comment_file`;
CREATE TABLE `comment_file` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `comment_id` int(11) NOT NULL,
   CONSTRAINT `fk_commentFile_comment_id`
       foreign key (comment_id)
           REFERENCES `comment`(`id`)
           ON DELETE CASCADE,
   `file_name` text NOT NULL UNIQUE,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB;