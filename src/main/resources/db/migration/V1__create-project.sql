DROP TABLE IF EXISTS `project_status`;
CREATE TABLE `project_status` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `project_role`;
CREATE TABLE `project_role` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `project_name` varchar(255) NOT NULL UNIQUE ,
    `description` TEXT NULL,
    `status_id` int(11) NOT NULL,
    CONSTRAINT `fk_status_id`
    FOREIGN KEY (`status_id`)
        REFERENCES `project_status`(`id`),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `project_user`;
CREATE TABLE `project_user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `project_id` int(11) NOT NULL,
    CONSTRAINT `fk_project_id`
        FOREIGN KEY (`project_id`)
        REFERENCES `project`(`id`),
    `role_id` int(11) NOT NULL,
    CONSTRAINT `fk_role_id`
        FOREIGN KEY (`role_id`)
        REFERENCES `project_role`(`id`),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `thekey` (`user_id`,`project_id`)
) ENGINE=InnoDB;