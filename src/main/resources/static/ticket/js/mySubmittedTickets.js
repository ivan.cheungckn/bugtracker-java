// const PAGETITLE = 'Tickets In All Projects'
let result;
let tempMap
let selectInfo;
let opens;
let inProgresses;
let closeds;
let unassigneds;
let categories;
let criticalLevels;
let projectNames;
let difficulties, userList, statusList
let projectID;
let ticketIDs;
var rangeStart, rangeEnd;
let categoriesInfo, criticalLevelsInfo, projectIDNamesInfo, difficultiesInfo, userListInfo, statusListInfo
let senior = {
    criticalLevel: true,
    status: true,
    assignTo: true,
    category: true,
    dueDate: true,
    title: true,
    description: true,
    delete: true
}
let owner = {
    criticalLevel: true,
    status: false,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: true
}
let beAssigned = {
    criticalLevel: true,
    status: true,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: false
}
let developer = {
    criticalLevel: true,
    status: false,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: false
}
let rightMap

async function getInfo() {
    //get project name

    const fetchRes = await fetch("/api/v1/ticket/info");
    const result = await fetchRes.json();
    console.log(result)

    categoriesInfo = result.categories
    criticalLevelsInfo = result.criticalLevels
    projectIDNamesInfo = result.projectIDNames
    difficultiesInfo = result.difficulties
    userListInfo = result.userList
    statusListInfo = result.statusList
    console.log(criticalLevelsInfo)

}

async function getUser() {
    const fetchRes = await fetch('/api/v1/users/current_user')
    const jsonRes = await fetchRes.json()
    USERID = jsonRes.currentUser.userID
    console.log(jsonRes.currentUser)
}
async function getData(){
    const fetchRes = await fetch(`/api/v1/ticket?target=createdBy`)
    result = await fetchRes.json();
    console.log(result)
    return await result.result
}

async function processData(result){
    result = await result
    const filteredResult = result.map(ticket=>({"Ticket Title": ticket['Ticket Title'],
                                                "Project Name": ticket['Project Name'],
                                                "Critical Level": ticket['Ticket Critical Level'],
                                                "Status": ticket['Ticket Status'].toUpperCase(),
                                                "Created At" : new Date(ticket['Ticket Created At']).toLocaleString("en", "fa-IR"),
                                                "Assigned To": ticket['Ticket Assigned To Name']? ticket['Ticket Assigned To Name']:'',
                                                "Assigned Time": ticket['Ticket Assigned Time']? new Date(ticket['Ticket Assigned Time']).toLocaleString("en", "fa-IR"):'',
                                                "Due Date": 
                                                ticket['Ticket Due Date']? new Date(ticket['Ticket Due Date']).toLocaleDateString("en", "fa-IR"):'',
                                                "Action":`<div class="dropdown no-arrow">
                                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 19px, 0px);">
                                                            <div class="dropdown-header">Action:</div>
                                                            <button onclick="document.location.href= '/ticket/comment.html?ticketId=${ticket['Ticket ID']}&projectId=${ticket['Project ID']}'" class="dropdown-item more-info-btn" data-form="${ticket['Ticket ID']}">More Info</button>
                                                            <button class="dropdown-item edit-btn" data-form="${ticket['Ticket ID']}">Edit</button>
                                                            <button class="dropdown-item delete-btn" data-form="${ticket['Ticket ID']}">Delete</button>
                                                            </div>
                                            </div>`}))
                                                
    console.log(filteredResult)
    return filteredResult
}

function deleteTicket(){
    const deleteBtns = document.querySelectorAll(".delete-btn");
    for (const btn of deleteBtns){
        btn.addEventListener('click', async ()=>{
            if (confirm('Are You Sure To Delete the Ticket ?')){
                await fetch('/api/v1/ticket', {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ 'ticketID': btn.dataset.form })
                })

                main();
            }
        }
    )}
}

function genDetailInfoForm(ticket, files, className) {

    let insertHTML = '';
    insertHTML += `<div id="detail-info-form-${ticket['Ticket ID']}" class="detail-info-form forms-custom shadow" data-editMode=false data-form="${ticket['Ticket ID']}">

                    <!-- Modal content -->
                    <div class="detail-info-form-content">
                        <div class="row close-div">

                            <span class="close" id="close" data-form="${ticket['Ticket ID']}">&times;
                            </span>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                            <h5 class="info-card-title">${ticket['Ticket Title']}</h5>
                            <hr>
                        </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Project Name:</div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <div id="project-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Project Name']}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Category:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="category-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Category']}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Critical Level:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="critical-level-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Critical Level']}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Status:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="status-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Status']}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Due Date:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="due-date-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Due Date'] ? new Date(ticket['Ticket Due Date']).toLocaleDateString("en", "fa-IR") : null}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Created At:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="created-at-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Created At'] ? new Date(ticket['Ticket Created At']).toLocaleString("en", "fa-IR") : null}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned To:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-to-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned To Name'] ? ticket['Ticket Assigned To Name'] : ''}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">   
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned By:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-by-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned by Name'] ? ticket['Ticket Assigned by Name'] : ''}</div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned Time:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-time-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned Time'] ? ticket['Ticket Assigned Time'] : ''}</div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                        <hr>
                        <div class="col-md-12">
                                <p class="card-text">${ticket['Ticket Description']}</p> 
                            </div>
                        </div>
                        `
    if (files) {
        insertHTML += `<div class="row">
                                ${genImages(ticket, files, className, 'detailForm')}
                            </div>`
    }


    insertHTML += `
                    </div>

                    </div>`
    return insertHTML
}

function genImages(ticket, files, className, loc) {
    let insertHTML = ''
    insertHTML += `<div id="carousel-${loc}-${ticket['Ticket ID']}" class="carousel slide ${className}" data-ride="carousel" data-interval="false">`

    if (files.length > 1) {
        insertHTML += `<ol class="carousel-indicators">`
        for (const i in files) {
            if (i === 0) {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="0" class="active"></li>`
            } else {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="${i}"></li>`
            }
        }
        insertHTML += `</ol>`
    }

    insertHTML += `<div class="carousel-inner">
        <div class="carousel-item active">
        <a href="/${files[0]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[0]}"></a>
        </div>`

    for (let i = 1; i < files.length; i++) {
        insertHTML +=
            `<div class="carousel-item">
            <a href="/${files[i]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[i]}"></a>
            </div>`

    }
    insertHTML += `</div>`
    if (files.length > 1) {

        insertHTML += `
        <a class="carousel-control-prev" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>`

    }
    insertHTML += `</div>`
    return insertHTML;
}


function isTicketOwner(ticketID, userID) {
    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == ticketID).find(ticket => ticket['Ticket Created By User ID'] == userID)
    if (ticket) {
        return true
    }
    return false
}
async function getProjectRole(ticketID) {
    const projectID = result.result.filter(ticket => ticket['Ticket ID'] == ticketID)[0]['Project ID']
    console.log(projectID)
    const fetchRes = await fetch(`/api/v1/project/member?projectId=${projectID}`)
    const jsonRes = await fetchRes.json()
    return jsonRes.message.role
}
function isTicketAssignedTo(ticketID, userID) {
    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == ticketID).find(ticket => ticket['Ticket Assigned To User ID'] == userID)
    if (ticket) {
        return true
    }
    return false
}
async function getMembersInProject(projectID){
    const fetchRes = await fetch(`/api/v1/project/member/${projectID}`)
    const result = await fetchRes.json()
    console.log(result)
    return result.message
    // console.log(result.message)
}

async function genEditHTMLforProjectUser(projectID, currentName) {
    const infos = await getMembersInProject(projectID)
        // console.log(info.name)
        let insertHTML = ``
        oneTime = true
        for (let i = 0; i<infos.length; i++) {
            if ((currentName == 'null' || !currentName) && oneTime) {
                insertHTML += `<option value="" selected></option>`
                oneTime = false
                i--;
                continue;
            }
            if (currentName == infos[i].name) {
                insertHTML += `<option value="${infos[i].memberId}" selected>${infos[i].name}</option>`
            } else {
                insertHTML += `<option value="${infos[i].memberId}">${infos[i].name}</option>`
            }
        };
        return insertHTML
}
function genEditHTML(infos, currentName) {
        // console.log(info.name)
        let insertHTML = ``
        oneTime = true
        for (let i = 0; i<infos.length; i++) {
            if ((currentName == 'null' || !currentName) && oneTime) {
                insertHTML += `<option value="" selected></option>`
                oneTime = false
                i--;
                continue;
            }
            if (currentName == infos[i].name) {
                insertHTML += `<option value="${infos[i].id}" selected>${infos[i].name}</option>`
            } else {
                insertHTML += `<option value="${infos[i].id}">${infos[i].name}</option>`
            }
        };
        return insertHTML
}

async function toEditStatus(form) {
    const isOwner = isTicketOwner(form.dataset.form, USERID)
    const role = await getProjectRole(form.dataset.form)
    const isToBeAssigned = isTicketAssignedTo(form.dataset.form, USERID)
    const currentProjectID = result.result.filter((ticket)=> ticket['Ticket ID'] == form.dataset.form)[0]['Project ID']
    console.log(currentProjectID)
    console.log('uesrID: ' + USERID)
    console.log('isOwner: ' + isOwner)
    console.log('role: ' + role)
    console.log('istobeAssigned: ' + isToBeAssigned)

    // if(isowenr)
    if (role == 'project manager' || role == 'owner') {
        // tempMap = rightMap
        rightMap = { ...senior }
    } else if (role == 'junior developer') {
        // temp = rightMap
        rightMap = { ...developer }
    }

    if (isOwner) {
        // tempMap = rightMap
        for (const key of Object.keys(rightMap)) {
            if (rightMap[key] || owner[key]) {
                rightMap[key] = true
            }
        }
        console.log('isowner')

    }
    if (isToBeAssigned) {
        // tempMap = rightMap
        for (const key of Object.keys(rightMap)) {
            if (rightMap[key] || beAssigned[key]) {
                rightMap[key] = true
            }
        }

    }

    if (rightMap['criticalLevel']) {

        //critical level
        let criticalLevelinForm = form.querySelector(`#critical-level-${form.dataset.form}`)
        // let criticalLevelName = criticalLevelinForm.textContent.split(':')[1].replace(/\s/g, '');
        let insertHTML = genEditHTML(criticalLevelsInfo, criticalLevelinForm.innerHTML)
        $(`#critical-level-${form.dataset.form}`).replaceWith(`
        <select name= "ticketCriticalLevelID" id="inputCriticalLevel-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    if (rightMap['status']) {

        //status
        let statusinForm = form.querySelector(`#status-${form.dataset.form}`)
        // let statusName = statusinForm.textContent.split(':')[1].replace(/\s/g, '');
        insertHTML = genEditHTML(statusListInfo, statusinForm.innerHTML)
        $(`#status-${form.dataset.form}`).replaceWith(`
        <select name= "ticketStatusID" id="inputStatus-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    //difficulty
    // let difficultyinForm = form.querySelector(`#difficulty-${form.dataset.form}`)
    // let difficultyName = difficultyinForm.textContent.split(':')[1].replace(/\s/g, '');
    // insertHTML = genEditHTML(difficultiesInfo, difficultyName)
    // $(`#difficulty-${form.dataset.form}`).replaceWith(`<div class="detail-form-div"> <label class="edit-detail-form-label" for="inputDifficulty">Difficulty</label><select name= "ticketDifficultyID" id="inputDifficulty" class="form-control edit-detail-form" required>
    // ${insertHTML}</select></div>`);

    if (rightMap['category']) {
        //category
        let categoryinForm = form.querySelector(`#category-${form.dataset.form}`)
        // let categoryName = categoryinForm.textContent.split(':')[1].replace(/\s/g, '');
        insertHTML = genEditHTML(categoriesInfo, categoryinForm.innerHTML)
        $(categoryinForm).replaceWith(`<select name= "ticketCategoryID" id="inputCategory-${form.dataset.form}" class="form-control edit-detail-form" required>
                                            ${insertHTML}</select>`);
    }

    if (rightMap['dueDate']) {

        //dueDate
        let dueDateinForm = form.querySelector(`#due-date-${form.dataset.form}`)
        // let dueDateName = dueDateinForm.textContent.split(':')[1].replace(/\s/g, '');
        // console.log(new Date(dueDateName))

        // insertHTML = genEditHTML(, dueDateName)
        const defaultDueDate = dueDateinForm.innerHTML == 'null' ? '' : dueDateinForm.innerHTML
        $(dueDateinForm).replaceWith(`
            <div class="input-group date" id="due-date-${form.dataset.form}">
            <input type="text" id="input-due-date-${form.dataset.form}" class="form-control" value="${defaultDueDate}">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
            </div>`);

        $(`#due-date-${form.dataset.form}`).datepicker({
            todayBtn: true,
            clearBtn: true,
            autoclose: true,
            todayHighlight: true,

        });
    }

    if (rightMap['assignTo']) {

        //assign to name
        let assignToinForm = form.querySelector(`#assign-to-name-${form.dataset.form}`)
        // let assignToName = assignToinForm.textContent.split(':')[1].replace(/\s/g, '');
        console.log(assignToinForm.innerHTML)
        insertHTML = await genEditHTMLforProjectUser(currentProjectID, assignToinForm.innerHTML)
        $(assignToinForm).replaceWith(`<select name= "ticketAssignToID" id="inputAssignTo-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    if (rightMap['title']) {

        //title
        let title = form.querySelector('.info-card-title');
        title.innerHTML = `<input type="text" id="inputTitle-${form.dataset.form}" class="form-control form-rounded" value="${title.innerHTML}">`
    }

    if (rightMap['description']) {

        //description
        let description = form.querySelector(`.card-text`)
        $(description).replaceWith(`<div class="col-md-10"><div class="form-group">
                <label for="inputDescription"></label>
                <textarea class="form-control" id="inputDescription-${form.dataset.form}" rows="5">${description.innerHTML}</textarea>
            </div></div>`)
    }

    if (rightMap['delete']) {

        const deleteHTML = `<button class="delete-btn" data-form="${form.dataset.form}">
                                                <i class="far fa-trash-alt"></i>
                                            </button>`
        form.querySelector(".close-div").insertAdjacentHTML('afterbegin', deleteHTML)

        form.querySelector(".delete-btn").addEventListener('click', async () => {
            if (confirm("Are You Sure To Delete the Ticket ?")) {

                await fetch('/api/v1/ticket', {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ 'ticketID': form.dataset.form })
                })

                form.remove()
                main();
            }
        })
    }
    rightMap = tempMap
}

function addListenerToCardsAndWindow() {
    let forms = document.querySelectorAll(".forms-custom[data-form]")
    for (const form of forms) {
        let btnClose = document.querySelector(`.close[data-form="${form.dataset.form}"]`)
        let btnEdit = document.querySelector(`.edit-btn[data-form="${form.dataset.form}"]`)
        console.log(btnEdit)
        btnClose.onclick = function () {
            form.style.display = "none";
            console.log("working")
        }
        btnEdit.addEventListener('click', () => {
            form.style.display='block'
            toEditStatus(form)

            form.dataset.editMode = true;


        })
        //except for filter form
        // addListenerToCardBody(form);

        window.addEventListener('click', async (event) => {
            if (event.target == form) {
                // if ()
                console.log(result.result)
                console.log('in window workgin')

                console.log(form.dataset.form)
                if (form.dataset.editMode) {
                    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == form.dataset.form)[0]

                    console.log(ticket)
                    //compare edited? information
                    // console.log($(`#inputCriticalLevel-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputStatus-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputCategory-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputAssignTo-${form.dataset.form} option:selected`).val())
                    // console.log($(`#input-due-date-${form.dataset.form}`).val())
                    // console.log($(`#inputTitle-${form.dataset.form}`).val())
                    // console.log($(`#inputDescription-${form.dataset.form}`).val())
                    isChanged = false;
                    if ($(`#inputCriticalLevel-${form.dataset.form} option:selected`).text() != ticket['Ticket Critical Level']) {
                        console.log('changed critical level');
                        isChanged = true;
                    }
                    if ($(`#inputStatus-${form.dataset.form} option:selected`).text() != ticket['Ticket Status']) {
                        console.log('changed Status');
                        isChanged = true;
                    }
                    if ($(`#inputCategory-${form.dataset.form} option:selected`).text() != ticket['Ticket Category']) {
                        console.log('changed Category');
                        isChanged = true;
                    }
                    if ($(`#inputAssignTo-${form.dataset.form} option:selected`).val() != ticket['Ticket Assigned To User ID'] && $(`#inputAssignTo-${form.dataset.form} option:selected`).val()) {
                        console.log($(`#inputAssignTo-${form.dataset.form} option:selected`).val())
                        console.log('changed Assigned To Name');
                        isChanged = true;
                    }
                    if ($(`#input-due-date-${form.dataset.form}`).val() != moment(new Date(ticket['Ticket Due Date'])).format('MM/DD/YYYY') && $(`#input-due-date-${form.dataset.form}`).val()) {
                        console.log('changed Due Date');
                        isChanged = true;
                    }
                    if ($(`#inputTitle-${form.dataset.form}`).val() != ticket['Ticket Title']) {
                        console.log('changed Title');
                        isChanged = true;
                    }
                    if ($(`#inputDescription-${form.dataset.form}`).val() != ticket['Ticket Description']) {
                        console.log('changed Description');
                        isChanged = true;
                    }
                    console.log('ticketid: ' + ticket['Ticket ID'])
                    if (isChanged) {
                        const data = {
                            ticketID: ticket['Ticket ID'],
                            category: $(`#inputCategory-${form.dataset.form} option:selected`).text(),
                            difficulty: ticket['Ticket Difficulty'],
                            criticalLevel: $(`#inputCriticalLevel-${form.dataset.form} option:selected`).text(),
                            dueDate: $(`#input-due-date-${form.dataset.form}`).val(),
                            title: $(`#inputTitle-${form.dataset.form}`).val(),
                            description: $(`#inputDescription-${form.dataset.form}`).val(),
                            status: $(`#inputStatus-${form.dataset.form} option:selected`).text(),
                            ticketAssignedToID: $(`#inputAssignTo-${form.dataset.form} option:selected`).val()
                        }
                        console.log($(`#inputAssignTo-${form.dataset.form} option:selected`).val())

                        if($(`#inputStatus-${form.dataset.form} option:selected`).text().toLowerCase() == 'unassigned'){
                            data['isAssignToNull'] = true;
                            // data['status'] = 'open'
                        }else{
                            if(!$(`#inputAssignTo-${form.dataset.form} option:selected`).val()){
                                alert("Not yet assign the tickets to others !!!!!")
                            }
                        }
                        
                        await fetch('/api/v1/ticket', {
                            method: 'PUT',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(data)
                        })
                        main();
                    }

                }



                form.style.display = "none";


            }

        });
        // console.log('forms: ' + form)
    }
}

async function genDataTable(){
    const data = await getData();
    const filteredResult = await processData(data);

    let table = document.createElement('table')
    table.className = "table table-striped table-bordered hover display dt-responsive";
    table.id = "dataTable";
    //thead
    const thObjClassMap = {"Ticket Title":"",
                    "Project Name":"",
                    "Critical Level":"badge,badge-warning",
                    "Status":"",
                    "Created At":"" ,
                    "Assigned To":"badge,badge-secondary",
                    "Assigned Time":"badge,badge-primary",
                    "Due Date": "badge,badge-danger",
                    "Action":"btn,btn-datatable,btn-icon,btn-transparent-dark,mr-2"}
    
    let thead = document.createElement('thead')
    let trInHead = document.createElement('tr')
    for (const th of Object.keys(thObjClassMap)){
        let thElement = document.createElement('th')
        thElement.textContent = th;
        trInHead.appendChild(thElement)
    }
    thead.appendChild(trInHead)
    
    //end thead

    //start tbody
    console.log(filteredResult)
    const tbody = document.createElement('tbody');
    
    for (const item of filteredResult){
        const trInBody = document.createElement('tr')
        let i = 0
        let keys = Object.keys(item)
        for (const key of keys){
            const tdElement = document.createElement('td')
            let isBadge = false
            if (thObjClassMap[keys[i]]){
                let div
                let spanWrapper
                if (keys[i] == 'Action'){
                    div = document.createElement('button')
                } else{

                    div = document.createElement('span')
                    spanWrapper = document.createElement('h5')
                }
                for (const k of thObjClassMap[keys[i]].split(',')){
                    div.classList.add(k)
                    div.innerHTML = item[key];
                    if(k =='badge'){

                        isBadge = true
                    }
                }
                if (spanWrapper && isBadge){
                    spanWrapper.appendChild(div)
                    tdElement.appendChild(spanWrapper)
                }else{

                    tdElement.appendChild(div)
                }
            }else{
                tdElement.textContent = item[key];
            }
            // console.log(thObjClassMap[keys[i]])
            
            trInBody.appendChild(tdElement);
            i++;
        }
        tbody.appendChild(trInBody)
    }

    //end tbody
    table.appendChild(thead)
    table.appendChild(tbody)
    return table
}

function genHeading(){
    const heading = document.createElement('div')
    const headingText = document.createElement('h3')
    headingText.textContent = "My Submitted Tickets"
    heading.appendChild(headingText)
    return heading
}

async function showDataTable(){
    const table = await genDataTable();
    const dataTable = document.querySelector("#main-content");
    dataTable.innerHTML =''
    dataTable.classList.add('container-fluid')
    
    const heading = genHeading()
    dataTable.appendChild(heading)
    dataTable.appendChild(table)
    
    $('#dataTable').DataTable({
        responsive: true
    });
    // $('#dataTable').DataTable({
    //         "ajax": "data/arrays.txt",
    //         "columnDefs": [ {
    //             "targets": -1,
    //             "data": null,
    //             "defaultContent": "<button>Click!</button>"
    //         } ]
    //     }
    //     )


}

function showDetailForm(){
    const dataTable = document.querySelector("#main-content");
    for (const ticket of result.result){
        dataTable.insertAdjacentHTML('beforeend',genDetailInfoForm(ticket, ticket['Ticket Attached Files'], 'image-in-detail-form', 'detail-form'))
    }
}



async function main(){

    await getInfo()
    await getUser()
    await showDataTable();
    showDetailForm();
    addListenerToCardsAndWindow()
    deleteTicket();
}

main();