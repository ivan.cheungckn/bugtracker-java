let categories;
let criticalLevels;
let projectIDNames;
let difficulties, userList, statusList;
async function getInfo() {
  //get project name

  const fetchRes = await fetch("/api/v1/ticket/info");
  const result = await fetchRes.json();
  console.log(result)

  categories = result.categories
  criticalLevels = result.criticalLevels
  projectIDNames = result.projectIDNames
  difficulties = result.difficulties
  userList = result.userList
  statusList = result.statusList

}

//main content
function insertHTML() {
  console.log(categories);
  document.querySelector("#main-content").innerHTML = "";
  HTMLtobeInsert = `<div class="container">
    
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Create Ticket</h1>
        <form class="needs-validation" action="/api/v1/ticket" enctype="multipart/form-data" method="POST" novalidate>
        <div class="form-row">
            <div class="form-group col-md-6">
            <label for="validationCustom01">Ticket Title</label>
            <input name= "ticketTitle" type="text" class="form-control" id="validationCustom01" placeholder="Enter Ticket Title" required>
            <div class="invalid-feedback">
                Please input ticket title.
            </div>
            </div>
    
            <div class="form-group col-md-6">
            <label for="selectProject">Project</label>
            <select name= "projectID" id="selectProject" class="custom-select" required>
                <option value="">Choose...</option>`;
  console.log(projectIDNames);
  for (const projectIDName of projectIDNames) {
    HTMLtobeInsert += `<option value='${projectIDName.id}'>${projectIDName.project_name}</option>`;
  }

  HTMLtobeInsert += `</select>
            <div class="invalid-feedback">Please select a project.</div>
            </div>
    
        </div>
    
    
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Ticket Description</label>
            <textarea name= "ticketDescription" class="form-control" id="exampleFormControlTextarea1" rows="3"
            placeholder="Enter Ticket Description" required></textarea>
            <div class="invalid-feedback">Please enter ticket description.</div>
        </div>
    
        <div class="form-row">
    
            <div class="form-group col-md-6">
            <label for="inputCriticalLevel">Critical Level</label>
            <select name= "ticketCriticalLevelID" id="inputCriticalLevel" class="form-control" required>
                <option value="">Choose...</option>`;

  for (const criticalLevel of criticalLevels) {
    HTMLtobeInsert += `<option value="${criticalLevel.id}">${criticalLevel.name}</option>`;
  }

  HTMLtobeInsert += `</select>
            <div class="invalid-feedback">Please select critical level.</div>
            </div>
            <div class="form-group col-md-6">
            <label for="selectCategory">Categories</label>
            <select name= "ticketCategoryID" id="selectCategory" class="form-control" required>
                <option value="">Choose...</option>`;
  for (const category of categories) {
    HTMLtobeInsert += `<option value="${category.id}">${category.name}</option>`;
  }

  HTMLtobeInsert += `</select>
            <div class="invalid-feedback">Please select a category.</div>
            </div>
    
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div class="custom-file">
            <input name= "image" type="file" class="custom-file-input" id="inputGroupFile01"
                aria-describedby="inputGroupFileAddon01" accept='image/*' multiple>
            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
        </div>
            <button type="submit" class="btn btn-primary" id="btn-create-ticket-submit" value='POST'>Submit</button>
        </form>
        </div>`;
  document.querySelector("#main-content").innerHTML = HTMLtobeInsert;
}

function runJquery() {
  $("#inputGroupFile01").on("change", function () {
    //get the file name
    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
    //replace the "Choose a file" label
    $(this).next(".custom-file-label").html(fileName);
  });
}

function validation() {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName("needs-validation");

  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function (form) {
    form.addEventListener(
        "submit",
        function (event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add("was-validated");
          // location.href('/ticket/mySubmittedTicket.html')
          // location.href = "/ticket/mySubmittedTicket.html";
          // event
          // console.log('can see?')
        },
        false
    );
  });
}


async function main() {
  await getInfo();
  insertHTML();
  runJquery();
  validation();

  console.log("ran");
}

main();
