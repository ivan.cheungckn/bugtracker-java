let result;
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const TICKETID = urlParams.get('ticketId')
console.log(TICKETID)

async function getMyTickets() {
    const fetchRes = await fetch(`/api/v1/ticket?target=byID&ticketID=${TICKETID}`);
    result = await fetchRes.json();
    console.log(result.result)
    result = result.result[0]
    // let resultForSelection = [...result.result][0];

}


function genDetailInfoForm(ticket, files, className) {

    let insertHTML = '';
    insertHTML += `<div id="detail-info-form-${ticket['Ticket ID']}" class="detail-info-form forms-custom" data-editMode=false data-form="${ticket['Ticket ID']}">

                    <!-- Modal content -->
                    <div class="detail-info-form-content row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="info-card-title">${ticket['Ticket Title']}</h5>
                                <hr>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Project Name:</div>
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div id="project-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Project Name']}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Category:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="category-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Category']}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Critical Level:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="critical-level-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Critical Level']}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Status:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="status-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Status']}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Due Date:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="due-date-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Due Date'] ? new Date(ticket['Ticket Due Date']).toLocaleDateString("en", "fa-IR") : ''}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Created At:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="created-at-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Created At'] ? new Date(ticket['Ticket Created At']).toLocaleString("en", "fa-IR") : ''}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Assigned To:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="assign-to-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned To Name'] ? ticket['Ticket Assigned To Name'] : ''}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">   
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Assigned By:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="assign-by-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned by Name'] ? ticket['Ticket Assigned by Name'] : ''}</div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Assigned Time:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="assign-time-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned Time'] ? new Date(ticket['Ticket Assigned Time']).toLocaleString("en", "fa-IR") : ''}</div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hashtag-title">Ticket Description:</div>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text">${ticket['Ticket Description']}</p> 
                                        </div>
                                    </div> 
                                </div>
                            </div>`
    // <div class="row">
    // <hr>
    //     <p class="card-text">${ticket['Ticket Description']}</p> 
    // </div>`
    let file = ticket['Ticket Attached Files'];
    insertHTML += `     </div>`
    if (file) {
        insertHTML += `     
        <div class="col-md-6">
        <div class="row">
                                ${genImages(ticket, file, className, 'detailForm')}
                            </div>
                            </div>`
    }
    insertHTML +=     `</div>
                    </div>`
    return insertHTML
}

// genDetailInfoForm(ticket, ticket['Ticket Attached Files'], 'image-in-detail-form', 'detail-form')
function genImages(ticket, files, className, loc) {
    let insertHTML = ''
    insertHTML += `<div id="carousel-${loc}-${ticket['Ticket ID']}" class="carousel slide ${className}" data-ride="carousel" data-interval="false">`

    if (files.length > 1) {
        insertHTML += `<ol class="carousel-indicators">`
        for (const i in files) {
            if (i === 0) {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="0" class="active"></li>`
            } else {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="${i}"></li>`
            }
        }
        insertHTML += `</ol>`
    }

    insertHTML += `<div class="carousel-inner">
        <div class="carousel-item active">
        <a href="/${files[0]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[0]}"></a>
        </div>`

    for (let i = 1; i < files.length; i++) {
        insertHTML +=
            `<div class="carousel-item">
            <a href="/${files[i]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[i]}"></a>
            </div>`

    }
    insertHTML += `</div>`
    if (files.length > 1) {

        insertHTML += `
        <a class="carousel-control-prev" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>`

    }
    insertHTML += `</div>`
    return insertHTML;
}

async function main() {
    await getMyTickets()
    if (result.hasOwnProperty('Ticket Attachments')) {
        genDetailInfoForm(result, result['Ticket Attachments'], 'ticket-info')

    } else {
        genDetailInfoForm(result, '', 'ticket-info')

    }
    // const infoDiv = document.querySelector('.container-fluid')
    const infoDiv = document.createElement('div')
    infoDiv.classList.add('container')
    infoDiv.innerHTML = genDetailInfoForm(result, '', 'ticket-info')
    document.querySelector(".container-fluid h1").insertAdjacentElement('afterend', infoDiv)
}
main();