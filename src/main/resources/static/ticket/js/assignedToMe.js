const PAGETITLE = 'Tickets Assigned To Me'
let result;
let tempMap
let selectInfo;
let opens;
let inProgresses;
let closeds;
let unassigneds;
let categories;
let criticalLevels;
let projectNames;
let difficulties, userList, statusList
let projectID;
let ticketIDs;
var rangeStart, rangeEnd;
let categoriesInfo, criticalLevelsInfo, projectIDNamesInfo, difficultiesInfo, userListInfo, statusListInfo
let senior = {
    criticalLevel: true,
    status: true,
    assignTo: true,
    category: true,
    dueDate: true,
    title: true,
    description: true,
    delete: true
}
let owner = {
    criticalLevel: true,
    status: false,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: true
}
let beAssigned = {
    criticalLevel: true,
    status: true,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: false
}
let developer = {
    criticalLevel: true,
    status: false,
    assignTo: false,
    category: true,
    dueDate: false,
    title: true,
    description: true,
    delete: false
}
let USERID;
async function getUser() {
    const fetchRes = await fetch('/api/v1/users/current_user')
    const jsonRes = await fetchRes.json()
    USERID = jsonRes.currentUser.userID
    console.log(jsonRes.currentUser)
}

// let USERID = 5
let rightMap = senior
//l?q=myProjectTickets&target=assignTo
//?projectName=xxx
async function getInfo() {
    //get project name

    const fetchRes = await fetch("/api/v1/ticket/info");
    const result = await fetchRes.json();
    console.log(result)

    categoriesInfo = result.categories
    criticalLevelsInfo = result.criticalLevels
    projectIDNamesInfo = result.projectIDNames
    difficultiesInfo = result.difficulties
    userListInfo = result.userList
    statusListInfo = result.statusList
    console.log(criticalLevelsInfo)

}
async function getMyTickets() {
    const fetchRes = await fetch('/api/v1/ticket?target=assignTo'); //changed
    result = await fetchRes.json();
    console.log(result.result)

    let resultForSelection = [...result.result];
    ticketIDs = [...new Set(resultForSelection.map(a => a['Ticket ID']))]
    //create select options

    projectNames = [...new Set(resultForSelection.map(a => a['Project Name']))]
    criticalLevels = [...new Set(resultForSelection.map(a => a['Ticket Critical Level']))]
    categories = [...new Set(resultForSelection.map(a => a['Ticket Category']))]
    difficulties = [...new Set(resultForSelection.map(a => a['Ticket Difficulty']))]
    userListAssignedTo = [...new Set(resultForSelection.map(a => a['Ticket Assigned To Name']))]
    userListAssignedBy = [...new Set(resultForSelection.map(a => a['Ticket Assigned by Name']))]
    userListCreatedBy = [...new Set(resultForSelection.map(a => a['Ticket Created By Name']))]
    statusList = [...new Set(resultForSelection.map(a => a['Ticket Status']))]
    // console.log('xxxx:  ' + categories)
}
// Doing Filter sub function
function filterTickets(resultCopyTemp, filterBy, IDName) {
    if ($(IDName).val()[0]) {
        let resultSelected = []
        let tempSaving = []
        for (const selected of $(IDName).val()) {
            if (selected) {
                tempSaving = resultCopyTemp.filter(ticket => ticket[filterBy] == selected);
                resultSelected = resultSelected.concat(tempSaving);
                console.log(resultSelected)
            }
        }
        return resultSelected
    }
    return resultCopyTemp;
}


// Doing Filter Main
async function processTickets() {
    let resultCopy = JSON.parse(JSON.stringify(result.result))
    resultCopy = filterTickets(resultCopy, 'Project Name', '#selectFormProject')
    resultCopy = filterTickets(resultCopy, 'Ticket Category', '#selectFormCategory')
    resultCopy = filterTickets(resultCopy, 'Ticket Critical Level', '#selectFormCriticalLevel')
    resultCopy = filterTickets(resultCopy, 'Ticket Difficulty', '#selectFormDifficulty')
    resultCopy = filterTickets(resultCopy, 'Ticket Status', '#selectFormStatus')
    resultCopy = filterTickets(resultCopy, 'Ticket Assigned To Name', '#selectFormAssignedTo')
    resultCopy = filterTickets(resultCopy, 'Ticket Assigned By Name', '#selectFormAssignedBy')
    resultCopy = filterTickets(resultCopy, 'Ticket Created By Name', '#selectFormCreatedBy')
    rangeStart = new Date($("#range-start").val());
    rangeEnd = new Date($('#range-end').val());
    if (rangeStart == 'Invalid Date') {
        rangeStart = new Date('01-01-2000');
    }
    if (rangeEnd == 'Invalid Date') {
        rangeEnd = new Date('01-01-9999');
    }
    // console.log('get dates: ' + $('.datepicker').datepicker('getStartDate'))
    // console.log('in process tickets: ' + rangeStart)
    // console.log('in process tickets: ' + rangeEnd)
    // console.log(resultCopy)
    let includeDueDateCheckBox = document.querySelector("#includeNoDueDate");
    if (includeDueDateCheckBox.checked == true) {
        resultCopy = resultCopy.filter(ticket => (new Date(ticket['Ticket Due Date']) >= rangeStart && new Date(ticket['Ticket Due Date']) <= rangeEnd) || !(ticket['Ticket Due Date']));
    } else {
        resultCopy = resultCopy.filter(ticket => new Date(ticket['Ticket Due Date']) >= rangeStart && new Date(ticket['Ticket Due Date']) <= rangeEnd)
    }
    // console.log(resultCopy)
    unassigneds = resultCopy.filter(ticket => ticket['Ticket Status'] == 'unassigned' );//&& ticket['Ticket Assigned To User ID'] == null
    opens = resultCopy.filter(ticket => ticket['Ticket Status'] == 'open');// && ticket['Ticket Assigned To User ID'] != null
    inProgresses = resultCopy.filter(ticket => ticket['Ticket Status'] == 'in progress');
    closeds = resultCopy.filter(ticket => ticket['Ticket Status'] == 'closed');

    for (const element of resultCopy) {
        element['Ticket Due Date'] = element['Ticket Due Date'] ? new Date(element['Ticket Due Date']).toLocaleDateString("en", "fa-IR") : null
        element['Ticket Created At'] = element['Ticket Created At'] ? new Date(element['Ticket Created At']).toLocaleString("en", "fa-IR") : null
        element['Ticket Updated At'] = element['Ticket Updated At'] ? new Date(element['Ticket Updated At']).toLocaleString("en", "fa-IR") : null
        element['Ticket Assigned Time'] = element['Ticket Assigned Time'] ? new Date(element['Ticket Assigned Time']).toLocaleString("en", "fa-IR") : null
    }
    insertCards();
    addListenerToCardsAndWindow();
    await dragCards();
}

//checkTicketOwner
function isTicketOwner(ticketID, userID) {
    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == ticketID).find(ticket => ticket['Ticket Created By User ID'] == userID)
    if (ticket) {
        return true
    }
    return false
}
function isTicketAssignedTo(ticketID, userID) {
    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == ticketID).find(ticket => ticket['Ticket Assigned To User ID'] == userID)
    if (ticket) {
        return true
    }
    return false
}
async function getProjectRole(ticketID) {
    const projectID = result.result.filter(ticket => ticket['Ticket ID'] == ticketID)[0]['Project ID']
    console.log(projectID)
    const fetchRes = await fetch(`/api/v1/project/member?projectId=${projectID}`)
    const jsonRes = await fetchRes.json()
    return jsonRes.message.role
}


//listen to change --> then remove card and reload
function addListenerToSelect() {
    let selectPickers = document.querySelectorAll(".selectpicker");
    // console.log(selectPickers)
    for (const selectPicker of selectPickers) {
        selectPicker.addEventListener('change', (event) => {
            let cardContent = document.querySelector('#card-content')
            if (cardContent) {
                cardContent.parentNode.removeChild(cardContent);
            }
            processTickets();
        })
    }
    let dueDateCheckBox = document.querySelector("#includeNoDueDate");
    dueDateCheckBox.addEventListener('click', (event) => {
        let cardContent = document.querySelector('#card-content')
        if (cardContent) {
            cardContent.parentNode.removeChild(cardContent);
        }
        processTickets();
    })
    let detailInfoForms = document.querySelectorAll('.detail-info-form')
    for (const form of detailInfoForms) {
        form.addEventListener('click', (event) => {

        })
    }
    let form = document.querySelector("#filter-form");
    form.addEventListener('onfocusout', (event) => {
        console.log('form: ' + form)

        let cardContent = document.querySelector('#card-content')
        if (cardContent) {
            cardContent.parentNode.removeChild(cardContent);
        }
        processTickets();
    })
}

//generate select option
function genOption(name, infoArray, needLabel, labelName, optionalColSize) {
    let insertHTML = ''
    insertHTML += `
    <div class="form-group ${optionalColSize}">`
    if (needLabel) {
        insertHTML += `<label for="${name}">${labelName}</label>`
    }
    insertHTML += `<select name= "${name}" class="form-control selectpicker" id="${name}" multiple data-live-search="true">`
    for (const info of infoArray) {
        // if (name=='selectProject' || name=='selectFormProject'){
        //     insertHTML += `<option value='${info}'>${info}</option>
        //     `
        // } else{
        if (info == null) {
            continue;
        }
        insertHTML += `<option value='${info}'>${info}</option>`
        // }
    }
    insertHTML += `</select>
    </div>`
    return insertHTML;
}

// top bar
function insertFirstBar() {
    let insertHTML = ''
    let ticketsInfo =
        insertHTML += `<div class="container">`
        insertHTML += `<div class="row"><div class="col-md-12"><h2>${PAGETITLE}</h2></div></div>`
        insertHTML +=`<div class="row" id = "first-bar">`
                            


    // insertHTML += genOption('selectProject', projectNames, false);
    
    insertHTML += `<input id="myInput" type="text" placeholder="Search..">`
    insertHTML += `<button type="submit" class="btn btn-primary btn-first-bar" id="btn-filter">Filter</button>`
    insertHTML += `
                        </div>
                    </div>`
    document.querySelector('#main-content').innerHTML = insertHTML;
};
//generate cards
function genCard(tickets) {
    let insertHTML = '';
    for (const ticket of tickets) {
        insertHTML += `<div class="card shadow draggable" draggable=true>
            <div class="card-body">
                <div class="card-content" data-form="${ticket['Ticket ID']}">
                <h5 class="card-title">${ticket['Ticket Title']}</h5>
                <p class="card-text">${ticket['Ticket Description']}</p></div>`
        let files = ticket['Ticket Attached Files']
        if (files) {
            insertHTML += genImages(ticket, files, 'image-in-card', 'card')
        }
        insertHTML += `</div>
            <div class="card-footer">
                
                <small class="hashtag hashtag-project-name">${ticket['Project Name']}</small>
                <small class="hashtag hashtag-category">${ticket['Ticket Category']}</small>
                <small class="hashtag hashtag-critical-level">${ticket['Ticket Critical Level']}</small>`
        if (ticket['Ticket Due Date']) {
            insertHTML += `<small class="hashtag hashtag-due-date">${ticket['Ticket Due Date']}</small>`
        }
        insertHTML += `</div>
        </div>`
        insertHTML += genDetailInfoForm(ticket, ticket['Ticket Attached Files'], 'image-in-detail-form', 'detail-form')
    }
    return insertHTML;
}

function genImages(ticket, files, className, loc) {
    let insertHTML = ''
    insertHTML += `<div id="carousel-${loc}-${ticket['Ticket ID']}" class="carousel slide ${className}" data-ride="carousel" data-interval="false">`

    if (files.length > 1) {
        insertHTML += `<ol class="carousel-indicators">`
        for (const i in files) {
            if (i === 0) {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="0" class="active"></li>`
            } else {
                insertHTML += `<li data-target="carousel-${loc}-${ticket['Ticket ID']}" data-slide-to="${i}"></li>`
            }
        }
        insertHTML += `</ol>`
    }

    insertHTML += `<div class="carousel-inner">
        <div class="carousel-item active">
        <a href="/${files[0]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[0]}"></a>
        </div>`

    for (let i = 1; i < files.length; i++) {
        insertHTML +=
            `<div class="carousel-item">
            <a href="/${files[i]}" target="_blank" rel="noopener noreferrer"><img class="d-block w-100" src="/${files[i]}"></a>
            </div>`

    }
    insertHTML += `</div>`
    if (files.length > 1) {

        insertHTML += `
        <a class="carousel-control-prev" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-${loc}-${ticket['Ticket ID']}" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>`

    }
    insertHTML += `</div>`
    return insertHTML;
}
//insert cards into HtML
function insertCards() {
    let insertHTML = '';
    insertHTML += `<div class="row" id="card-content">`
    //                     <div class="col-md-3 cards-col" data-col-for="Unassigned"> //changed
    //                         <h3>Unassigned</h3>`
    // insertHTML += genCard(unassigneds);
    //insertHTML += '</div>'
    insertHTML += `
                <div class="col-md-4 cards-col" data-col-for="Open">
                    <h3>Open</h3>`
    insertHTML += genCard(opens);
    insertHTML += `</div>
                <div class="col-md-4 cards-col" data-col-for="In Progress">
                    <h3>In Progress</h3>`
    insertHTML += genCard(inProgresses);
    insertHTML += `</div>
                    <div class="col-md-4 cards-col" data-col-for="Closed">
                    <h3>Closed</h3>`
    insertHTML += genCard(closeds);
    insertHTML += `</div>
                </div>`
    let loc = document.querySelector("#first-bar");
    // console.log(loc)
    loc.insertAdjacentHTML("afterend", insertHTML);
}

//create filter Form
function filterForm() {
    let insertHTML = ''
    insertHTML += `<div id="filter-form" class="filter-form" data-form="filter-form">
                    <div class="filter-form-content">
                        <div class="form-row close-div"><span class="close" id="close-filter" data-form="filter-form">&times;</span></div>
                        <div class="form-row">
                            ${genOption('selectFormProject', projectNames, true, 'Project', 'col-md-6')}
                            ${genOption('selectFormCategory', categories, true, 'Category', 'col-md-6')}
                        </div>
                        <div class="form-row">
                            ${genOption(
        "selectFormCriticalLevel",
        criticalLevels,
        true,
        "Critical Level",
        "col-md-6"
    )}
                            ${genOption(
        "selectFormDifficulty",
        difficulties,
        true,
        "Difficulty",
        "col-md-6"
    )}
                        </div>
                        <div class="form-row">
                            ${genOption(
        "selectFormStatus",
        statusList,
        true,
        "Status",
        "col-md-6"
    )}
                            ${genOption(
        "selectFormAssignedTo",
        userListAssignedTo,
        true,
        "Ticket Assigned To",
        "col-md-6"
    )}
                        </div>
                        <div class="form-row">
                            ${genOption(
        "selectFormAssignedBy",
        userListAssignedBy,
        true,
        "Ticket Assigned by",
        "col-md-6"
    )}
                            ${genOption(
        "selectFormCreatedBy",
        userListCreatedBy,
        true,
        "Ticket Created By",
        "col-md-6"
    )}
                        </div>
                        <div class="input-group input-daterange">
                            <input id="range-start" type="text" class="form-control" value="2010-01-01">
                            <div class="input-group-addon">to</div>
                            <input id="range-end" type="text" class="form-control" value="9999-04-19">
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="includeNoDueDate" required checked>
                            <label class="form-check-label" for="includeNoDueDate">
                                Include No Due Date Tickets
                            </label>
                        </div>

                    </div>

                    </div>`;
    document.body.insertAdjacentHTML("beforeend", insertHTML);
}

function genDetailInfoForm(ticket, files, className) {

    let insertHTML = '';
    insertHTML += `<div id="detail-info-form-${ticket['Ticket ID']}" class="detail-info-form forms-custom shadow" data-editMode=false data-form="${ticket['Ticket ID']}">

                    <!-- Modal content -->
                    <div class="detail-info-form-content">
                        <div class="row close-div">
                        
                            <div class="dropdown no-arrow">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 19px, 0px);">
                                <div class="dropdown-header">Action:</div>
                                <button class="dropdown-item edit-btn" data-form="${ticket['Ticket ID']}">Edit</button>
                                </div>
                            </div>
                            <span class="close" id="close" data-form="${ticket['Ticket ID']}">&times;
                            </span>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                            <h5 class="info-card-title">${ticket['Ticket Title']}</h5>
                            <hr>
                        </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Project Name:</div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <div id="project-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Project Name']}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Category:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="category-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Category']}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Critical Level:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="critical-level-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Critical Level']}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Status:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="status-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Status']}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Due Date:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="due-date-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Due Date'] ? ticket['Ticket Due Date'] : ''}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Created At:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="created-at-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Created At'] ? ticket['Ticket Created At'] : ''}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned To:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-to-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned To Name'] ? ticket['Ticket Assigned To Name'] : ''}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">   
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned By:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-by-name-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned by Name'] ? ticket['Ticket Assigned by Name'] : ''}</div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="hashtag-title">Ticket Assigned Time:</div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="assign-time-${ticket['Ticket ID']}" class="hashtag-content">${ticket['Ticket Assigned Time'] ? ticket['Ticket Assigned Time'] : ''}</div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                        <hr>
                        <div class="col-md-12">
                                <p class="card-text">${ticket['Ticket Description']}</p> 
                            </div>
                        </div>
                        `
    if (files) {
        insertHTML += `<div class="row">
                                ${genImages(ticket, files, className, 'detailForm')}
                            </div>`
    }


    insertHTML += `<div class="row">
                        
                        <a href="/ticket/comment.html?ticketId=${ticket['Ticket ID']}&projectId=${ticket['Project ID']}" class="btn btn-secondary btn-icon-split more-info">
                            <span class="icon text-white-50">
                                <i class="fas fa-arrow-right"></i>
                            </span>
                        <span class="text">More Infos</span>
                        </a>                        
                    </div>
                    </div>

                    </div>`
    return insertHTML
}

async function genAssignForm(ticketID) {
    const ticket = result.result.find(ticket => ticket['Ticket ID'] == ticketID)
    let insertHTML = '';
    insertHTML += `<div id="assign-form-outer" class="assign-form forms-custom">

                    
                    <div class="assign-form-content">
                        <div class="row close-div"><span class="close" id="close-assign-form" data-form="${ticketID}">&times;</span></div>
                        <div class="row">
                            <h5 class="info-card-title">${ticket['Ticket Title']}</h5>
                            <hr>
                        </div>
                        <form id="assign-form-inner">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="hashtag hashtag-due-date">Due Date</span>
                                    <div id="set-due-date" class="input-group date">
                                        <input name=dueDate type="text" class="form-control" required><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <span class="hashtag hashtag-assign-to">Assign To</span>
                                    <div>
                                        <select class="option-wrapper" name="ticketAssignedToID"required>
                                            
                                            ${await genEditHTMLforProjectUser(ticket['Project ID'], 'xxxxx')}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper">
                                <input type="submit" value="submit" id="submit-assign-form" class="btn btn-primary btn-md">
                            </div>
                        </form>`


    insertHTML += `</div>

                    </div>`
    return insertHTML
}

function addListenerToCardBody(form) {
    if (form.dataset.form != 'filter-form') {
        let cardBody = document.querySelector(`.card-content[data-form="${form.dataset.form}"]`)
        cardBody.addEventListener('click', (event) => {

            form.style.display = "block"
            let temp = document.querySelectorAll(".image-in-card .carousel-indicators li, .carousel-control-prev-icon, .carousel-control-next-icon")
            for (const t of temp) {
                t.style.display = 'none';
            }
        })
    }
    console.log('run')
}
//show filter form / close
function showHidefilterForm() {
    let btnFilter = document.querySelector('#btn-filter');
    let filterForm = document.querySelector('#filter-form');
    let btnClose = document.querySelector('#close-filter');

    btnFilter.onclick = function () {
        filterForm.style.display = "block";
    };

    btnClose.onclick = function () {
        filterForm.style.display = "none";
        console.log("working")
    }
    window.addEventListener('click', (event) => {
        if (event.target == filterForm) {
            filterForm.style.display = "none";
            let cardContent = document.querySelector('#card-content')
            if (cardContent) {
                cardContent.parentNode.removeChild(cardContent);
            }
            processTickets();
        }
    })
}

async function getMembersInProject(projectID){
    const fetchRes = await fetch(`/api/v1/project/member/${projectID}`)
    const result = await fetchRes.json()
    console.log(result)
    return result.message
    // console.log(result.message)
}

async function genEditHTMLforProjectUser(projectID, currentName) {
    const infos = await getMembersInProject(projectID)
        // console.log(info.name)
        let insertHTML = ``
        oneTime = true
        for (let i = 0; i<infos.length; i++) {
            if ((currentName == 'null' || !currentName) && oneTime) {
                insertHTML += `<option value="" selected></option>`
                oneTime = false
                i--;
                continue;
            }
            if (currentName == infos[i].name) {
                insertHTML += `<option value="${infos[i].memberId}" selected>${infos[i].name}</option>`
            } else {
                insertHTML += `<option value="${infos[i].memberId}">${infos[i].name}</option>`
            }
        };
        return insertHTML
}
function genEditHTML(infos, currentName) {
        // console.log(info.name)
        let insertHTML = ``
        oneTime = true
        for (let i = 0; i<infos.length; i++) {
            if ((currentName == 'null' || !currentName) && oneTime) {
                insertHTML += `<option value="" selected></option>`
                oneTime = false
                i--;
                continue;
            }
            if (currentName == infos[i].name) {
                insertHTML += `<option value="${infos[i].id}" selected>${infos[i].name}</option>`
            } else {
                insertHTML += `<option value="${infos[i].id}">${infos[i].name}</option>`
            }
        };
        return insertHTML
}

async function toEditStatus(form) {
    const isOwner = isTicketOwner(form.dataset.form, USERID)
    const role = await getProjectRole(form.dataset.form)
    const isToBeAssigned = isTicketAssignedTo(form.dataset.form, USERID)
    const currentProjectID = result.result.filter((ticket)=> ticket['Ticket ID'] == form.dataset.form)[0]['Project ID']
    console.log(currentProjectID)
    console.log('uesrID: ' + USERID)
    console.log('isOwner: ' + isOwner)
    console.log('role: ' + role)
    console.log('istobeAssigned: ' + isToBeAssigned)

    // if(isowenr)
    if (role == 'project manager' || role == 'owner') {
        // tempMap = rightMap
        rightMap = { ...senior }
    } else if (role == 'junior developer') {
        // temp = rightMap
        rightMap = { ...developer }
    }

    if (isOwner) {
        // tempMap = rightMap
        for (const key of Object.keys(rightMap)) {
            if (rightMap[key] || owner[key]) {
                rightMap[key] = true
            }
        }
        console.log('isowner')

    }
    if (isToBeAssigned) {
        // tempMap = rightMap
        for (const key of Object.keys(rightMap)) {
            if (rightMap[key] || beAssigned[key]) {
                rightMap[key] = true
            }
        }

    }

    if (rightMap['criticalLevel']) {

        //critical level
        let criticalLevelinForm = form.querySelector(`#critical-level-${form.dataset.form}`)
        // let criticalLevelName = criticalLevelinForm.textContent.split(':')[1].replace(/\s/g, '');
        let insertHTML = genEditHTML(criticalLevelsInfo, criticalLevelinForm.innerHTML)
        $(`#critical-level-${form.dataset.form}`).replaceWith(`
        <select name= "ticketCriticalLevelID" id="inputCriticalLevel-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    if (rightMap['status']) {

        //status
        let statusinForm = form.querySelector(`#status-${form.dataset.form}`)
        // let statusName = statusinForm.textContent.split(':')[1].replace(/\s/g, '');
        insertHTML = genEditHTML(statusListInfo, statusinForm.innerHTML)
        $(`#status-${form.dataset.form}`).replaceWith(`
        <select name= "ticketStatusID" id="inputStatus-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    //difficulty
    // let difficultyinForm = form.querySelector(`#difficulty-${form.dataset.form}`)
    // let difficultyName = difficultyinForm.textContent.split(':')[1].replace(/\s/g, '');
    // insertHTML = genEditHTML(difficultiesInfo, difficultyName)
    // $(`#difficulty-${form.dataset.form}`).replaceWith(`<div class="detail-form-div"> <label class="edit-detail-form-label" for="inputDifficulty">Difficulty</label><select name= "ticketDifficultyID" id="inputDifficulty" class="form-control edit-detail-form" required>
    // ${insertHTML}</select></div>`);

    if (rightMap['category']) {
        //category
        let categoryinForm = form.querySelector(`#category-${form.dataset.form}`)
        // let categoryName = categoryinForm.textContent.split(':')[1].replace(/\s/g, '');
        insertHTML = genEditHTML(categoriesInfo, categoryinForm.innerHTML)
        $(categoryinForm).replaceWith(`<select name= "ticketCategoryID" id="inputCategory-${form.dataset.form}" class="form-control edit-detail-form" required>
                                            ${insertHTML}</select>`);
    }

    if (rightMap['dueDate']) {

        //dueDate
        let dueDateinForm = form.querySelector(`#due-date-${form.dataset.form}`)
        // let dueDateName = dueDateinForm.textContent.split(':')[1].replace(/\s/g, '');
        // console.log(new Date(dueDateName))

        // insertHTML = genEditHTML(, dueDateName)
        const defaultDueDate = dueDateinForm.innerHTML == 'null' ? '' : dueDateinForm.innerHTML
        $(dueDateinForm).replaceWith(`
            <div class="input-group date" id="due-date-${form.dataset.form}">
            <input type="text" id="input-due-date-${form.dataset.form}" class="form-control" value="${defaultDueDate}">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
            </div>`);

        $(`#due-date-${form.dataset.form}`).datepicker({
            todayBtn: true,
            clearBtn: true,
            autoclose: true,
            todayHighlight: true,

        });
    }

    if (rightMap['assignTo']) {

        //assign to name
        let assignToinForm = form.querySelector(`#assign-to-name-${form.dataset.form}`)
        // let assignToName = assignToinForm.textContent.split(':')[1].replace(/\s/g, '');
        console.log(assignToinForm.innerHTML)
        insertHTML = await genEditHTMLforProjectUser(currentProjectID, assignToinForm.innerHTML)
        $(assignToinForm).replaceWith(`<select name= "ticketAssignToID" id="inputAssignTo-${form.dataset.form}" class="form-control edit-detail-form" required>
        ${insertHTML}</select>`);
    }

    if (rightMap['title']) {

        //title
        let title = form.querySelector('.info-card-title');
        title.innerHTML = `<input type="text" id="inputTitle-${form.dataset.form}" class="form-control form-rounded" value="${title.innerHTML}">`
    }

    if (rightMap['description']) {

        //description
        let description = form.querySelector(`.card-text`)
        $(description).replaceWith(`<div class="col-md-10"><div class="form-group">
                <label for="inputDescription"></label>
                <textarea class="form-control" id="inputDescription-${form.dataset.form}" rows="5">${description.innerHTML}</textarea>
            </div></div>`)
    }

    if (rightMap['delete']) {

        const deleteHTML = `<button class="delete-btn" data-form="${form.dataset.form}">
                                                <i class="far fa-trash-alt"></i>
                                            </button>`
        form.querySelector(".close-div").insertAdjacentHTML('afterbegin', deleteHTML)

        form.querySelector(".delete-btn").addEventListener('click', async () => {
            if (confirm("Are You Sure To Delete the Ticket ?")) {

                await fetch('/api/v1/ticket', {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ 'ticketID': form.dataset.form })
                })

                form.remove()
                main();
            }
        })
    }

    rightMap = tempMap



}

function addListenerToCardsAndWindow() {
    let forms = document.querySelectorAll(".forms-custom[data-form]")
    for (const form of forms) {
        let btnClose = document.querySelector(`.close[data-form="${form.dataset.form}"]`)
        let btnEdit = document.querySelector(`.edit-btn[data-form="${form.dataset.form}"]`)
        // console.log(btnClose)
        btnClose.onclick = function () {
            form.style.display = "none";
            console.log("working")
        }
        btnEdit.addEventListener('click', () => {

            toEditStatus(form)

            form.dataset.editMode = true;


        })
        //except for filter form
        addListenerToCardBody(form);

        window.addEventListener('click', async (event) => {
            if (event.target == form) {
                // if ()
                console.log(result.result)
                // console.log()

                console.log(form.dataset.form)
                if (form.dataset.editMode) {
                    const ticket = result.result.filter(ticket => ticket['Ticket ID'] == form.dataset.form)[0]

                    console.log(ticket)
                    //compare edited? information
                    // console.log($(`#inputCriticalLevel-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputStatus-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputCategory-${form.dataset.form} option:selected`).val())
                    // console.log($(`#inputAssignTo-${form.dataset.form} option:selected`).val())
                    // console.log($(`#input-due-date-${form.dataset.form}`).val())
                    // console.log($(`#inputTitle-${form.dataset.form}`).val())
                    // console.log($(`#inputDescription-${form.dataset.form}`).val())
                    isChanged = false;
                    if ($(`#inputCriticalLevel-${form.dataset.form} option:selected`).text() != ticket['Ticket Critical Level']) {
                        console.log('changed critical level');
                        isChanged = true;
                    }
                    if ($(`#inputStatus-${form.dataset.form} option:selected`).text() != ticket['Ticket Status']) {
                        console.log('changed Status');
                        isChanged = true;
                    }
                    if ($(`#inputCategory-${form.dataset.form} option:selected`).text() != ticket['Ticket Category']) {
                        console.log('changed Category');
                        isChanged = true;
                    }
                    if ($(`#inputAssignTo-${form.dataset.form} option:selected`).val() != ticket['Ticket Assigned To User ID'] && $(`#inputAssignTo-${form.dataset.form} option:selected`).val()) {
                        console.log($(`#inputAssignTo-${form.dataset.form} option:selected`).val())
                        console.log('changed Assigned To Name');
                        isChanged = true;
                    }
                    if ($(`#input-due-date-${form.dataset.form}`).val() != moment(new Date(ticket['Ticket Due Date'])).format('MM/DD/YYYY') && $(`#input-due-date-${form.dataset.form}`).val()) {
                        console.log('changed Due Date');
                        isChanged = true;
                    }
                    if ($(`#inputTitle-${form.dataset.form}`).val() != ticket['Ticket Title']) {
                        console.log('changed Title');
                        isChanged = true;
                    }
                    if ($(`#inputDescription-${form.dataset.form}`).val() != ticket['Ticket Description']) {
                        console.log('changed Description');
                        isChanged = true;
                    }
                    console.log('ticketid: ' + ticket['Ticket ID'])
                    if (isChanged) {
                        const data = {
                            ticketID: ticket['Ticket ID'],
                            category: $(`#inputCategory-${form.dataset.form} option:selected`).text(),
                            difficulty: ticket['Ticket Difficulty'],
                            criticalLevel: $(`#inputCriticalLevel-${form.dataset.form} option:selected`).text(),
                            dueDate: $(`#input-due-date-${form.dataset.form}`).val(),
                            title: $(`#inputTitle-${form.dataset.form}`).val(),
                            description: $(`#inputDescription-${form.dataset.form}`).val(),
                            status: $(`#inputStatus-${form.dataset.form} option:selected`).text(),
                            ticketAssignedToID: $(`#inputAssignTo-${form.dataset.form} option:selected`).val()
                        }
                        //if assign to null, isAssignToNull = true
                        if($(`#inputStatus-${form.dataset.form} option:selected`).text().toLowerCase() == 'unassigned'){
                            console.log('work')
                            data['isAssignToNull'] = true;
                            // data['status'] = 'open';
                        }else{
                            if(!$(`#inputAssignTo-${form.dataset.form} option:selected`).val()){
                                alert("Not yet assign the tickets to others !!!!!")
                            }
                        }
                        
                        await fetch('/api/v1/ticket', {
                            method: 'PUT',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(data)
                        })
                        main();
                    }

                }



                form.style.display = "none";
                let cardContent = document.querySelector('#card-content')
                if (cardContent) {
                    cardContent.parentNode.removeChild(cardContent);
                }
                processTickets();
                let temp = document.querySelectorAll('.image-in-card .carousel-indicators li')
                for (const t of temp) {
                    t.style.display = 'block';
                }

            }

        });
        // console.log('forms: ' + form)
    }
}

async function dragCards() {
    const draggables = document.querySelectorAll(".draggable")
    const cardColumns = document.querySelectorAll(".cards-col")
    // console.log(cardColumns)
    for (const draggable of draggables) {
        const fromColTitle = draggable.parentNode.querySelector('h3').textContent
        const fromCol = draggable.parentNode.parentNode.querySelector(`.cards-col[data-col-for='${fromColTitle}']`)
        console.log(fromCol)
        let fromNextEle = draggable.nextSibling
        // if(!fromNextEle.querySelector('.card')){
        //     fromNextEle=null
        // }
        // console.log(fromCol)
        draggable.addEventListener('dragstart', () => {
            draggable.classList.add("dragging")
            console.log('drag start')
        })

        draggable.addEventListener('dragend', async () => {
            let toColTitle = draggable.parentNode.querySelector('h3').textContent.toLowerCase()
            let ticketID = draggable.querySelector('.card-content').dataset.form

            console.log(ticketID)
            if (toColTitle.toLowerCase() == fromColTitle.toLowerCase()) {
                //do nothing
                console.log('drag no change')
            } else {
                toColTitle = toColTitle.toLowerCase();
                if (confirm(`Change Ticket Status from \n${fromColTitle} to ${toColTitle}?`)) {
                    console.log(fromColTitle)

                    if (fromColTitle.toLowerCase() == 'unassigned') {
                        document.body.insertAdjacentHTML('beforeend', await genAssignForm(ticketID))

                        $('#set-due-date.input-group.date').datepicker({
                            todayBtn: true,
                            clearBtn: true,
                            autoclose: true,
                            todayHighlight: true
                        });
                        let assignFormOuter = document.querySelector("#assign-form-outer");
                        let assignFormInner = document.querySelector("#assign-form-inner")
                        // document.querySelector('#submit-assign-form').addEventListener('click', (event)=>{
                        //     event.preventDefault();
                        // })
                        assignFormInner.addEventListener("submit", async (event) => {
                            event.preventDefault()
                            if (confirm('Do you confirm to submit the changes?')) {

                                const t = event.currentTarget;
                                console.log(t)
                                event.preventDefault();
                                const formData = new FormData(t)
                                console.log('toCol: ' + toColTitle)
                                formData.append('status', toColTitle)
                                formData.append('ticketID', ticketID)
                                await fetch(`/api/v1/ticket`, {
                                    method: 'PUT',
                                    body: formData
                                })
                                assignFormOuter.remove();
                                main();
                                // console.log(assignForm)
                            }
                        })
                        document.querySelector("#close-assign-form").addEventListener('click', (e) => {
                            let btn = e.currentTarget
                            if (fromNextEle) {
                                console.log(1)
                                fromCol.insertBefore(draggable, fromNextEle)
                            } else {
                                console.log(2)
                                fromCol.appendChild(draggable)
                            }
                            // assignFormOuter.remove()
                            // assignFormOuter.style.display = "none";
                            console.log("assign form close btn working")
                            // btn.removeEventListener('click');
                            document.querySelector("#assign-form-outer").remove();
                            // main();
                        })

                    } else {
                        // const formData = new FormData();

                        const formData = new FormData()
                        console.log('toCol: ' + toColTitle)

                        if (toColTitle.toLowerCase() == 'unassigned') {
                            formData.append('isAssignToNull', true)
                            // toColTitle = 'open'
                        }
                        formData.append('status', toColTitle)
                        formData.append('ticketID', ticketID)
                        
                        await fetch(`/api/v1/ticket`, {
                            method: 'PUT',
                            body: formData
                        })
                        main()
                    }

                } else {
                    console.log(fromNextEle)
                    console.log(draggable)
                    if (fromNextEle) {
                        console.log(1)
                        fromCol.insertBefore(draggable, fromNextEle)
                    } else {
                        console.log(2)
                        fromCol.appendChild(draggable)
                    }
                }
            }
            draggable.classList.remove("dragging")
        })
    }

    for (const col of cardColumns) {

        col.addEventListener('dragover', (e) => {
            const afterElement = getDragAfterElement(col, e.clientY)
            const draggable = document.querySelector('.dragging')
            if (afterElement == null) {
                col.appendChild(draggable)
            } else {
                col.insertBefore(draggable, afterElement)
            }
        })
    }
}



function getDragAfterElement(col, y) {
    const draggableElements = [...col.querySelectorAll('.draggable:not(.dragging)')]
    // console.log(draggableElements)
    return draggableElements.reduce((closest, child) => {
        const box = child.getBoundingClientRect()
        // console.log(box)
        const offset = y - box.top - box.height / 2
        if (offset < 0 && offset > closest.offset) {
            return { offset: offset, element: child }
        } else {
            return closest
        }
    }, { offset: Number.NEGATIVE_INFINITY }).element
}

function jqueryCode() {
    //date picker
    $(".input-group.input-daterange").datepicker({
        todayBtn: true,
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    });

    $('.input-daterange input').each(function () {
        $(this).datepicker('clearDates');
    });

    $("#myInput").on("keyup", function () {
        const ori = $(this).val()
        var value = $(this).val().toLowerCase();
        console.log('in new func')
        $(".card").filter(function () {
            if ($(this).text().toLowerCase().indexOf(value) > -1){

                $(this).toggle(true)
                // $(this).mark($(ori))
            }else{
                $(this).toggle(false)

            }
            console.log($(this).find('.card'))
            
        });
    });
    // $("#myInput").on("keyup", function() {
    //     var g = $(this).val().toLowerCase();
    //     $(".card *").each(function() {
    //         var s = $(this).text().toLowerCase();
    //         $(this).closest('.card')[ s.indexOf(g) !== -1 ? 'show' : 'hide' ]();
    //     });
    // });​

    function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("#myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
      
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }

    // $('#set-due-date').datepicker();

    // $('.input-daterange input').each(function () {
    //     $(this).datepicker('clearDates');
    // });

}
async function main() {
    if(document.querySelector('#filter-form')){
        document.querySelector('#filter-form').remove();
    }
    await getUser();
    await getMyTickets();
    await getInfo();
    insertFirstBar();
    filterForm();
    await processTickets();
    showHidefilterForm();
    addListenerToSelect();

    // detailInfoForm();
    jqueryCode();
    // insertCards();
    $('select').selectpicker();

    console.log('Project Role:  ' + console.log(JSON.stringify(getProjectRole(1))))
}

main();
