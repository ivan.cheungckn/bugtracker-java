let result;
let opens;
let inProgresses;
let closeds;
let unassigneds;

async function getMyTickets() {
  const fetchRes = await fetch("/api/v1/ticket");
  result = await fetchRes.json();
  opens = result.result.filter((ticket) => ticket["Ticket Status"] == "open");
  inProgresses = result.result.filter(
    (ticket) => ticket["Ticket Status"] == "in progress"
  );
  closeds = result.result.filter(
    (ticket) => ticket["Ticket Status"] == "closed"
  );

  console.log(open);
}

function fitInTickets() {
  let insertHTML = "";
  insertHTML += `<div class="container">
                        <div class="row">
                        <div class="col-md-4">
                            <h3>Open</h3>`;
  insertHTML += genCard(opens);

  insertHTML += `</div>
                <div class="col-md-4">
                    <h3>In Progress</h3>`;
  insertHTML += genCard(inProgresses);
  insertHTML += `</div>
                    <div class="col-md-4">
                    <h3>Closed</h3>`;
  insertHTML += genCard(closeds);
  insertHTML += `</div>
                </div>
                </div>`;
  document.querySelector("#main-content").innerHTML = insertHTML;
}

function genCard(tickets) {
  let insertHTML = "";
  for (const ticket of tickets) {
    insertHTML += `<div class="card">
            <div class="card-body">
                <h5 class="card-title">${ticket["Ticket Title"]}</h5>
                <p class="card-text">${ticket["Ticket Description"]}</p>
            </div>
            <div class="card-footer">
                
                <small class="hashtag hashtag-project-name">${ticket["Project Name"]}</small>
                <small class="hashtag hashtag-category">${ticket["Ticket Category"]}</small>
                <small class="hashtag hashtag-critical-level">${ticket["Ticket Critical Level"]}</small>`;
    if (ticket["Ticket Due Date"]) {
      insertHTML += `<small class="hashtag hashtag-due-date">${new Date(
        ticket["Ticket Due Date"]
      ).toLocaleDateString()}</small>`;
    }
    insertHTML += `</div>
        </div>`;
  }
  return insertHTML;
}

async function main() {
  await getMyTickets();
  fitInTickets();
}

main();
//for attach js
//q = visiting which page
//info = get what information
