const commentWrapper = document.querySelector("#comment-wrapper");
const apiVersion = `api/v1`;
const preRoute = ``;
const route = `${preRoute}/${apiVersion}/comment`;
const projectRoute = `${preRoute}/${apiVersion}/project`;
const parsedUrl = new URL(window.location.href);
const ticketId = parsedUrl.searchParams.get("ticketId");
const projectId = parsedUrl.searchParams.get("projectId");
let user = {};
const commentHeaderContainer = document.querySelector('#comment-header-container');

run();
async function run() {
  user.id = await getUser();
  await getProjectRole();
  getComments();
  console.log(user);
}

async function getUser() {
  const res = await fetch(`/${apiVersion}/users/info`);
  const result = await res.json();
  if (result.status) {
    const member = result.message;
    return member.userId;
  } else {
    window.alert(result.message);
    location.replace("/login.html");
    //throw new Error(result.message);
  }
}

async function getProjectRole(){
    const res = await fetch(`/${apiVersion}/project/member?projectId=${projectId}`);
    const result = await res.json();
    if (result.status) {
      return true;
    } else {
      window.alert(result.message);
      location.replace("/project/myProject.html");
      //throw new Error(result.message);
    }
}
/* get comments from DB  then run display(comments) to show all the comments */
async function getComments() {
    try {
        const res = await fetch(`${route}/${ticketId}`);
        const result = await res.json();
        const searchContainer = document.querySelector('#search-container');
        searchContainer.innerHTML="";
        searchContainer.innerHTML = `<input class = "border-bottom border-0 bg-transparent" type="text" id="search-from-parent-comments" name="search-from-all-members" placeholder="Search">`
        const searchBar = searchContainer.querySelector('#search-from-parent-comments');
        searchBar.value = "";
        const searchBarEvent = (event)=>{
            const searchValue = event.target.value.toLowerCase();
            const arr = result.message.filter((x)=>x.content.toLowerCase().includes(searchValue));
            commentWrapper.innerHTML = "";
            display(arr);
        }
        searchBar.addEventListener('keyup',searchBarEvent);
        commentHeaderContainer.innerHTML = "";
        commentHeaderContainer.append(constructCommentArea("",true));
        commentWrapper.innerHTML = "";
        if (result.status === true) {
            console.log(result.message);
            display(result.message);
        } else {
            console.error(`Error: ${result.message}`);
            window.alert(result.message);
        }
    } catch (e) {
        console.error(e);
        window.alert(e);
    }
}


function display(comments) {
    // comments.sort((a,b)=>Date.parse(b.updateDate)-Date.parse(a.updateDate));
    comments.sort((a, b) => b.commentId - a.commentId);
    for (const comment of comments) {
        const temp = constructCommentRecursive(comment);
        commentWrapper.append(temp);
    }
}
function constructCommentRecursive(comment) {
    if (comment.comments.length === 0) {
        return constructComment(comment);
    } else {
        let parent = constructComment(comment);
        // comment.comments.sort((a,b)=>Date.parse(b.updateDate)-Date.parse(a.updateDate));
        comment.comments.sort((a, b) => b.commentId - a.commentId);
        for (const c of comment.comments) {
            const temp = constructCommentRecursive(c);
            parent.append(temp);
        }
        return parent;
    }
}
/* This function construct the reply area/new comment area */
function constructCommentArea(commentId, isHeader) {
    //text area
    const outerDiv = document.createElement('div');
    outerDiv.className = "mb-4";
    const textArea = constructHtmlElement('textArea', [], "newComment", "", "");
    textArea.setAttribute("placeholder", "Leave your comment here...");
    const utilsWrapper = constructHtmlElement('div', ["d-none","utilsWrapper"], "", "", "");
    const utilsContainer = constructHtmlElement('div', ["d-flex"], "utilsContainer", "", "");
    const uploadFileFormInnerHtml = `<span class="bg-transparent mr-1">Upload: </span><input type="file" id="new" name="file" multiple>`;
    const uploadFileForm = constructHtmlElement('form', ["px-2", "text-gray-600", "text-xs"], `uf-new`, uploadFileFormInnerHtml, "");
    const clearButtonInnerHtml = `<i class="fas fa-cut"></i>`;
    const clearButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", clearButtonInnerHtml, "");
    const saveButtonInnerHtml = `<i class="fas fa-cloud-upload-alt"></i>`;
    const saveButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", saveButtonInnerHtml, "");
    const cancelButtonInnerHtml = `<i class="fas fa-ban"></i>`;
    const cancelButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", cancelButtonInnerHtml, "");
    utilsContainer.append(saveButton);
    utilsContainer.append(clearButton);
    utilsContainer.append(cancelButton);
    utilsWrapper.append(utilsContainer);
    utilsWrapper.append(uploadFileForm);
    outerDiv.append(textArea);
    outerDiv.append(utilsWrapper);
    if (isHeader) {
        textArea.addEventListener('focus', (event) => {
            if (!textArea.classList.contains('d-none')) {
                textArea.classList.add('edit-comment-1');
            }
            utilsWrapper.classList.remove('d-none');
        });
    }else{
        if (!textArea.classList.contains('d-none')) {
            textArea.classList.add('edit-comment-1');
        }
        utilsWrapper.classList.remove('d-none');
    }

    clearButton.addEventListener('click', (event) => {
        textArea.value = "";
    })
    cancelButton.addEventListener('click', (event) => {
        if (confirm('Are you sure you want to discard your comment?')) {
            if (!utilsWrapper.classList.contains('d-none')) {
                utilsWrapper.classList.add('d-none')
            }
            textArea.classList.remove('edit-comment-1');
            textArea.value = "";
            uploadFileForm.innerHTML = uploadFileFormInnerHtml;
            if(isHeader === false){
                const replyArea = document.querySelector('#reply-area');
                if(!replyArea.classList.contains('d-none')){
                    replyArea.classList.add('d-none');
                }
                replyArea.classList.remove('d-flex');
                replyArea.innerHTML="";
            }
        }
    })
    saveButton.addEventListener('click', async (event) => {
        console.log('abc');
        const formData = new FormData(uploadFileForm);
        if (textArea.value.trim().length > 0) {
            console.log(event.target.dataset.id);
            //const commentId = event.target.dataset.id!==""?event.target.dataset.id:"";
            formData.append("ticketId", `${ticketId}`);
            formData.append("commentId", `${commentId}`);
            formData.append("content", `${textArea.value.trim()}`);
            const res = await fetch(`${route}/`, {
                method: "POST",
                body: formData
            });
            const result = await res.json();
            if (result.status === false) {
                console.error(result.message);
                window.alert(result.message);
            }
            getComments();
            if(isHeader === false){
                const replyArea = document.querySelector('#reply-area');
                if(!replyArea.classList.contains('d-none')){
                    replyArea.classList.add('d-none');
                }
                replyArea.classList.remove('d-flex');
                replyArea.innerHTML="";
            }
        } else {
            window.alert('Comment cannot be empty');
        }

    })

    //upload
    //clear
    //save
    return outerDiv;
}
/* The core of constructing a comment */
function constructComment(comment) {
    const isParent = comment.comments.length > 0 ? true : false;
    const isChild = comment.parentCommentId ? true : false;

    const collapseId = isParent ? `c-${comment.commentId}` : "";
    const parentCommentId = isChild ? `c-${comment.parentCommentId}` : "";

    const outerDiv = constructHtmlElement('div', ["border-left-info", "pl-2", "mb-4", "border", "shadow-sm"], `${parentCommentId}`, "", "");
    const contentContainer = constructHtmlElement('div', ["d-flex"], "", "", "");
    const content = constructHtmlElement('div', ["d-flex", "align-items-center"], "", "", "");
    const buttonContainer = constructHtmlElement('div', ["d-flex", "align-items-center"], "", "", "");
    const collapseButton = constructHtmlElement('div', ["pl-2"], "", "", "");
    const fileContainer = document.createElement('div');
    const replyButton = constructHtmlElement('div', ["pl-2", "text-gray-600"], "", `<i class="fas fa-reply"></i>`,comment.commentId);
    replyButton.addEventListener('click',(event)=>{
        const replyArea = constructCommentArea(comment.commentId,false);
        const fixedBottom = document.querySelector('#reply-area');
        const replyingTo = constructHtmlElement('div',"","",`Replying to ${comment.commenter}`,"");
        const replyingToContent = constructHtmlElement('div',"","",`Comment: ${comment.content}`,"");
        fixedBottom.innerHTML="";
        fixedBottom.append(replyingTo);
        fixedBottom.append(replyingToContent);
        fixedBottom.classList.add('bg-white','p-4');
        fixedBottom.append(...replyArea.childNodes);
        const txtArea = fixedBottom.querySelector('textarea');
        wordsArr = comment.content.split(' ');
        console.log(wordsArr);
        //const placeholder = wordsArr.length>3?wordsArr.splice(0,3).join(" ").concat("..."):wordsArr.join(" ");
        // txtArea.setAttribute("placeholder", `${placeholder}`);
        txtArea.setAttribute("placeholder", `Leave your comment here...`);

       if(!fixedBottom.classList.contains('d-flex')){
           fixedBottom.classList.add('d-flex');
       }
        fixedBottom.classList.remove('d-none');
    })
 
    const isEdit = comment.updateDate!==comment.createDate?"[Edited]":"";
    const xInnerHtml = `<span class="custom-comment">${comment.content}</span> <span class="text-xs text-gray-600">By ${comment.commenter} ${isEdit} [${new Date(comment.updateDate).toLocaleString()}]</span>`;
    const x = constructHtmlElement('span', ["py-2", "edit-textarea"], "", xInnerHtml, "");

    if (isChild) {
        outerDiv.classList.add("collapse", "pl-5");
    }
    content.append(x);
    if (isParent) {
        const collapseIconHtml = `<i class="fas fa-caret-down fa-2x"></i>`;
        const collapseClass = `data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"`
        let buttonInnerHtml = `<a href="#${collapseId}"  class=" text-xs text-gray-600" ${collapseClass}>${collapseIconHtml}</a>`;
        collapseButton.innerHTML = isParent ? buttonInnerHtml : "";
    }

    /* file */
    const uploadFileFormInnerHtml = `<span class="bg-transparent mr-1">Upload: </span><input type="file" id="file-${comment.commentId}" name="file" multiple>`;
    const uploadFileForm = constructHtmlElement('form', ["px-2", "text-gray-600", "text-xs", "d-none"], `uf-${comment.commentId}`, uploadFileFormInnerHtml, comment.commentId);
    const uploadFileInput = uploadFileForm.querySelector('input');
    comment.files.sort((a, b) => b.commentFileId - a.commentFileId);
    if (user.id === comment.userId) {
        fileContainer.append(uploadFileForm);
    }
    for (const file of comment.files) {
        const tempInnerHtmlDownLoad = `<a class = "text-xs mr-1" href="/${file.fileName}"  download><i class="fas fa-file-download"></i></a>`;
        const tempInnerHtml = `${tempInnerHtmlDownLoad}<a class = "text-xs" href="/${file.fileName}"  target=_blank>${file.fileName}</a>`;
        const temp = constructHtmlElement('div', ["px-2"], "", tempInnerHtml, file.commentFileId);
        temp.dataset.fileName = file.fileName;
        /* file remove button */
        if (user.id === comment.userId) {
            const iconHtml = `<i class="far fa-times-circle fa-sm"></i>`;
            const deleteButton = constructHtmlElement('a', ["text-gray-800", "ml-2", "file-remove", "d-none"], "", iconHtml, "");
            deleteButton.href = "#";
            deleteButton.addEventListener('click', async (event) => {
                event.target.parentNode.classList.add('d-none');
            });
            temp.append(deleteButton);
        }
        fileContainer.append(temp);
    }
    /* edit button */
    if (user.id === comment.userId) {

        const clearButtonInnerHtml = `<i class="fas fa-cut"></i>`;
        const clearButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", clearButtonInnerHtml, "");
        const saveButtonInnerHtml = `<i class="fas fa-cloud-upload-alt"></i>`;
        const saveButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", saveButtonInnerHtml, comment.commentId);
        const deleteCommentButtonInnerHtml = `<i class="fas fa-trash-alt"></i>`;
        const cancelButtonInnerHtml = `<i class="fas fa-ban"></i>`;
        const deleteCommentButton = constructHtmlElement('div', ["px-2", "text-gray-600", "d-none"], "", deleteCommentButtonInnerHtml, comment.commentId);

        const cancelButton = constructHtmlElement('div', ["px-2", "text-gray-600"], "", cancelButtonInnerHtml, "");
        const containerForDC = document.createElement('div');
        //deleteCommentButton.classList.add("px-2", "text-gray-600");
        containerForDC.className = "d-flex";
        containerForDC.append(saveButton);
        containerForDC.append(clearButton);
        containerForDC.append(cancelButton);

        const iconHtml = `<i class="fas fa-edit"></i>`;
        const editButton = constructHtmlElement('div', ["text-gray-600", "pl-2", "text-xs"], "", iconHtml, "");

        editButton.addEventListener('click', (event) => {
            console.log('edit');
            if (!event.target.classList.contains('d-none')) {
                editButton.classList.add('d-none');
            }
            const fileRemoves = fileContainer.querySelectorAll('a.file-remove');
            const fileRows = fileContainer.querySelectorAll('div');
            for (const removeButton of fileRemoves) {
                removeButton.classList.remove('d-none');
            }
            uploadFileForm.classList.remove('d-none');
            deleteCommentButton.classList.remove('d-none');
            const textArea = document.createElement('textarea');
            textArea.className = "edit-comment-1";
            textArea.value = comment.content;
            const backup = x.innerHTML;
            const backup2 = x.childNodes[0].innerHTML;
            x.innerHTML = "";
            if (!content.classList.contains("col-10")) {
                content.classList.add("col-lg-6", "col-10");
            }
            x.append(textArea);
            x.append(containerForDC);
            deleteCommentButton.addEventListener('click', deleteEvent);
            saveButton.addEventListener('click', saveEvent);
            cancelButton.addEventListener('click', cancelEvent);
            clearButton.addEventListener('click', clearEvent);

            function cancelEvent() {
                x.innerHTML = backup;
                editButton.classList.remove('d-none');
                if (!deleteCommentButton.classList.contains('d-none')) {
                    deleteCommentButton.classList.add('d-none');
                }
                for (const removeButton of fileRemoves) {
                    if (!removeButton.classList.contains('d-none')) {
                        removeButton.classList.add('d-none');
                    }
                }
                for (const div of fileContainer.querySelectorAll('div')) {
                    div.classList.remove('d-none');
                }
                if (!uploadFileForm.classList.contains('d-none')) {
                    uploadFileForm.classList.add('d-none');
                }
                content.classList.remove("col-lg-6", "col-10");
                uploadFileForm.innerHTML = uploadFileFormInnerHtml;
                cancelButton.removeEventListener('click', cancelEvent);
                deleteCommentButton.removeEventListener('click', deleteEvent);
                saveButton.removeEventListener('click', saveEvent);
                clearButton.removeEventListener('click', clearEvent);
            }

            function clearEvent(event) {
                event.target.parentNode.parentNode.childNodes[0].value = "";
            }

            async function deleteEvent(event) {
                if (confirm('Are you sure you want to delete this comment?')) {
                    const res = await fetch(`${route}/${event.target.dataset.id}`, { method: 'DELETE' });
                    const result = await res.json();
                    if (result.status === false) {
                        console.error(result.status);
                    }
                    getComments();
                }
            }

            async function saveEvent(event) {
                if (textArea.value.trim().length > 0) {
                    /* Update content */
                    if(textArea.value.trim() !== backup2){
                    
                    const updateCommentRes = await fetch(`${route}/${comment.commentId}`, {
                        method: 'PUT',
                        headers: {
                            "Content-Type": 'application/json'
                        },
                        body: JSON.stringify({ content: textArea.value })
                    });
                    const update = await updateCommentRes.json();
                    if (update.status === false) {
                        window.alert(`Update Content Fail: ${update.message}`);
                    }
                    }
                    /* Delete Files */
                    const deleteFileRows = fileContainer.querySelectorAll('div.d-none');
                    if (fileContainer.querySelectorAll('div.d-none').length > 0) {
                        //delete from database
                        const deleteArr = [];
                        for (const row of deleteFileRows) {
                            const obj = {
                                id: row.dataset.id,
                                fileName: row.dataset.fileName
                            }
                            deleteArr.push(obj);
                        }
                        const delObj = { files: deleteArr };
                        const res = await fetch(`${route}/files`, {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(delObj)
                        })
                        const result = await res.json();
                        if (result.status === false) {
                            window.alert(result.message);
                            console.error(`Delete files error: ${result.message}`);
                        }

                    }
                    /* Upload Files */
                    const input = uploadFileForm.querySelector('input');
                    if (input.files.length > 0) {

                        //upload files
                        const formData = new FormData(uploadFileForm);
                        console.log(formData.get('file'));
                        try {
                            const res = await fetch(`${route}/${comment.commentId}/files`, {
                                method: 'POST',
                                body: formData
                            });
                            const result = await res.json();
                            if (result.status === false) {
                                window.alert(result.message);
                            }

                        } catch (e) {
                            console.error(e);
                            window.alert("Fail to upload files: Exceed files upload uploading limit");
                        }

                    }

                    getComments();


                } else {
                    window.alert('Comment cannot be empty!');
                }
            }
        })
        buttonContainer.append(deleteCommentButton);
        buttonContainer.append(editButton);

    }


    buttonContainer.append(replyButton);
    buttonContainer.append(collapseButton);
    contentContainer.append(content);
    contentContainer.append(buttonContainer);
    outerDiv.append(contentContainer);
    outerDiv.append(fileContainer);

    return outerDiv;
}
/* utilities */
function constructHtmlElement(type, classes, id, innerHtml, data) {
    const div = document.createElement(`${type}`);
    div.classList.add(...classes);
    div.id = id;
    div.dataset.id = data;
    div.innerHTML = innerHtml;
    return div;
}

