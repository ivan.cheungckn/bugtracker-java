const statusContainer = document.querySelector("#status-container");
const apiVersion = `api/v1`;
const preRoute = ``;
const route = `${preRoute}/${apiVersion}/project`;
const formContainer = document.querySelector("form#project-form");
const projectName = document.querySelector(
  "form#project-form input[type=text]"
);
const projectDescription = document.querySelector("form#project-form textarea");
const icons = formContainer.querySelectorAll(".fas.fa-exclamation-circle");
let radioButtons;
getStatus();
async function getStatus() {
  const res = await fetch(`${route}/status`);
  const result = await res.json();
  if (result.status === true) {
    statusContainer.innerHTML = "";
    for (const status of result.message) {
      const temp = createRadioButtons(status.id, status.name);
      statusContainer.append(...temp.childNodes);
    }
  } else {
    console.error(e);
    window.alert(result.message);
  }
  console.log(icons);

  radioButtons = statusContainer.querySelectorAll("input[type=radio]");
  initEventListener();
}

function createRadioButtons(id, name) {
  const container = document.createElement("div");
  container.innerHTML = `<input type="radio" id="${name}" name="statusId" value="${id}">
    <label for="${name}">${name}</label><br>`;
  return container;
}

document
  .querySelector("form#project-form")
  .addEventListener("submit", async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    if (validation()) {
      const formData = new FormData(form);
      const res = await fetch(`${route}/`, {
        method: "POST",
        body: formData,
      });
      result = await res.json();
      if (result.status === true) {
        location.replace(`/project/project.html?projectId=${result.message}`);
      } else {
        console.error(result.message);
        window.alert(
          `${result.message}\n You may try to submit the form again`
        );
      }
    }
  });

function validation() {
  let isValid = true;
  let count = 0;
  if (
    projectName.value.trim().length === 0 ||
    projectDescription.value.trim().length === 0
  ) {
    isValid = false;
  }

  for (const button of radioButtons) {
    if (button.checked) {
      count = count + 1;
    }
  }
  if (count !== 1) {
    isValid = false;
  }
  if (isValid === false) {
    window.alert("Incomplete form");
  }
  return isValid;
}

function initEventListener() {
  projectName.addEventListener("keyup", (event) => {
    if (
      event.target.value.trim().length > 0 &&
      !icons[0].classList.contains("d-none")
    ) {
      icons[0].classList.add("d-none");
    } else if (event.target.value.trim().length === 0) {
      icons[0].classList.remove("d-none");
    }
  });
  projectDescription.addEventListener("keyup", (event) => {
    if (
      event.target.value.trim().length > 0 &&
      !icons[1].classList.contains("d-none")
    ) {
      icons[1].classList.add("d-none");
    } else if (event.target.value.trim().length === 0) {
      icons[1].classList.remove("d-none");
    }
  });
  for (const button of radioButtons) {
    button.addEventListener("click", () => {
      if (!icons[2].classList.contains("d-none")) {
        icons[2].classList.add("d-none");
      }
    });
  }
}
