const apiVersion = `api/v1`;
const preRoute = ``;
const route = `${preRoute}/${apiVersion}/users`;

async function getUserProjects(){
const res = await fetch(`${route}/projects`);
const result = await res.json();
if(result.status === true){
    return result.message;
}else{
    window.alert(result.message);
    console.error(result.message);
}
}
main();
async function main(){
    const projects = await getUserProjects();
    console.log(projects);
    const projectWrapper = document.querySelector('#project-wrapper');
    const searchContainer = document.querySelector('#search-container');
    searchContainer.innerHTML="";
    searchContainer.innerHTML = `<input class = "border-bottom border-0 bg-transparent" type="text" id="search-from-parent-comments" name="search-from-all-members" placeholder="Search">`
    const searchBar = searchContainer.querySelector('#search-from-parent-comments');
    searchBar.value = "";
    const searchBarEvent = (event)=>{
        const searchValue = event.target.value.toLowerCase();
        console.log(searchValue);
        const arr = projects.filter((x)=>x.projectName.toLowerCase().includes(searchValue));
        projectWrapper.innerHTML = "";
        display(arr);
    }
    searchBar.addEventListener('keyup',searchBarEvent);
    if(projects.length>0){
        display(projects);
    }else{
        projectWrapper.innerHTML = "You are not involved in any projects";
    }
    function display(projects){
        const header = constructHtmlElement('div',["d-flex","mb-2","flex-wrap"],"","","");
        const name = constructHtmlElement('div',["p-2","col-sm-4","col-6"],"","Project Name","");
        const description = constructHtmlElement('div',["p-2","col-sm-5","col-6"],"","Description","");
        const status = constructHtmlElement('div',["p-2","col-sm-3","col-4"],"","Status","");
        header.append(name,description,status);
        projectWrapper.append(header);
        for(const project of projects){
            const row = constructHtmlElement('div',["d-flex","shadow","mb-2","border", "flex-wrap"],"","","");
            const projectNameInnerHtml = `<a href = "/project/project.html?projectId=${project.projectId}">[${project.projectName}]</a>`
            const projectName = constructHtmlElement('div',["p-2","col-sm-4","col-6"],"",projectNameInnerHtml,"");
            wordsArr = project.description.split(' ');
            const projectDescriptionInnerHtml = wordsArr.length>10?wordsArr.splice(0,10).join(" ").concat("..."):wordsArr.join(" ");
            const projectDescription = constructHtmlElement('div',["p-2","col-sm-5","col-6"],"",projectDescriptionInnerHtml,"");
            const projectStatus = constructHtmlElement('div',["p-2","col-sm-3","col-4"],"",`${project.projectStatus}`,"");
            row.append(projectName,projectDescription,projectStatus);
            projectWrapper.append(row);
        }
    }
}

function constructHtmlElement(type, classes, id, innerHtml, data) {
    const div = document.createElement(`${type}`);
    div.classList.add(...classes);
    div.id = id;
    div.dataset.id = data;
    div.innerHTML = innerHtml;
    return div;
}