const apiVersion = `api/v1`;
const preRoute = ``;
const route = `${preRoute}/${apiVersion}/project`;
const parsedUrl = new URL(window.location.href);
const pjId = parsedUrl.searchParams.get("projectId");
const mainContent = document.querySelector('.container-fluid');
//const role = Object.freeze({ owner: 1, "project manager": 2, "senior developer": 3, "junior developer": 4 });
let role = {};
let projectStatus = {};
const ticketStatus = Object.freeze({ open: 1, unassigned: 2, "in progress": 3, closed: 4 });
//const projectStatus = Object.freeze({ development: 1, staging: 2, production: 3 });
let thisMemberInfo;
const membersContainer = document.querySelector('#members-collapse');
const deleteProjectButton = document.querySelector('#delete-project-button');
const loading = document.querySelector('#loading');
const addMembersButton = document.querySelector('#add-members');
const addMembersModal = document.querySelector('#add-members-modal');
const modalDialog = document.querySelector(`#modal-dialog-add-members`);
// run();
window.onload = async function () {
  

    projectStatus = await this.getStatus();
    role = await this.getRole();
    loading.classList.add('d-none');
    await this.displayProject();
    if (mainContent.classList.contains('d-none')) {
        mainContent.classList.remove('d-none');
    }
};
// async function run(){
//     projectStatus = await this.getStatus();
//     role = await this.getRole();
//     loading.classList.add('d-none');
//     await this.displayProject();
//     if (mainContent.classList.contains('d-none')) {
//         mainContent.classList.remove('d-none');
//     }
// }
/* Main */
async function displayProject() {
    try {

        thisMemberInfo = await getThisMember(pjId);
        const projectInfo = await getProjectInfoById(pjId);
        displayBasicProjectInfo(projectInfo, thisMemberInfo);
        if ((thisMemberInfo.roleId === role.owner || thisMemberInfo.roleId === role["project manager"])){
            addMembersButton.classList.remove('d-none');
            addMembersButton.addEventListener('click', addMembersEvent);
        }
        const tempDiv = constructMembers(projectInfo.members, thisMemberInfo, pjId, false);
        membersContainer.innerHTML = "";
        membersContainer.append(...tempDiv.childNodes);

        displayReportsData(projectInfo.tickets);

    }
    catch (e) {
        console.error(e);
    }

}


/* Get the categories of project status and role from database */
async function getStatus() {
    const res = await fetch(`${route}/status`);
    const result = await res.json();
    if (result.status === true) {
        let obj = {}
        for (const status of result.message) {
            obj[status.name] = status.id;
        }
        return obj
    } else {
        console.error(e);
        window.alert(result.message);
        window.replace('/login.html')
        return null;
    }
}
async function getRole() {
    const res = await fetch(`${route}/role`);
    const result = await res.json();
    if (result.status === true) {
        let obj = {}
        for (const role of result.message) {
            obj[role.name] = role.id;
        }
        return obj
    } else {
        console.error(e);
        window.alert(result.message);
        return null;
    }
}
/* Add click event listener to buttons that already exist in the html document */
deleteProjectButton.addEventListener('click', async (event) => {
    event.preventDefault();
    const res = await fetch(`${route}/${pjId}`, { method: 'DELETE' });
    const result = await res.json();
    window.alert(result.message)
    if (result.status === true) {
        location.replace("/project/myProject.html");
    }
})

async function addMembersEvent() {
    //get users who are not in this project
    const res = await fetch(`/api/v1/users/${pjId}`);
    const result = await res.json();
    const users = result.message;
    const body = addMembersModal.querySelector('.modal-body');
    const loading = addMembersModal.querySelector('#loading-add-members');
    loading.classList.remove('d-none');
    const searchBar = document.querySelector('#search-from-all-members');
    searchBar.value="";
    const searchBarEvent = (event) =>{
        loading.classList.remove('d-none');
        const searchValue = event.target.value.toLowerCase();
        const arr = users.filter((x)=>x.name.toLowerCase().includes(searchValue))
        body.innerHTML = "";
        loopThroughUsers(arr);
    }
    searchBar.addEventListener('keyup',searchBarEvent)
    
    body.classList.add('d-none');
    body.innerHTML = "";

    if (result.status === true) {
        //display users
       
        loopThroughUsers(users);

    } else {
        body.innerHTML = "Fail to retrieve users information!"
        console.error(result.message);
    }
    function loopThroughUsers(users){
        for (const user of users) {
            const userRow = constructHtmlElement("div", ["d-flex", "flex-wrap", "align-items-center", "fadeIn", "text-gray-800"], "", "", "");
            const name = constructHtmlElement("span", ["py-2", "col-3"], "", `${user.name}`, "");
            const username = constructHtmlElement("span", ["py-2", "col-5"], "", `${user.username}`, "");
            const addButton = constructHtmlElement("span", ["py-2", "col-12", "col-md-1"], "", `<i class="fas fa-plus-circle"></i>`, `${user.userId}`);
            const outerDiv = constructHtmlElement("div", ["py-2", "col-3"], `ar-${user.userId}-out`, "", "");
            outerDiv.append(createDropDownButtonForMemberRole(user.userId, 4, "junior developer", thisMemberInfo, true));
            userRow.append(name, username, outerDiv, addButton);
            body.append(userRow);

            addButton.addEventListener('click', async (event) => {
                const temp = document.querySelector(`#ar-${user.userId}-out`);
                //console.log(event.target.parentNode.childNodes[2].childNodes[0].childNodes[0].dataset.roleId);
                const roleId = temp.childNodes[0].childNodes[0].dataset.roleId
                const res = await fetch(`${route}/member?projectId=${pjId}&userId=${user.userId}&roleId=${roleId}`, { method: 'POST' });
                const result = await res.json();
                if (result.status === false) {
                    window.alert(result.message);
                    console.error(result.message);
                }
                searchBar.removeEventListener('keyup', searchBarEvent);
                document.querySelector('#save-member').click();
                addMembersEvent();
                //displayMembers(pjId,true,false);
            });
        }
        loading.classList.add('d-none');
        body.classList.remove('d-none');
    }
}


async function getUser() {
    const res = await fetch();
    const user = await res.json();
    return user;
}
/* User authentication  */
async function getThisMember(projectId) {
    const res = await fetch(`${route}/member?projectId=${projectId}`);
    const result = await res.json();
    if (result.status) {
        const member = result.message;
        return member;
    } else {
        window.alert(result.message);
        location.replace("/login.html");
        //throw new Error(result.message);
    }
}
async function getProjectInfoById(projectId) {
    const res = await fetch(`${route}/${projectId}`);
    const result = await res.json();
    if (result) {
        const projectInfo = result.message;
        return projectInfo;
    } else {
        throw new Error('Cannot get project Info!')
    }
}

/* project info related */
function displayBasicProjectInfo(projectInfo, member) {
    const projectName = document.querySelector('#project-name');
    projectName.innerHTML = projectInfo.projectName;
    const createdAtElement = document.querySelector('#project-created_by');
    createdAtElement.innerHTML = `Created By: ${projectInfo.owner} at <span class = "text-xs">${new Date(projectInfo.createdAt).toLocaleString()}</span>`;
    const projectDescription = document.querySelector('#project-description');
    projectDescription.innerHTML = `Description: ${projectInfo.description}`;
    const pjStatus = document.querySelector('#project-status');
    pjStatus.innerHTML = `Status: ${projectInfo.status}`;
    const lastUpdate = document.querySelector('#project-last-update');
    lastUpdate.classList.add('text-xs');
    lastUpdate.innerHTML = `Last update:${new Date(projectInfo.updatedAt).toLocaleString()}`;
    if (member.roleId === role.owner) {
        //add eventlistenr

        const trigger = document.querySelector('#trigger-edit-projectInfo');
        const editButtonsContainer = document.querySelector('#edit-buttons-container');
        const save = document.querySelector('#save-project');
        const cancel = document.querySelector('#cancel-project');
        const saveEvent = async () => {
            const textAreas = document.querySelectorAll('.edit-area');
            for (const textArea of textAreas) {
                if (textArea.value.trim().length === 0) {
                    window.alert('Project name and description cannot be empty!');
                    return;
                }
            }
            const statusId = document.querySelector('#projectStatus').dataset.id;
            const uploadObj = { projectName: textAreas[0].value.trim(), description: textAreas[1].value.trim(), statusId };
            const res = await fetch(`${route}/${pjId}`, {
                method: 'PUT',
                headers: {
                    "Content-Type": 'application/json'
                },
                body: JSON.stringify(uploadObj)
            });
            const result = await res.json();
            if (result.status === true) {
                trigger.removeEventListener('click', triggerEvent);
                cancel.removeEventListener('click', cancelEvent);
                save.removeEventListener('click', saveEvent);
                const projectInfo = await getProjectInfoById(pjId);
                displayBasicProjectInfo(projectInfo, thisMemberInfo);
                if (!editButtonsContainer.classList.contains('d-none')) {
                    editButtonsContainer.classList.add('d-none');
                }
                trigger.classList.remove('d-none');
            } else {
                console.error(result.message);
                window.alert(result.message);
            }
        }
        save.addEventListener('click', saveEvent)
        const cancelEvent = () => {
            projectName.innerHTML = projectInfo.projectName;
            projectDescription.innerHTML = `Description: ${projectInfo.description}`;
            pjStatus.innerHTML = `Status: ${projectInfo.status}`;
            if (!editButtonsContainer.classList.contains('d-none')) {
                editButtonsContainer.classList.add('d-none');
            }
            trigger.classList.remove('d-none');
        }
        cancel.addEventListener('click', cancelEvent);
        const triggerEvent = () => {
            if (!trigger.classList.contains('d-none')) {
                trigger.classList.add('d-none');
            }
            editButtonsContainer.classList.remove('d-none');
            const txtAreaName = createContainer(['edit-area'], projectName.innerHTML, 1);
            const subStringOfDescription = projectDescription.innerHTML.substring("Description: ".length);
            const txtAreaDescription = createContainer(['edit-area'], subStringOfDescription, 2);
            projectName.innerHTML = "";
            projectName.append(txtAreaName);
            projectDescription.innerHTML = "";
            projectDescription.append(txtAreaDescription);
            const statusSubStr = pjStatus.innerHTML.substring("Status: ".length);
            pjStatus.innerHTML = "";
            pjStatus.append(createDropDownButton(statusSubStr));
        }
        trigger.addEventListener('click', triggerEvent);
        if (trigger.classList.contains('d-none')) {
            trigger.classList.remove('d-none');
        }
    }

}
const createDropDownButton = (initialStr) => {
    const container = document.createElement('div');
    const button = document.createElement("button");
    button.classList.add("btn", "btn-primary", "dropdown-toggle");
    button.setAttribute("type", "button");
    button.setAttribute("data-toggle", "dropdown");
    button.setAttribute("aria-haspopup", "true");
    button.setAttribute("aria-expanded", "false");
    button.id = "projectStatus";
    button.innerHTML = initialStr;
    container.append(button);
    const status = document.createElement('div');
    status.classList.add("dropdown-menu", "animated--fade-in")
    status.setAttribute("aria-labelledby", "projectStatus");
    for (const st in projectStatus) {
        if (st == initialStr) {
            button.dataset.id = projectStatus[st];
            continue;
        }
        const temp = document.createElement('a');
        temp.innerHTML = `<a class="dropdown-item" href="#" data-id = "${projectStatus[st]}">${st}</a>`
        status.append(temp);
        temp.addEventListener('click', (event) => {
            const pjStatus = document.querySelector('#project-status');
            pjStatus.innerHTML = "";
            pjStatus.append(createDropDownButton(event.target.innerHTML));
        });
    }
    container.append(button);
    container.append(status);
    return container;
}
const createContainer = (classList, innerHtml, id) => {
    const container = document.createElement(`textarea`);
    container.classList.add(...classList);
    container.id = `edit-project-${id}`;
    container.innerHTML = innerHtml;
    return container;
}

/* Member related */
async function displayMembers(projectId) {
    const res = await fetch(`${route}/member/${projectId}`);
    const result = await res.json();
    if (result.status === true) {
        const temp = constructMembers(result.message, thisMemberInfo, projectId, false);
        membersContainer.innerHTML = "";
        membersContainer.append(...temp.childNodes);
    } else {
        console.error(result);
        window.alert(result.message);
    }
}
function addEventListenerToEditMembers(members, member, projectId) {

    if ((member.roleId === role.owner || member.roleId === role["project manager"])) {
        const trigger = document.querySelector('#trigger-edit-member');
        const actionButtons = document.querySelector('#action-button-member');
        const saveButton = document.querySelector('#save-member');
        const cancelButton = document.querySelector('#cancel-member');
        trigger.classList.remove('d-none');
        const saveEvent = async () => {
            const membersCollapse = document.querySelector('#members-collapse');
            const editMembers = membersCollapse.querySelectorAll('.member-edit');
            const editMembers2 = membersCollapse.querySelectorAll('.member-delete');
            const arr = [...editMembers];
            const arr2 = [...editMembers2];
            const validArr = arr.filter((x) => x.childNodes.length === 2);
            const updateMembers = validArr.map((elem) => {
                const obj = {
                    roleId: elem.childNodes[0].childNodes[0].childNodes[0].dataset.roleId,
                    memberId: elem.childNodes[0].childNodes[0].childNodes[0].dataset.memberId
                };
                return obj;
            });
            const deleteMembers = arr2.map((elem) => {
                return elem.childNodes[0].childNodes[0].childNodes[0].dataset.memberId;
            });
            const deleteIds = { memberIds: deleteMembers };
            const updateIds = { midRids: updateMembers };
            console.log(deleteIds);
            let updateResult = true;
            let deleteResult = true;
            if (updateMembers.length > 0) {
                const res = await fetch(`${route}/member?projectId=${pjId}`, {
                    method: 'PUT',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    body: JSON.stringify(updateIds)
                });
                updateResult = await res.json();
            }
            if (deleteMembers.length > 0) {
                const res = await fetch(`${route}/member?projectId=${pjId}`, {
                    method: 'DELETE',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    body: JSON.stringify(deleteIds)
                });
                deleteResult = await res.json();
            }
            if (updateResult.status === false && deleteResult.status === false) {
                window.alert(`Update: ${updateResult.message}\nRemove: ${deleteResult.message}`);
            }
            else if (updateResult.status === false) {
                window.alert(`Update: ${updateResult.message}`);
            }
            else if (deleteResult.status === false) {
                window.alert(`Remove: ${deleteResult.message}`);
            }
            trigger.removeEventListener('click', triggerEvent);
            saveButton.removeEventListener('click', saveEvent);
            cancelButton.removeEventListener('click', cancelEvent);
            if (!actionButtons.classList.contains('d-none')) {
                actionButtons.classList.add('d-none');
            }
            displayMembers(pjId);
            addMembersButton.classList.remove('d-none');
        }
        saveButton.addEventListener('click', saveEvent);
        const cancelEvent = () => {
            if (!actionButtons.classList.contains('d-none')) {
                actionButtons.classList.add('d-none');
            }

            trigger.removeEventListener('click', triggerEvent);
            saveButton.removeEventListener('click', saveEvent);
            cancelButton.removeEventListener('click', cancelEvent);
            const temp = constructMembers(members, member, projectId, false);
            trigger.classList.remove('d-none');
            addMembersButton.classList.remove('d-none');
            membersContainer.innerHTML = "";
            membersContainer.append(...temp.childNodes);
            addMembersButton.classList.remove('d-none');
        }
        cancelButton.addEventListener('click', cancelEvent);

        const triggerEvent = () => {
            if (!trigger.classList.contains('d-none')) {
                trigger.classList.add('d-none');
            }
            actionButtons.classList.remove('d-none');
            // if(!addMembersButton.classList.contains('d-none')){
            //     addMembersButton.classList.add('d-none');
            // }
            const temp = constructMembers(members, member, projectId, true);
            membersContainer.innerHTML = "";
            membersContainer.append(...temp.childNodes);
            addMembersButton.classList.add('d-none');
        }
        trigger.addEventListener('click', triggerEvent);

    }
}
function constructMembers(members, member, projectId, isEdit) {
    if (!isEdit) {
        addEventListenerToEditMembers(members, member, projectId);
    }
    members.sort((a, b) => a.roleId - b.roleId);
    const tmpElement = document.createElement('div');
    // construct(isEdit);
    //function construct(isEdit) {
    for (const projectMember of members) {
        const outerDiv = document.createElement('div');
        outerDiv.className = 'd-flex justify-content-between member mb-2 align-items-center';
        const memberName = document.createElement('div');
        memberName.className = 'member-name';
        memberName.innerHTML = projectMember.memberId !== member.memberId ? projectMember.name : "You";
        const memberEditContainer = document.createElement('div');
        memberEditContainer.className = 'd-flex member-edit';
        //const tmpStr1 = `<div class="member-role" id = "mr-${projectMember.memberId}-out">${projectMember.role}</div> <div href="#" data-id ="${projectMember.memberId}" class="member-remove text-danger"> <i class="fas fa-user-slash"></i> </div>`;
        const tmpStr1 = `<div href="#" data-id ="${projectMember.memberId}" class="member-remove text-danger"> <i class="fas fa-user-slash"></i> </div>`;
        const tmpStr2 = `<div class="member-role">${projectMember.role}</div>`;
        memberEditContainer.innerHTML = tmpStr2;
        if ((member.roleId === role.owner || member.roleId === role['project manager']) && isEdit === true) {
            if (projectMember.roleId > member.roleId && projectMember.memberId !== member.memberId) {
                memberEditContainer.innerHTML = tmpStr1;
                const outerDiv = document.createElement('div');
                outerDiv.className = "member-role";
                outerDiv.id = `mr-${projectMember.memberId}-out`;
                outerDiv.append(createDropDownButtonForMemberRole(projectMember.memberId, projectMember.roleId, projectMember.role, member, false));
                memberEditContainer.insertBefore(outerDiv, memberEditContainer.childNodes[0]);
            }
            if (memberEditContainer.childElementCount === 2) {
                memberEditContainer.lastChild.addEventListener('click', async (event) => {
                    event.preventDefault();
                    event.target.parentNode.classList.remove('member-edit');
                    event.target.parentNode.classList.add('member-delete');
                    event.target.parentNode.parentNode.className = 'd-none';
                });
            }
        }
        outerDiv.append(memberName);
        outerDiv.append(memberEditContainer);
        tmpElement.append(outerDiv);
    }
    //}

    //console.log(tmpElement);
    return tmpElement;
}
const createDropDownButtonForMemberRole = (projectMemberId, roleId, initialStr, member, isAdd) => {
    const container = document.createElement('div');
    const button = document.createElement("button");
    // button.classList.add("btn-primary", "dropdown-toggle", "member-role-option");
    button.classList.add("btn", "btn-primary", "dropdown-toggle", "member-role-option", "py-0");
    button.setAttribute("type", "button");
    button.setAttribute("data-toggle", "dropdown");
    button.setAttribute("aria-haspopup", "true");
    button.setAttribute("aria-expanded", "false");
    button.id = `mr-${projectMemberId}`;
    button.innerHTML = initialStr;
    container.append(button);
    const status = document.createElement('div');
    status.classList.add("dropdown-menu", "animated--fade-in")
    status.setAttribute("aria-labelledby", `mr-${projectMemberId}`);
    for (const key in role) {
        if (role[key] === roleId) {
            button.dataset.roleId = roleId;
            button.dataset.memberId = projectMemberId;
            continue;
        }
        if (role[key] >= member.roleId) {
            const temp = document.createElement('a');
            temp.innerHTML = `<a class="dropdown-item" href="#" data-id = "${role[key]}">${key}</a>`
            status.append(temp);
            temp.addEventListener('click', (event) => {
                const searchParams = isAdd ? `#ar-${projectMemberId}-out` : `#mr-${projectMemberId}-out`;
                const pjStatus = document.querySelector(`${searchParams}`);
                pjStatus.innerHTML = "";
                pjStatus.append(createDropDownButtonForMemberRole(projectMemberId, role[key], event.target.innerHTML, member, isAdd));
            });
        }
    }
    container.append(button);
    container.append(status);
    return container;
}

/* Reports related */
//display also tickets
function displayReportsData(tickets) {
    //report
    const unassignedTicketCount = document.querySelector('#unassigned-tickets-count');
    const openTicketCount = document.querySelector('#open-tickets-count');
    const inProgressTicketCount = document.querySelector('#in-progress-tickets-count');
    const closedTicketCount = document.querySelector('#closed-tickets-percentage');
    const totalTickets = document.querySelector('#total-tickets-count');
    // const unassignedTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus.unassigned);
    // const openTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus.open);
    console.log(tickets);
    const unassignedTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus.unassigned && ticket.assignedTo === null);
    const openTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus.open && ticket.assignedTo !== null);

    const inProgressTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus["in progress"]);
    const closedTickets = tickets.filter((ticket) => ticket.ticketStatusId === ticketStatus.closed);
    const arr = [unassignedTickets.length, openTickets.length, inProgressTickets.length, closedTickets.length]
    unassignedTicketCount.innerHTML = `${unassignedTickets.length}`;
    openTicketCount.innerHTML = `${openTickets.length}`;
    inProgressTicketCount.innerHTML = `${inProgressTickets.length}`;
    const percentage = (arr[3] / tickets.length) * 100
    closedTicketCount.innerHTML = `${percentage} %`;
    totalTickets.innerHTML = unassignedTickets.length + openTickets.length + inProgressTickets.length;
    //chart
    displayChart(...arr);
    //tickets
    const progressBar = document.querySelector('.progress-bar.bg-success');
    //progressBar.setAttribute("style", `width: ${percentage}%`);
    progressBar.style.width = `${percentage}%`;
    const unassignedColumn = document.querySelector('#column-unassigned');
    const openColumn = document.querySelector('#column-open');
    const inProgressColumn = document.querySelector('#column-in-progress');
    const closeColumn = document.querySelector('#column-closed');
    unassignedColumn.innerHTML = constructTickets(unassignedTickets);
    openColumn.innerHTML = constructTickets(openTickets);
    inProgressColumn.innerHTML = constructTickets(inProgressTickets);
    closeColumn.innerHTML = constructTickets(closedTickets);
}
/* Tickets related */
function constructTickets(assortedTickets) {
    let tempDiv = document.createElement('div');
    let color = ``;
    if(assortedTickets.length>0){
    if (assortedTickets[0].assignedTo === null) {
        color = 'border-bottom-danger';
    }
    else if (assortedTickets[0].ticketStatusId === ticketStatus.open) {
        color = 'border-bottom-warning';
    }
    else if (assortedTickets[0].ticketStatusId === ticketStatus["in progress"]) {
        color = 'border-bottom-info';
    }
    else if (assortedTickets[0].ticketStatusId === ticketStatus.closed) {
        color = 'border-bottom-success';
    }
}
    for (const ticket of assortedTickets) {
        const outerDiv = document.createElement('div');
        const header = document.createElement('div');
        const body = document.createElement('div');
        const jump = document.createElement('a');
        outerDiv.className = 'card shadow mb-4';
        header.className = `card-header py-3 ${color}`;
        body.className = 'card-body';
        jump.className = 'p-1 jump-to-ticket';
        jump.innerHTML = `<i class="fas fa-directions"></i>`;
        jump.setAttribute("href", `/ticket/comment.html?ticketId=${ticket.ticketId}&projectId=${pjId}`);
        header.innerHTML = `<h6 class="m-0 font-weight-bold text-primary">${ticket.title}</h6>`;
        body.innerHTML = `${ticket.ticketDescription}`;
        outerDiv.append(header);
        outerDiv.append(body);
        outerDiv.append(jump);
        tempDiv.append(outerDiv);
    }
    return tempDiv.innerHTML;
}

/* Utils */
function constructHtmlElement(type, classes, id, innerHtml, data) {
    const div = document.createElement(`${type}`);
    div.classList.add(...classes);
    div.id = id;
    div.dataset.id = data;
    div.innerHTML = innerHtml;
    return div;
}