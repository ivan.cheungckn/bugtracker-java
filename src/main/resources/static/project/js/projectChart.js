// Set new default font family and font color to mimic Bootstrap's default styling

(Chart.defaults.global.defaultFontFamily = "Nunito"),
  '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = "#858796";
// Pie Chart Example
var ctx = document.getElementById("myPieChart");
function displayChart(count1, count2, count3, count4) {
  var myPieChart = new Chart(ctx, {
    type: "doughnut",
    data: {
      labels: ["Unassiged", "Open", "In-progress", "Close"],
      datasets: [
        {
          data: [count1, count2, count3, count4],
          backgroundColor: ["#D53A2B", "#ECB52D", "#2E9FAF", "#18AC77"],
          hoverBackgroundColor: ["#D53A2B", "#ECB52D", "#2E9FAF", "#18AC77"],
          hoverBorderColor: "rgba(234, 236, 244, 1)",
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: "#dddfeb",
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: true,
      },
      cutoutPercentage: 80,
    },
  });
}
