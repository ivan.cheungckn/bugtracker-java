const apiVer = `api/v1`;
const preR = ``;
const rou = `${preR}/${apiVer}/users`;

async function getUserInfo(){
    const res = await fetch(`${rou}/info`);
    const result = await res.json();
    if(result.status === false){
        window.alert(result.message);
    }
    return result.message;
}

async function displayLayout(){
const user = await getUserInfo();
console.log(user);
if(user.systemLevelId === 1 || user.systemLevelId === 2){
    document.querySelector('#create-project-tab').classList.remove('d-none');
}
}

// displayLayout();
// displayNotifications();
async function getNotification(){
    const res = await fetch(`${rou}/notification`);
    const result = await res.json();
    if(result.status === false){
        window.alert(result.message);
    }
    return result.message;
}

async function displayNotifications(){
    const notifications = await getNotification();
    function constructHtmlElement(type, classes, id, innerHtml, data) {
        const div = document.createElement(`${type}`);
        div.classList.add(...classes);
        div.id = id;
        div.dataset.id = data;
        div.innerHTML = innerHtml;
        return div;
    }
    const monthArr = ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"];
    const badgeCounter = document.querySelector('.badge-counter');
    const notReadArr = notifications.filter((notification)=>notification.isRead === false);
    if(notReadArr.length===0){
        badgeCounter.classList.add('d-none');
    }else{
        badgeCounter.classList.remove('d-none');
        badgeCounter.innerHTML = notReadArr.length>10?"10+":`${notReadArr.length}`;
    }
    const alertContainer = document.querySelector('#alert-dropdown');
    alertContainer.innerHTML ="";
    const header = constructHtmlElement("h6",["dropdown-header"],"","ALERTS CENTER","");
    const markAllAsRead = constructHtmlElement("a",["dropdown-item","d-flex","align-items-center"],"","Mark All As Read","");
    markAllAsRead.setAttribute("href","#");
    markAllAsRead.addEventListener('click',async()=>{
        event.preventDefault();
        const idArr = notifications.map((x)=>x.id);
        const res = await fetch(`${rou}/notification`,{
            method:'PUT',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({notificationIds:idArr})
        })
        const result = await res.json();
        if(result.status === false){
            window.alert(result.message);
            console.error(result.message);
        }
        displayNotifications();
    })

    alertContainer.append(header);
    alertContainer.append(markAllAsRead);

    for(const notification of notifications){
        const notificationContainer = constructHtmlElement('div',["dropdown-item","d-flex","align-items-center"],"","",`${notification.ticketId}`);
        const iconWrapper = constructHtmlElement('div',["mr-3"],"","","");
        const iconContainer = constructHtmlElement('div',["icon-circle", "bg-success"],"",`<i class="fas fa-ticket-alt"></i>`,"");
        const rawDate =new Date(notification.ticketCreatedAt);
        const dateInString = `${monthArr[rawDate.getMonth()]} ${rawDate.getDate()}, ${rawDate.getFullYear()}`
        const notRead = notification.isRead?``:`<span class = "text-danger"><i class="fas fa-circle"></i></span>`;
        const content = constructHtmlElement('div',["small", "text-gray-700"],"",`<div>${dateInString}</div>[${notification.projectName}][${notification.ticketTitle}] A new ticket is received. ${notRead}`,"")
        iconWrapper.append(iconContainer);
        notificationContainer.append(iconWrapper);
        notificationContainer.append(content);
        alertContainer.append(notificationContainer);
        notificationContainer.addEventListener('click', async (event)=>{
            const res = await fetch(`${rou}/notification`,{
                method:'PUT',
                headers: {'Content-Type':'application/json'},
                body:JSON.stringify({notificationIds:[`${notification.id}`]})
            });
            const result = await res.json();
            if(result.status===false){
                console.error(result.message);
                window.alert(result.message);
            }
            location.replace(`/ticket/comment.html?ticketId=${notification.ticketId}&projectId=${notification.projectId}`)
        });
    }

    
}