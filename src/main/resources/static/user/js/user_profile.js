(function ($) {
  "use strict"; // Start of use strict

  // async function main() { // display current user's username, icon
  //   const res = await fetch("api/v1/users/current_user");
  //   const data = await res.json();
  //   console.log(data); // display current user's username, icon
  //   document.getElementById("nav-profile-name").innerHTML = data.currentUser.username; // current currentUser's username
  //   document.querySelector("#nav-profile-icon").setAttribute("src", `/icon/${data.currentUser.icon}`);
  // }

  async function main() {
    const res = await fetch("/api/v1/users/current_user");
    const data = await res.json();
    console.log(data);
}
main();
 


  // Nav bar profile icon
  function updateUserList(users) {
    let htmlStr = ``;
    for (const user of users) {
      console.log(user)
      htmlStr += `
      <div class="card profile-card" style="width: 18rem;">
        <div>
          <div class=""><img class="profileIconImage" src="/icon/${user.icon?user.icon:"default.png"}"></div>
          <div class="profileName">${user.name}</div>
          <div class="profileUsername">Username: ${user.username}</div>
          <div class="profileSystemLevel">System level: ${user.system_level_id}</div>
          <div class="profileRank">Rank: ${user.rank_id}</div>
        </div>
      </div>
        `;
      // <div class="profileUsername">Username</div>
      // <div class="profileSystemLevel"><i class="far fa-rocket"></i>System level</div>
      // <div class="profileRank">Rank</div>
    }
    document.querySelector('#profile-container').innerHTML = htmlStr;
  }

  async function initPage() {
    const res = await fetch("api/v1/users/user_profile");
    const data = await res.json();
    // console.log(data);
    updateUserList(data.users);
  }

  // window.onload = () => {
    initPage();
    document
      .getElementById("search-profile-button")
      .addEventListener("click", async function (e) {
        console.log("clicked");
        const keyword = document.getElementById("search-value").value; // empty input will be undefined
        // console.log(keyword);
        let query = `api/v1/users?`;
        if (keyword) {
          query += `keyword=${keyword}`;
        }
        const res = await fetch(query);
        const data = await res.json();
        updateUserList(data.users);
        // console.log(data);

        //   let htmlStr = ``;
        //   for (const user of data.user) {
        //     htmlStr += `
        //       <div class="profileName">${user.name}</div>
        //       `
        //     // <div class="profileUsername">Username</div>
        //     // <div class="profileSystemLevel"><i class="far fa-rocket"></i>System level</div>
        //     // <div class="profileRank">Rank</div>
        //   }
        //   document.querySelector(".user-profile-form .profile-group").innerHTML = htmlStr;
      });
  // };

  // main();


})(jQuery); // End of use strict

// window.onload = () => {
//   document.querySelector("#search-profile").addEventListener("submit", async function (e) { // e is event
//     e.preventDefault();
//     const formObject = {};
//     for (const input of this) {
//       console.log(input);
//       if (!["submit"].includes(input.type)) {
//         formObject[input.name] = input.value;
//       }
//     }
//     console.log('test 123', formObject);
//     // "/api/v1/users/user_profile"
//     const res = await fetch(`/api/v1/users/user_profile?name=${formObject.name}`, {
//       method: "GET",
//       // headers: {
//       //   "Content-Type": "application/json"
//       // },
//       // body: JSON.stringify(formObject)
//     });
//     if (res.status !== 200) {
//       const data = await res.json();
//       alert(data.message);
//     } else {
//       window.location.href = "/";

//       // console.log(formObject);
//     }
//   });
// }