(function ($) {
  "use strict"; // Start of use strict

  async function main() { // display current user's username, icon
    const res = await fetch("/api/v1/users/my_profile");
    const data = await res.json();
    console.log(data); // display current user's username, icon
    document.getElementById("my-profile-name").innerHTML = data.myProfile.name; // current currentUser's username
    document.getElementById("my-profile-username").innerHTML = `My username: ${data.myProfile.username}`;
    document.getElementById("my-profile-user-id").innerHTML = `My user id: ${data.myProfile.userID}`;
    document.getElementById("my-profile-system-level").innerHTML = `System level: ${data.myProfile.system_level_id}`;
    document.getElementById("my-profile-rank").innerHTML = `Rank: ${data.myProfile.rank_id}`;
    document.querySelector("#my-profile-icon").setAttribute("src", `/icon/${data.myProfile.icon ? data.myProfile.icon : "default.png"}`);
  }
  main();
})(jQuery); 