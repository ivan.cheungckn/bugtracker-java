async function main() {

    const registerForm = document.querySelector("#register-form");

    {
        const res = await fetch("api/v1/users/system_level");
        const data = await res.json();
        console.log(data);
        // console.log(registerForm.querySelector('select[name="system_level"]'))
        // let htmlStr = ``;
        // for (const sys of data.systemId) {
        //     htmlStr += `<option value="${sys.id}">${sys.name}</option>`;
        // }
        // registerForm.querySelector('select[name="system_level"]').innerHTML = htmlStr;
    }

    // NEW
    registerForm.addEventListener('submit', async function (e) {
        e.preventDefault();

        // Serialize the Form afterwards
        const form = this;
        const formData = new FormData();

        formData.append('username', form.username.value);
        formData.append('name', form.name.value);
        formData.append('password', form.password.value);
        formData.append('system_level', form.system_level.value);
        formData.append('rank', form.rank.value);
        // console.log(form.icon.files); // success here
        formData.append('icon', form.icon.files[0]);

        // console.log(formData.get("icon"));
        let userStr = ''; // concat string
        for (var value of formData.values()) {
            // console.log(value);
            userStr = userStr + ' ' + value;
        }
        console.log(userStr);

        const res = await fetch('/api/v1/users/register', {
            method: "POST",
            body: formData
        });
        const result = await res.json();
        registerForm.querySelector.innerHTML = result;
        window.location.href = "/";

    })


    // // OLD
    // registerForm.addEventListener("submit", async (e) => { // e is event
    //     e.preventDefault();
    //     const formObject = {};
    //     for (const input of registerForm) {
    //         console.log(input);
    //         if (!["submit"].includes(input.type)) {
    //             formObject[input.name] = input.value;
    //         }
    //     }
    //     console.log(formObject);
    //     const res = await fetch('/api/v1/users/register', {
    //         method: "POST",
    //         headers: {
    //             "Content-Type": "application/json"
    //         },
    //         body: JSON.stringify(formObject)
    //     });
    //     if (res.status !== 200) {
    //         console.log("error");
    //         alert("The username already exists. Please use a different username.");
    //     } else {
    //         const userId = (await res.json());
    //         console.log(userId);
    //         registerForm.reset();
    //     }
    //     // console.log(formObject);
    // });
}

window.onload = () => {
    main();
}