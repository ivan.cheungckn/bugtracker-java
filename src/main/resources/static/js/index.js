(function ($) {
  "use strict"; // Start of use strict

  async function main() { // display current user's username, icon
    const res = await fetch("/api/v1/users/current_user");
    const data = await res.json();
    console.log(data); // display current user's username, icon
    document.getElementById("nav-profile-name").innerHTML = data.currentUser.username; // current currentUser's username
    document.querySelector("#nav-profile-icon").setAttribute("src", `/icon/${data.currentUser.icon?data.currentUser.icon:"default.png"}`);
    // document.querySelector("#nav-profile-icon").setAttribute("src", `/default.png`);

  }

  // Nav bar profile icon
  // window.onload = () => {

  //   main();
  // }

  // NEW
  $('#accordionSidebar').load('../index.html #accordionSidebar > *', function () {
    $('.navbar.navbar-expand.navbar-light.bg-white.topbar.mb-4.static-top.shadow').load('../index.html .navbar.navbar-expand.navbar-light.bg-white.topbar.mb-4.static-top.shadow > *', function (){
      main();
      displayLayout();
      displayNotifications();
      $("#sidebarToggle, #sidebarToggleTop").on("click", function (e) {
        console.log('123');
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
          $(".sidebar .collapse").collapse("hide");
        }

    })
    });
  })
  $('#logoutModal').load('../index.html #logoutModal > *')
  // jQuery(document).ready(function(){



  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function () {
    if ($(window).width() < 768) {
      $(".sidebar .collapse").collapse("hide");
    }
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function (
    e
  ) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on("scroll", function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $(".scroll-to-top").fadeIn();
    } else {
      $(".scroll-to-top").fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on("click", "a.scroll-to-top", function (e) {
    var $anchor = $(this);
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $($anchor.attr("href")).offset().top,
        },
        1000,
        "easeInOutExpo"
      );
    e.preventDefault();
  });
  // });

})(jQuery); // End of use strict

//for attach js
//q = visiting which page
//info = get what information

// window.onload = function () {
//   const urlParams = new URLSearchParams(window.location.search);
//   const visitPage = urlParams.get("q");
//   if (visitPage === "createTicket") {
//     var script = document.createElement('script');
//     script.src = `js/createTicket.js`

//   } else if (visitPage === "myTickets") {

//     var script = document.createElement('script');
//     script.src = `js/myTickets.js`;
//     var link = document.createElement('link');
//     link.href = "css/myTickets.css";
//     link.rel = "stylesheet";
//   } else if (visitPage === "myProjectTickets") {

//     var script = document.createElement('script');
//     script.src = `js/myProjectTickets.js`;
//     var link = document.createElement('link');
//     link.href = "css/myTickets.css";
//     link.rel = "stylesheet";
//   } else if (visitPage === "mySubmittedTickets") {

//     var script = document.createElement('script');
//     script.src = `js/mySubmittedTickets.js`;
//     var link = document.createElement('link');
//     link.href = "css/mySubmittedTickets.css";
//     link.rel = "stylesheet";
//   }else if (visitPage === "myProjectInfo") {

//     var script = document.createElement('script');
//     script.src = `/project/js/project.js`;
//     var link = document.createElement('link');
//     link.href = "/project/css/project.css";
//     link.rel = "stylesheet";
//   };
//   script.onload = function () {
//     console.log("Script has been run!");
//   }
//   document.body.appendChild(script);
//   if (link) {
//     document.head.appendChild(link);
//   }

//   console.log(urlParams);
//   console.log(visitPage);
// };
