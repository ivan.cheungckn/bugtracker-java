package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectForm {
    @JsonProperty("userId")
    private long userId;
    @JsonProperty("projectName")
    private String projectName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("statusId")
    private long statusId;

    public ProjectForm(long userId, String projectName, String description, long statusId) {
        this.userId = userId;
        this.projectName = projectName;
        this.description = description;
        this.statusId = statusId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}
