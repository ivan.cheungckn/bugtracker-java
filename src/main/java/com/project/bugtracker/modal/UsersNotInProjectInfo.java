package com.project.bugtracker.modal;

public class UsersNotInProjectInfo {
    private long userId;
    private String name;
    private String username;

    public UsersNotInProjectInfo(long userId, String name, String username) {
        this.userId = userId;
        this.name = name;
        this.username = username;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
