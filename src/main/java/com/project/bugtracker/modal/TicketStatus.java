package com.project.bugtracker.modal;

public enum TicketStatus {
    UNASSIGNED("unassigned"),
    OPEN("open"),
    INPROGRESS("in progress"),
    CLOSED("closed");

    private String status;

    TicketStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
