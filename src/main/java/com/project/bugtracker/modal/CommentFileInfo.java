package com.project.bugtracker.modal;

public class CommentFileInfo {
    private Long commentFileId;
    private String fileName;

    public CommentFileInfo(Long commentFileId, String fileName) {
        this.commentFileId = commentFileId;
        this.fileName = fileName;
    }

    public Long getCommentFileId() {
        return commentFileId;
    }

    public void setCommentFileId(Long commentFileId) {
        this.commentFileId = commentFileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
