package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateMemberForm {
    @JsonProperty("memberId")
    private long memberId;
    @JsonProperty("roleId")
    private long roleId;
    @JsonProperty("projectId")
    private long projectId;

    public UpdateMemberForm(long memberId, long roleId, long projectId) {
        this.memberId = memberId;
        this.roleId = roleId;
        this.projectId = projectId;
    }

    public UpdateMemberForm() {
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }
}
