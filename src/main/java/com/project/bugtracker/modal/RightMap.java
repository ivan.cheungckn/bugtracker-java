package com.project.bugtracker.modal;

public class RightMap {
    private boolean ticketCategory;
    private boolean ticketDifficulty;
    private boolean ticketCriticalLevel;
    private boolean ticketDueDate;
    private boolean ticketTitle;
    private boolean ticketDescription;
    private boolean ticketStatus;
    private boolean ticketAssigned;
    private boolean isAssignToNull;

    public RightMap(boolean ticketCategory, boolean ticketDifficulty, boolean ticketCriticalLevel, boolean ticketDueDate, boolean ticketTitle, boolean ticketDescription, boolean ticketStatus, boolean ticketAssigned, boolean isAssignToNull) {
        this.ticketCategory = ticketCategory;
        this.ticketDifficulty = ticketDifficulty;
        this.ticketCriticalLevel = ticketCriticalLevel;
        this.ticketDueDate = ticketDueDate;
        this.ticketTitle = ticketTitle;
        this.ticketDescription = ticketDescription;
        this.ticketStatus = ticketStatus;
        this.ticketAssigned = ticketAssigned;
        this.isAssignToNull = isAssignToNull;
    }

    public boolean isTicketCategory() {
        return ticketCategory;
    }

    public void setTicketCategory(boolean ticketCategory) {
        this.ticketCategory = ticketCategory;
    }

    public boolean isTicketDifficulty() {
        return ticketDifficulty;
    }

    public void setTicketDifficulty(boolean ticketDifficulty) {
        this.ticketDifficulty = ticketDifficulty;
    }

    public boolean isTicketCriticalLevel() {
        return ticketCriticalLevel;
    }

    public void seTicketCriticalLevel(boolean ticketCriticalLevel) {
        this.ticketCriticalLevel = ticketCriticalLevel;
    }

    public boolean isTicketDueDate() {
        return ticketDueDate;
    }

    public void setTicketDueDate(boolean ticketDueDate) {
        this.ticketDueDate = ticketDueDate;
    }

    public boolean isTicketTitle() {
        return ticketTitle;
    }

    public void setTicketTitle(boolean ticketTitle) {
        this.ticketTitle = ticketTitle;
    }

    public boolean isTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(boolean ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public boolean isTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(boolean ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public boolean isTicketAssigned() {
        return ticketAssigned;
    }

    public void setTicketAssigned(boolean ticketAssigned) {
        this.ticketAssigned = ticketAssigned;
    }

    public boolean isAssignToNull() {
        return isAssignToNull;
    }

    public void setAssignToNull(boolean assignToNull) {
        isAssignToNull = assignToNull;
    }
}
