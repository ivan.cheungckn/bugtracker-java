package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;

public class ProjectTicket {
    private long ticketId;
    private String title;
    private String ticketStatus;
    private Timestamp dueDate;
    private String assignedTo;

    public ProjectTicket(long id, String title, String ticketStatus, Timestamp dueDate, @Nullable String assignedTo) {
        this.ticketId = id;
        this.title = title;
        this.ticketStatus = ticketStatus;
        this.dueDate = dueDate;
        this.assignedTo = assignedTo;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long id) {
        ticketId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Timestamp getDueDate() {
        return dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
}
