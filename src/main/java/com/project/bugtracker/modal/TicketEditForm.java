package com.project.bugtracker.modal;

import org.springframework.web.bind.annotation.RequestPart;

public class TicketEditForm {
    private Long ticketID;
    private Boolean isAssignToNull;
    private String category;
    private String difficulty;
    private String criticalLevel;
    private String dueDate;
    private String title;
    private String status;
    private String description;
    private Long ticketAssignedToId;

    public TicketEditForm(Long ticketID, Boolean isAssignToNull, String category, String difficulty, String criticalLevel, String dueDate, String title, String status, String description, Long ticketAssignedToId) {
        this.ticketID = ticketID;
        this.isAssignToNull = isAssignToNull;
        this.category = category;
        this.difficulty = difficulty;
        this.criticalLevel = criticalLevel;
        this.dueDate = dueDate;
        this.title = title;
        this.status = status;
        this.description = description;
        this.ticketAssignedToId = ticketAssignedToId;
    }

    public TicketEditForm() {
    }

    public Long getTicketId() {
        return ticketID;
    }

    public void setTicketId(Long ticketID) {
        this.ticketID = ticketID;
    }

    public Boolean getAssignToNull() {
        return isAssignToNull;
    }

    public void setAssignToNull(Boolean assignToNull) {
        isAssignToNull = assignToNull;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getCriticalLevel() {
        return criticalLevel;
    }

    public void setCriticalLevel(String criticalLevel) {
        this.criticalLevel = criticalLevel;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTicketAssignedToId() {
        return ticketAssignedToId;
    }

    public void setTicketAssignedToId(Long ticketAssignedToId) {
        this.ticketAssignedToId = ticketAssignedToId;
    }
}
