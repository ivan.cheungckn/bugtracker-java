package com.project.bugtracker.modal;

import java.util.ArrayList;
import java.util.List;

public class ProjectOutput {
    private long projectId;
    private String owner;
    private String projectName;
    private String description;
    private String status;
    private final List<ProjectTicket> tickets = new ArrayList<ProjectTicket>();
    private final List<ProjectMember> members = new ArrayList<ProjectMember>();

    public ProjectOutput(long projectId, String owner, String projectName, String description, String status) {
        this.projectId = projectId;
        this.owner = owner;
        this.projectName = projectName;
        this.description = description;
        this.status = status;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProjectTicket> getTickets() {
        return tickets;
    }

    public void addTicket(ProjectTicket ticket) {
        this.tickets.add(ticket);
    }

    public List<ProjectMember> getMembers() {
        return members;
    }

    public void addMember(ProjectMember member) {
        this.members.add(member);
    }
}
