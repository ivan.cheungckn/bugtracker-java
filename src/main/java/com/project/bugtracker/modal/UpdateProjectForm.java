package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateProjectForm {
    @JsonProperty("projectId")
    private long projectId;
    @JsonProperty("projectName")
    private String projectName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("statusId")
    private long statusId;

    public UpdateProjectForm(long projectId, String projectName, String description, long statusId) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.description = description;
        this.statusId = statusId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}
