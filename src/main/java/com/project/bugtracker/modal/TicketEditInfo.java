package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

public class TicketEditInfo {
    @JsonProperty("Ticket Category")
    private String ticketCategory;

    @JsonProperty("Ticket Critical Level")
    private String ticketCriticalLevel;

    @JsonProperty("Ticket Title")
    private String ticketTitle;

    @JsonProperty("Ticket Description")
    private String ticketDescription;

    @JsonProperty("Ticket Difficulty")
    private String ticketDifficulty;

    @JsonProperty("Ticket Status")
    private String status;

    @JsonProperty("Ticket Due Date")
    private Timestamp dueDate;

    @JsonProperty("Ticket Assigned To User ID")
    private Long ticketAssignToId;
    @JsonProperty("Ticket Assigned By User ID")
    private Long ticketAssignById;

    public TicketEditInfo(String ticketCategory, String ticketCriticalLevel, String ticketTitle, String ticketDescription, String ticketDifficulty, String status, Timestamp dueDate, Long ticketAssignToId, Long ticketAssignById) {
        this.ticketCategory = ticketCategory;
        this.ticketCriticalLevel = ticketCriticalLevel;
        this.ticketTitle = ticketTitle;
        this.ticketDescription = ticketDescription;
        this.ticketDifficulty = ticketDifficulty;
        this.status = status;
        this.dueDate = dueDate;
        this.ticketAssignToId = ticketAssignToId;
        this.ticketAssignById = ticketAssignById;
    }

    public String getTicketCategory() {
        return ticketCategory;
    }

    public void setTicketCategory(String ticketCategory) {
        this.ticketCategory = ticketCategory;
    }

    public String getTicketCriticalLevel() {
        return ticketCriticalLevel;
    }

    public void setTicketCriticalLevel(String ticketCriticalLevel) {
        this.ticketCriticalLevel = ticketCriticalLevel;
    }

    public String getTicketTitle() {
        return ticketTitle;
    }

    public void setTicketTitle(String ticketTitle) {
        this.ticketTitle = ticketTitle;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getTicketDifficulty() {
        return ticketDifficulty;
    }

    public void setTicketDifficulty(String ticketDifficulty) {
        this.ticketDifficulty = ticketDifficulty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getDueDate() {
        return dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    public Long getTicketAssignToId() {
        return ticketAssignToId;
    }

    public void setTicketAssignToId(Long ticketAssignToId) {
        this.ticketAssignToId = ticketAssignToId;
    }

    public Long getTicketAssignById() {
        return ticketAssignById;
    }

    public void setTicketAssignById(Long ticketAssignById) {
        this.ticketAssignById = ticketAssignById;
    }
}
