package com.project.bugtracker.modal;

import java.sql.Timestamp;

public class EditTicketForm {
    String ticketCategory;
    String ticketCriticalLevel;
    String ticketStatus;
    Timestamp ticketDueDate;
    String ticketTitle;
    String ticketDescription;
    long ticketAssignToUserId;
    long ticketAssignByUserId;

    public EditTicketForm(String ticketCategory, String ticketCriticalLevel, String ticketStatus, Timestamp ticketDueDate, String ticketTitle, String ticketDescription, long ticketAssignToUserId, long ticketAssignByUserId) {
        this.ticketCategory = ticketCategory;
        this.ticketCriticalLevel = ticketCriticalLevel;
        this.ticketStatus = ticketStatus;
        this.ticketDueDate = ticketDueDate;
        this.ticketTitle = ticketTitle;
        this.ticketDescription = ticketDescription;
        this.ticketAssignToUserId = ticketAssignToUserId;
        this.ticketAssignByUserId = ticketAssignByUserId;
    }

    public long getTicketAssignByUserId() {
        return ticketAssignByUserId;
    }

    public void setTicketAssignByUserId(long ticketAssignByUserId) {
        this.ticketAssignByUserId = ticketAssignByUserId;
    }

    public String getTicketCategory() {
        return ticketCategory;
    }

    public void setTicketCategory(String ticketCategory) {
        this.ticketCategory = ticketCategory;
    }

    public String getTicketCriticalLevel() {
        return ticketCriticalLevel;
    }

    public void setTicketCriticalLevel(String ticketCriticalLevel) {
        this.ticketCriticalLevel = ticketCriticalLevel;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Timestamp getTicketDueDate() {
        return ticketDueDate;
    }

    public void setTicketDueDate(Timestamp ticketDueDate) {
        this.ticketDueDate = ticketDueDate;
    }

    public String getTicketTitle() {
        return ticketTitle;
    }

    public void setTicketTitle(String ticketTitle) {
        this.ticketTitle = ticketTitle;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public long getTicketAssignToUserId() {
        return ticketAssignToUserId;
    }

    public void setTicketAssignToUserId(long ticketAssignToUserId) {
        this.ticketAssignToUserId = ticketAssignToUserId;
    }
}
