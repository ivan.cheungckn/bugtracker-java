package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrentUser {
    @JsonProperty("currentUser")
    private CurrentUserInfo currentUser;

    public CurrentUser(CurrentUserInfo currentUser) {
        this.currentUser = currentUser;
    }

    public CurrentUserInfo getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(CurrentUserInfo currentUser) {
        this.currentUser = currentUser;
    }
}
