package com.project.bugtracker.modal;

public class Member {
    private long projectId;
    private long memberId;

    public Member(long projectId, long memberId) {
        this.projectId = projectId;
        this.memberId = memberId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }
}
