package com.project.bugtracker.modal;

import com.project.bugtracker.entity.Categories;
import com.project.bugtracker.entity.CriticalLevels;
import com.project.bugtracker.entity.Status;

import java.util.List;
import java.util.Set;

public class TicketInfo {
    private Set<TicketCategoriesInfo> categories;
    private Set<TicketCriticalLevelsInfo> criticalLevels;
    private Set<TicketProjectInfo> projectIDNames;
    private Set<TicketStatusInfo> statusList;
    private Set<TicketUserInfo> userList;

    public TicketInfo(Set<TicketCategoriesInfo> categories, Set<TicketCriticalLevelsInfo> criticalLevels, Set<TicketProjectInfo> projectIDNames, Set<TicketStatusInfo> statusList, Set<TicketUserInfo> userList) {
        this.categories = categories;
        this.criticalLevels = criticalLevels;
        this.projectIDNames = projectIDNames;
        this.statusList = statusList;
        this.userList = userList;
    }

    public Set<TicketCategoriesInfo> getCategories() {
        return categories;
    }

    public void setCategories(Set<TicketCategoriesInfo> categories) {
        this.categories = categories;
    }

    public Set<TicketCriticalLevelsInfo> getCriticalLevels() {
        return criticalLevels;
    }

    public void setCriticalLevels(Set<TicketCriticalLevelsInfo> criticalLevels) {
        this.criticalLevels = criticalLevels;
    }

    public Set<TicketProjectInfo> getProjectIDNames() {
        return projectIDNames;
    }

    public void setProjectIDNames(Set<TicketProjectInfo> projectIDNames) {
        this.projectIDNames = projectIDNames;
    }

    public Set<TicketStatusInfo> getStatusList() {
        return statusList;
    }

    public void setStatusList(Set<TicketStatusInfo> statusList) {
        this.statusList = statusList;
    }

    public Set<TicketUserInfo> getUserList() {
        return userList;
    }

    public void setUserList(Set<TicketUserInfo> userList) {
        this.userList = userList;
    }
}
