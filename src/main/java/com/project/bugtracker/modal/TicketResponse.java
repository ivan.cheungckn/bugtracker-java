package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Time;
import java.sql.Timestamp;

public class TicketResponse {
    @JsonProperty("Ticket Category")
    private String ticketCategory;

    @JsonProperty("Ticket Critical Level")
    private String ticketCriticalLevel;

    @JsonProperty("Ticket Title")
    private String ticketTitle;

    @JsonProperty("Ticket Description")
    private String ticketDescription;

    @JsonProperty("Ticket Difficulty")
    private String ticketDifficulty;

    @JsonProperty("Ticket Status")
    private String status;

    @JsonProperty("Ticket Due Date")
    private Timestamp dueDate;

    @JsonProperty("Ticket Assigned To User ID")
    private Long ticketAssignToId;
    @JsonProperty("Ticket Assigned By User ID")
    private Long ticketAssignById;

    @JsonProperty("Project Created At")
    private Timestamp projectCreatedAt;

    @JsonProperty("Project Description")
    private String projectDescription;

    @JsonProperty("Project ID")
    private Long projectId;

    @JsonProperty("Project Name")
    private String projectName;

    @JsonProperty("Project Status")
    private String projectStatus;
    @JsonProperty("Project Updated At")
    private Timestamp projectUpdatedAt;

    @JsonProperty("Ticket Assigned Time")
    private Timestamp ticketAssignedTime;
    @JsonProperty("Ticket Assigned To Name")
    private String ticketAssignedToName;

    @JsonProperty("Ticket Assigned To Username")
    private String ticketAssignedToUsername;
    @JsonProperty("Ticket Assigned by Name")
    private String ticketAssignedByName;

    @JsonProperty("Ticket Assigned by Username")
    private String ticketAssignedByUsername;

    @JsonProperty("Ticket Created At")
    private Timestamp ticketCreatedAt;

    @JsonProperty("Ticket Created By Name")
    private String ticketCreatedByName;
    @JsonProperty("Ticket Created By User ID")
    private Long ticketCreatedByUserID;
    @JsonProperty("Ticket Created By Username")
    private String ticketCreatedByUsername;

    @JsonProperty("History")
    private String history;

    @JsonProperty("Ticket ID")
    private Long ticketID;
    @JsonProperty("Ticket Updated At")
    private Timestamp ticketUpdatedAt;

    public TicketResponse(String ticketCategory, String ticketCriticalLevel, String ticketTitle, String ticketDescription, String ticketDifficulty, String status, Timestamp dueDate, Long ticketAssignToId, Long ticketAssignById, Timestamp projectCreatedAt, String projectDescription, Long projectId, String projectName, String projectStatus, Timestamp projectUpdatedAt, Timestamp ticketAssignedTime, String ticketAssignedToName, String ticketAssignedToUsername, String ticketAssignedByName, String ticketAssignedByUsername, Timestamp ticketCreatedAt, String ticketCreatedByName, Long ticketCreatedByUserID, String ticketCreatedByUsername, String history, Long ticketID, Timestamp ticketUpdatedAt) {
        this.ticketCategory = ticketCategory;
        this.ticketCriticalLevel = ticketCriticalLevel;
        this.ticketTitle = ticketTitle;
        this.ticketDescription = ticketDescription;
        this.ticketDifficulty = ticketDifficulty;
        this.status = status;
        this.dueDate = dueDate;
        this.ticketAssignToId = ticketAssignToId;
        this.ticketAssignById = ticketAssignById;
        this.projectCreatedAt = projectCreatedAt;
        this.projectDescription = projectDescription;
        this.projectId = projectId;
        this.projectName = projectName;
        this.projectStatus = projectStatus;
        this.projectUpdatedAt = projectUpdatedAt;
        this.ticketAssignedTime = ticketAssignedTime;
        this.ticketAssignedToName = ticketAssignedToName;
        this.ticketAssignedToUsername = ticketAssignedToUsername;
        this.ticketAssignedByName = ticketAssignedByName;
        this.ticketAssignedByUsername = ticketAssignedByUsername;
        this.ticketCreatedAt = ticketCreatedAt;
        this.ticketCreatedByName = ticketCreatedByName;
        this.ticketCreatedByUserID = ticketCreatedByUserID;
        this.ticketCreatedByUsername = ticketCreatedByUsername;
        this.history = history;
        this.ticketID = ticketID;
        this.ticketUpdatedAt = ticketUpdatedAt;
    }
}
