package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserProfile {
    @JsonProperty("id")
    private Long Id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("rank_id")
    private String rankId;
    @JsonProperty("system_level_id")
    private String systemLevelId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("icon")
    private String icon;

    public UserProfile(Long id, String name, String rankId, String systemLevelId, String username, String icon) {
        Id = id;
        this.name = name;
        this.rankId = rankId;
        this.systemLevelId = systemLevelId;
        this.username = username;
        this.icon = icon;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRankId() {
        return rankId;
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }

    public String getSystemLevelId() {
        return systemLevelId;
    }

    public void setSystemLevelId(String systemLevelId) {
        this.systemLevelId = systemLevelId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
