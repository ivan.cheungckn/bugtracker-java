package com.project.bugtracker.modal;

import java.sql.Timestamp;
import java.util.List;

public class CommentInfo {
    private Long commentId;
    private Long parentCommentId;
    private Long userId;
    private String commenter;
    private String content;
    private Timestamp createDate;
    private Timestamp updateDate;
    private List<CommentInfo> comments;
    private List<CommentFileInfo> files;

    public CommentInfo(Long commentId, Long parentCommentId, Long userId, String commenter, String content, Timestamp createDate, Timestamp updateDate, List<CommentInfo> comments, List<CommentFileInfo> files) {
        this.commentId = commentId;
        this.parentCommentId = parentCommentId;
        this.userId = userId;
        this.commenter = commenter;
        this.content = content;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.comments = comments;
        this.files = files;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Long parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCommenter() {
        return commenter;
    }

    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<CommentInfo> getComments() {
        return comments;
    }

    public void setComments(List<CommentInfo> comments) {
        this.comments = comments;
    }

    public List<CommentFileInfo> getFiles() {
        return files;
    }

    public void setFiles(List<CommentFileInfo> files) {
        this.files = files;
    }
}
