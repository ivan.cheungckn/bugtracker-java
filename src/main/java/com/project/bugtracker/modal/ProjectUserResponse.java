package com.project.bugtracker.modal;

public class ProjectUserResponse {
    private Long userId;
    private Long projectId;
    private String projectName;
    private Long roleId;
    private String projectStatus;
    private String description;

    public ProjectUserResponse(Long userId, Long projectId, String projectName, Long roleId, String projectStatus, String description) {
        this.userId = userId;
        this.projectId = projectId;
        this.projectName = projectName;
        this.roleId = roleId;
        this.projectStatus = projectStatus;
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
