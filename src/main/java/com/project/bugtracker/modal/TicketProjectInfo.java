package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TicketProjectInfo {
    @JsonProperty("id")
    private long id;
    @JsonProperty("project_name")
    private String projectName;

    public TicketProjectInfo(long id, String projectName) {
        this.id = id;
        this.projectName = projectName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
