package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrentUserInfo {
    @JsonProperty("username")
    private String username;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("userID")
    private Long userId;

    public CurrentUserInfo(String username, String icon, Long userId) {
        this.username = username;
        this.icon = icon;
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
