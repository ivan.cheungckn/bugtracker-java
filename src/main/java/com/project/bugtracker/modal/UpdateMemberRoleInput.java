package com.project.bugtracker.modal;

public class UpdateMemberRoleInput {
    private long memberId;
    private long roleId;

    public UpdateMemberRoleInput(long memberId, long roleId) {
        this.memberId = memberId;
        this.roleId = roleId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
