package com.project.bugtracker.modal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectMember {
    @JsonProperty("memberId")
    private long memberId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("role")
    private String role;
    @JsonProperty("roleId")
    private long roleId;

    public ProjectMember(long memberId, String name, String role, long roleId) {
        this.memberId = memberId;
        this.name = name;
        this.role = role;
        this.roleId = roleId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
