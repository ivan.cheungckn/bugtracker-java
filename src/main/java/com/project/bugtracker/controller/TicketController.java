package com.project.bugtracker.controller;

import com.project.bugtracker.auth.TicketEditRight;
import com.project.bugtracker.auth.TicketProjectRole;
import com.project.bugtracker.entity.*;
import com.project.bugtracker.exception.FailException;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.exception.RangeException;
import com.project.bugtracker.exception.Response;
import com.project.bugtracker.modal.*;
import com.project.bugtracker.service.ProjectService;
import com.project.bugtracker.service.TicketService;
import com.project.bugtracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/api/v1/ticket")
@RestController
public class TicketController {
    private TicketService ticketService;
    private ProjectService projectService;

    @Autowired
    public TicketController(TicketService ticketService,
                            ProjectService projectService){
        this.ticketService = ticketService;
        this.projectService = projectService;
    }

    @GetMapping("info")
    public ResponseEntity getTicketInfo(){
        Set<TicketCategoriesInfo> categories = ticketService.getCategories();
        Set<TicketCriticalLevelsInfo> criticalLevels = ticketService.getCriticalLevels();
        Set<TicketProjectInfo> projects = ticketService.getProjects();
        Set<TicketStatusInfo> statuses = ticketService.getStatus();
        Set<TicketUserInfo> users = ticketService.getUsers();
        TicketInfo ticketInfo = new TicketInfo(categories, criticalLevels, projects, statuses, users);
        return new ResponseEntity(ticketInfo, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity getTickets(HttpSession session,
                           @RequestParam(value = "projectName", required=false) String projectName,
                           @RequestParam("target") String target,
                           @RequestParam(value = "ticketID", required=false) Long ticketId){
        System.out.println(projectName);
        Long userId = (Long) session.getAttribute("userId");
        System.out.println(userId);
        Set<Ticket> tickets = ticketService.getTicket(userId, target, projectName, ticketId);
        System.out.println(tickets);
        List<TicketResponse> ticketResponseList = new ArrayList<TicketResponse>();
        for (Ticket ticket: tickets){
            Long ticketAssignToId = null;
            String ticketAssignToName = null;
            String ticketAssignToUserName= null;
            Long ticketAssignById = null;
            String ticketAssignByName = null;
            String ticketAssignByUserName= null;
            Timestamp ticketAssignTime= null;
            if (ticket.getTicketAssignToUser() != null){
                ticketAssignToId = ticket.getTicketAssignToUser().getId();
                ticketAssignToName = ticket.getTicketAssignToUser().getName();
                ticketAssignToUserName= ticket.getTicketAssignToUser().getUsername();
            }
            if (ticket.getTicketAssignedBy() != null){
                ticketAssignById = ticket.getTicketAssignedBy().getId();
                ticketAssignByName = ticket.getTicketAssignedBy().getName();
                ticketAssignByUserName= ticket.getTicketAssignedBy().getUsername();
                ticketAssignTime= ticket.getTicketAssignTime();
            }
            ticketResponseList.add(
                    new TicketResponse(
                            ticket.getCategory().getName(),
                            ticket.getCriticalLevel().getName(),
                            ticket.getTitle(),
                            ticket.getDescription(),
                            null,
                            ticket.getStatus().getName(),
                            ticket.getDueDate(),
                            ticketAssignToId,
                            ticketAssignById,
                            ticket.getProject().getCreatedAt(),
                            ticket.getProject().getDescription(),
                            ticket.getProject().getId(),
                            ticket.getProject().getProjectName(),
                            ticket.getProject().getProjectStatus().getName(),
                            ticket.getProject().getUpdatedAt(),
                            ticketAssignTime,
                            ticketAssignToName,
                            ticketAssignToUserName,
                            ticketAssignByName,
                            ticketAssignByUserName,
                            ticket.getCreatedAt(),
                            ticket.getUser().getName(),
                            ticket.getUser().getId(),
                            ticket.getUser().getUsername(),
                            null,
                            ticket.getId(),
                            ticket.getUpdatedAt()
                    )
            );
        }
        HashMap<String, List<TicketResponse>> hashMap = new HashMap<String, List<TicketResponse>>();
        hashMap.put("result", ticketResponseList);
        if (tickets.size() > 0){
            return new ResponseEntity(hashMap, HttpStatus.OK);
        }else{
            throw new FailException("Fail to get tickets");
        }

    }

    @PostMapping(value = "", consumes = { "multipart/form-data"})
    public void createTicket(@RequestPart("ticketTitle") String ticketTitle,
                                     @RequestPart("ticketDescription") String ticketDescription,
                                     @RequestPart("ticketCriticalLevelID") Long ticketCriticalLevelId,
                                     @RequestPart("ticketCategoryID") Long ticketCategoryId,
                                     @RequestPart("projectID") Long projectID,
                                     @RequestPart(value = "image", required=false) List<MultipartFile> files,
                                     HttpSession session,
                                     HttpServletResponse response) throws Exception{
        if (ticketTitle == null || ticketDescription == null || ticketCriticalLevelId == null || ticketCategoryId == null || projectID == null){
            throw new RangeException("EMPTY INPUT");
        }
        Long userId = (Long) session.getAttribute("userId");
        ticketService.createTicket(userId, ticketTitle, ticketDescription, ticketCriticalLevelId, ticketCategoryId, projectID, files);
        response.sendRedirect("/ticket/mySubmittedTicket.html");
    }

    @DeleteMapping("")
    public ResponseEntity deleteTickets(@RequestPart("ticketID") Long ticketId,
                              HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        boolean isTicketOwner = ticketService.isTicketOwner(userId, ticketId);
        ProjectUser projectUser = ticketService.getProjectRole(userId, ticketId);
        if (isTicketOwner || projectUser.getRole().getName() == "project manager" || projectUser.getRole().getName() == "owner"){
            ticketService.deleteTicket(ticketId);
            return new ResponseEntity(new Response(true, "success"), HttpStatus.OK);
        } else {
            throw new FailException("Fail to delete ticket");
        }
    }

    @PutMapping(value = "", consumes = "multipart/form-data")
    public ResponseEntity editTicketInfo(HttpSession session,
                                         @RequestPart("ticketID") Long ticketId,
                                         @RequestPart(value = "isAssignToNull", required=false) Boolean isAssignToNull,
                                         @RequestPart(value = "category", required=false) String category,
                                         @RequestPart(value = "difficulty", required=false) String difficulty,
                                         @RequestPart(value = "criticalLevel", required=false) String criticalLevel,
                                         @RequestPart(value = "dueDate", required=false) String dueDate,
                                         @RequestPart(value = "title", required=false) String title,
                                         @RequestPart(value = "status", required=false) String status,
                                         @RequestPart(value = "description", required=false) String description,
                                         @RequestPart(value = "ticketAssignedToID", required=false) Long ticketAssignedToId) throws Exception
    {
        Long userId = (Long) session.getAttribute("userId");
        System.out.println("ticketId " + ticketId);
        boolean isTicketOwner = ticketService.isTicketOwner(userId, ticketId);
        ProjectUser projectUser = ticketService.getProjectRole(userId, ticketId);
        TicketEditInfo ticketEditInfo = ticketService.getEditTicketInfo(ticketId);
        boolean isAssignToNullOut = false;
        boolean isAssignedChanged = false;
        boolean isAnyChanges = false;
        RightMap rightMap = null;
        System.out.println(ticketEditInfo.getTicketAssignToId().toString());
        System.out.println(userId.toString());
        if (projectUser.getRole().getName() == TicketProjectRole.PM.getRole() || projectUser.getRole().getName() == TicketProjectRole.OWNER.getRole()){
            rightMap = TicketEditRight.SENIOR_RIGHT.getRightMap();
        } else if (ticketEditInfo.getTicketAssignToId().longValue() == userId.longValue()) {
            rightMap = TicketEditRight.BE_ASSIGNED_RIGHT.getRightMap();
        } else if (isTicketOwner) {
            rightMap = TicketEditRight.OWNER_RIGHT.getRightMap();
        } else if (projectUser.getRole().getName() == TicketProjectRole.JUNIOR.getRole() || projectUser.getRole().getName() == TicketProjectRole.SENIOR.getRole()) {
            rightMap = TicketEditRight.DEVELOPER_RIGHT.getRightMap();
        }
        System.out.println(status);
        System.out.println(rightMap.isTicketStatus());
        if (rightMap != null && rightMap.isAssignToNull()){
            isAssignToNullOut = isAssignToNull;
        }else{
            isAssignToNullOut = false;
        }

        if (rightMap.isTicketCategory() && category != null) {
            ticketEditInfo.setTicketCategory(category);
            isAnyChanges = true;
        }
        if (rightMap.isTicketCriticalLevel() && criticalLevel != null) {
            ticketEditInfo.setTicketCriticalLevel(criticalLevel);
            isAnyChanges = true;
        }
        if (rightMap.isTicketDueDate() && dueDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(dueDate);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            ticketEditInfo.setDueDate(timestamp);
            isAnyChanges = true;
        }
        if (rightMap.isTicketTitle() && title != null) {
            ticketEditInfo.setTicketTitle(title);
            isAnyChanges = true;
        }
        if (rightMap.isTicketDescription() && description != null) {
            ticketEditInfo.setTicketDescription(description);
            isAnyChanges = true;
        }
        System.out.println(rightMap.isTicketStatus());
        if (rightMap.isTicketStatus() && status != null) {
            ticketEditInfo.setStatus(status);
            isAnyChanges = true;
        }

        if (rightMap.isAssignToNull() && ticketAssignedToId != null){
            ticketEditInfo.setTicketAssignToId(ticketAssignedToId);
            isAssignedChanged = true;
        }

        if (status != TicketStatus.UNASSIGNED.getStatus() && ticketEditInfo.getTicketAssignToId() == null){
            throw new RangeException("Not yet assign the ticket to others!!!");
        }

        if (isAnyChanges){
            if (isAssignedChanged){
                ticketService.editTicket(ticketEditInfo, isAssignedChanged, ticketId, isAssignToNullOut, userId);
            }else{
                ticketService.editTicket(ticketEditInfo, isAssignedChanged, ticketId, isAssignToNullOut, null);
            }
            TicketEditInfo savedTicket = ticketService.getEditTicketInfo(ticketId);
            return new ResponseEntity(new Response(true, savedTicket), HttpStatus.OK);
        }

        return new ResponseEntity(new Response(false, "Update Ticket failed"), HttpStatus.BAD_REQUEST);

    }

//    @PutMapping("")
//    public void editTicketInfo(HttpSession session,
//                               @RequestPart("ticketID") Long ticketId,
//                               @RequestPart("isAssignToNull") boolean isAssignToNull) throws Exception{
//        Long userId = (Long) session.getAttribute("userId");
//
//    }

    @ExceptionHandler
    public ResponseEntity handleException(Exception exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity handleException(ProjectNotFoundException exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.BAD_REQUEST);
    }
}
