package com.project.bugtracker.controller;

import com.project.bugtracker.entity.*;
import com.project.bugtracker.exception.FailException;
import com.project.bugtracker.exception.Response;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.modal.*;
import com.project.bugtracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/project")
public class ProjectController {
    private ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("{id}")
    public ResponseEntity<ProjectOutput> getProjectInfoById(@PathVariable("id") Integer id){
        Project project = projectService.getProjectById(id);
        ProjectOutput projectOutput = new ProjectOutput(project.getId(),
                                                        project.getUser().getName(),
                                                        project.getProjectName(),
                                                        project.getDescription(),
                                                        project.getProjectStatus().getName()
                                                        );
        for (Ticket ticket: project.getTickets()){
            if (ticket.getTicketAssignToUser() == null){
                projectOutput.addTicket(new ProjectTicket(ticket.getId(), ticket.getTitle(), ticket.getStatus().getName(), ticket.getDueDate(), null));
            }else{
                projectOutput.addTicket(new ProjectTicket(ticket.getId(), ticket.getTitle(), ticket.getStatus().getName(), ticket.getDueDate(), ticket.getTicketAssignToUser().getName()));
            }
        }

        for (ProjectUser projectUser: project.getMembers()){
            projectOutput.addMember(new ProjectMember(projectUser.getUser().getId(), projectUser.getUser().getName(), projectUser.getRole().getName(), projectUser.getRole().getId()));
        }

        return new ResponseEntity(new Response(true, projectOutput), HttpStatus.ACCEPTED);
    }

    @PostMapping("")
    public ResponseEntity createProject(@RequestPart String projectName,
                                        @RequestPart String description,
                                        @RequestPart long statusId,
                                        HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        Long Id = projectService.createProject(new ProjectForm(userId, projectName, description, statusId));
        if (Id != null){
            return new ResponseEntity(new Response(true, Id.toString()), HttpStatus.OK);
        }else{
            throw new FailException("Fail to create a project");
        }
    }

    @PutMapping("{id}")
    public ResponseEntity updateProject(@RequestPart String projectName,
                                        @RequestPart String description,
                                        @RequestPart long statusId,
                                        @PathVariable("id") long projectId){
        boolean isUpdated = projectService.updateProject(new UpdateProjectForm(projectId, projectName, description, statusId));
        if (isUpdated){
            return new ResponseEntity(new Response(true, "Successfully update project information"), HttpStatus.OK);
        }else{
            throw new FailException("Fail to update project information");
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteProject(@PathVariable("id") long projectId, HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        boolean isDeleted = projectService.deleteProject(projectId);
        if (isDeleted){
            return new ResponseEntity(new Response(true, "Successfully delete the project"), HttpStatus.OK);
        } else {
            throw new FailException("Fail to delete project!");
        }
    }

    @GetMapping("member")
    public ResponseEntity getMemberRole(HttpSession session, @RequestParam("projectId") long projectId) {
        Long userId = (Long) session.getAttribute("userId");
        ProjectUser projectUser = projectService.getMemberRole(userId, projectId);
        if (projectUser != null) {
            session.setAttribute("projectRole", projectUser.getRole().getId());
            ProjectMember member = new ProjectMember(userId, projectUser.getUser().getName(), projectUser.getRole().getName(), projectUser.getRole().getId());
            return new ResponseEntity(new Response(true, member), HttpStatus.OK);
        }else{
            throw new FailException("Fail to get member");
        }
    }

    @GetMapping("member/{id}")
    public ResponseEntity getProjectMembers(@PathVariable("id") long projectId){
        Set<ProjectUser> projectUserList = projectService.getProjectUsersByProjectId(projectId);
        if (projectUserList.size() > 0){
            List<ProjectMember> projectMemberList = new ArrayList<ProjectMember>();
            for (ProjectUser projectUser: projectUserList){
                projectMemberList.add(new ProjectMember(projectUser.getUser().getId(), projectUser.getUser().getName(), projectUser.getRole().getName(), projectUser.getRole().getId()));
            }
            return new ResponseEntity(new Response(true, projectMemberList), HttpStatus.OK);
        }else {
            throw new FailException("Fail to get members");
        }
    }

    @PostMapping("member")
    public ResponseEntity addMemberToProject(HttpSession session,
                                             @RequestParam("userId") long addUserId,
                                             @RequestParam("projectId") long projectId,
                                             @RequestParam("roleId") long roleId){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        long id = projectService.addMember(addUserId, projectId, roleId);
        if (id > 0){
            return new ResponseEntity(new Response(true, id), HttpStatus.OK);
        }else{
            throw new FailException("Fail to add members");
        }
    }

    @PutMapping("member")
    public ResponseEntity updateMemberRole(@RequestPart("midRids") List<UpdateMemberRoleInput> midRids,
                                           @RequestParam("projectId") long projectId,
                                           HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        List<UpdateMemberForm> updateMemberFormList = new ArrayList<UpdateMemberForm>();
        for (UpdateMemberRoleInput input: midRids){
            updateMemberFormList.add(new UpdateMemberForm(input.getMemberId(), input.getRoleId(), projectId));
        }
        boolean isUpdate = projectService.updateMemberRole(updateMemberFormList);
        if (isUpdate){
            return new ResponseEntity(new Response(true, "Successfully update the role of the member"), HttpStatus.OK);
        }else{
            throw new FailException("Fail to change the role of the member");
        }
    }

    @DeleteMapping("member")
    public ResponseEntity removeProjectMember(HttpSession session,
                                              @RequestPart("memberIds") List<Long> memberIds,
                                              @RequestParam("projectId") long projectId){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        List<Member> memberList = new ArrayList<Member>();
        for (Long memberId: memberIds){
            memberList.add(new Member(projectId, memberId));
        }
        boolean isRemove = projectService.removeMember(memberList);
        if(isRemove){
            return new ResponseEntity(new Response(true, "Successfully remove the member from the project"), HttpStatus.OK);
        }else{
            throw new FailException("Fail to remove the member from the project");
        }
    }

    @GetMapping("status")
    public ResponseEntity getStatus(HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        List<ProjectStatus> projectStatusList = projectService.getStatus();
        if (projectStatusList.size() > 0){
            return new ResponseEntity(new Response(true, projectStatusList), HttpStatus.OK);
        }else{
            throw new FailException("For some reason, fail to get status information");
        }
    }

    @GetMapping("role")
    public ResponseEntity getRole(HttpSession session) {
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) {return new ResponseEntity(new Response(false, "You are not allowed to delete the project!"), HttpStatus.BAD_REQUEST);}
        List<ProjectRole> projectRoleList = projectService.getRole();
        if (projectRoleList.size() > 0){
            return new ResponseEntity(new Response(true, projectRoleList), HttpStatus.OK);
        }else{
            throw new FailException("For some reason, fail to get role information");
        }
    }

        @ExceptionHandler
    public ResponseEntity handleException(Exception exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity handleException(ProjectNotFoundException exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.BAD_REQUEST);
    }

}
