package com.project.bugtracker.controller;

import com.project.bugtracker.entity.ProjectUser;
import com.project.bugtracker.entity.SystemLevel;
import com.project.bugtracker.entity.User;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.exception.RangeException;
import com.project.bugtracker.exception.Response;
import com.project.bugtracker.modal.*;
import com.project.bugtracker.service.UserService;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@RequestMapping(value = "/api/v1/users")
@RestController
public class UserController {


    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping(value = "register", consumes = { "multipart/form-data" })
    public ResponseEntity save(@RequestPart("username") String username,
                                     @RequestPart("password") String password,
                                     @RequestPart("name") String name,
                                     @RequestPart("icon") MultipartFile file
    ){
        try{
            String iconPath = userService.saveIcon(file);
                User savedUser = userService.saveUser(new User(
                        username,
                        password,
                        name,
                        null,
                        userService.getSystemLevelByName("staff").get(),
                        userService.getRankByName("junior").get(),
                        iconPath
                        ));
                return new ResponseEntity(savedUser,HttpStatus.ACCEPTED);

        }catch (Exception exc){
            exc.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Error Message");
        }
    }

    @GetMapping("current_user")
    public ResponseEntity getCurrentUser(HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) { throw new RangeException("No user Id"); }
        User user = userService.getCurrentUserById(userId);
        CurrentUserInfo currentUserInfo = new CurrentUserInfo(user.getUsername(), user.getIcon(), user.getId());
        CurrentUser currentUser = new CurrentUser(currentUserInfo);
        return new ResponseEntity(currentUser, HttpStatus.OK);
    }

    @GetMapping("info")
    public ResponseEntity getUserInfo(HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) { throw new RangeException("No user Id"); }
        User user = userService.getCurrentUserById(userId);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("userId", Long.toString(userId));
        hashMap.put("name", user.getName());
        hashMap.put("systemLevelId", Long.toString(user.getSystemLevel().getId()));
        hashMap.put("systemLevel", user.getSystemLevel().getName());
        hashMap.put("icon", user.getIcon());
        hashMap.put("rankId", Long.toString(user.getRank().getId()));
        return new ResponseEntity(new Response(true, hashMap), HttpStatus.OK);
    }

    @GetMapping("projects")
    public ResponseEntity getUserProjects(HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) { throw new RangeException("No user Id"); }
        List<ProjectUser> projectUserList = userService.getUserProjects(userId);
        List<ProjectUserResponse> projectUserResponseList = new ArrayList<ProjectUserResponse>();
        for (ProjectUser projectUser: projectUserList){
            projectUserResponseList.add(new ProjectUserResponse(projectUser.getUser().getId(), projectUser.getProject().getId(), projectUser.getProject().getProjectName(), projectUser.getRole().getId(), projectUser.getProject().getProjectStatus().getName(), projectUser.getProject().getDescription()));
        }
        return new ResponseEntity(new Response(true, projectUserResponseList), HttpStatus.OK);
    }

    @GetMapping("/{id:[0-9]+}")
    public ResponseEntity getUsersWhoAreNotInTheProject(@PathVariable("id") String projectId,
                                                        HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) { throw new RangeException("No user Id"); }
        List<User> userList = userService.getUsersWhoAreNotInTheProject(Long.parseLong(projectId));
        List<UsersNotInProjectInfo> usersNotInProjectInfoList = new ArrayList<UsersNotInProjectInfo>();
        System.out.println(userList);
        for (User user: userList){
            usersNotInProjectInfoList.add(new UsersNotInProjectInfo(user.getId(), user.getName(), user.getUsername()));
        }
        return new ResponseEntity(new Response(true, usersNotInProjectInfoList), HttpStatus.OK);
    }

    @GetMapping("user_profile")
    public ResponseEntity getAllUserProfile(){
        List<User> users = userService.getAllUserProfile();
        List<UserProfile> userProfileList = new ArrayList<UserProfile>();
        for (User user: users){
            userProfileList.add(new UserProfile(user.getId(), user.getName(), user.getRank().getName(), user.getSystemLevel().getName(), user.getUsername(), user.getIcon()));
        }
        Map<String, List<UserProfile>> map = new HashMap<String, List<UserProfile>>();
        map.put("users", userProfileList);
        return new ResponseEntity(map, HttpStatus.OK);
    }

    @GetMapping("my_profile")
    public ResponseEntity getUserProfile(HttpSession session){
        Long userId = (Long) session.getAttribute("userId");
        if (userId == null) { throw new RangeException("No user Id"); }
        User user = userService.getUserProfile(userId);

        UserProfile userProfile = new UserProfile(user.getId(), user.getName(), user.getRank().getName(), user.getSystemLevel().getName(), user.getUsername(), user.getIcon());
        Map<String, UserProfile> map = new HashMap<String, UserProfile>();
        map.put("myProfile", userProfile);
        return new ResponseEntity(map, HttpStatus.OK);
    }

    @ExceptionHandler
    public ResponseEntity handleException(Exception exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity handleException(ProjectNotFoundException exc){
        Response error = new Response();
        error.setStatus(false);
        error.setMessage(exc.getMessage());
        return new ResponseEntity<Response>(error, HttpStatus.BAD_REQUEST);
    }
}
