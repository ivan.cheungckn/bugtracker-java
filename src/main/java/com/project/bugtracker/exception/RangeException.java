package com.project.bugtracker.exception;

public class RangeException extends RuntimeException{
    public RangeException(String message){
        super(message);
    }
}
