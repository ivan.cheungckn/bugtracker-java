package com.project.bugtracker.exception;

public class Response {
    private boolean status;
    private Object message;

    public Response(boolean status, Object message) {
        this.status = status;
        this.message = message;
    }
    public Response(){

    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
