package com.project.bugtracker.exception;

public class FailException extends RuntimeException{
    public FailException(String message){
        super(message);
    }
}
