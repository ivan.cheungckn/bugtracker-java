package com.project.bugtracker.dao;

import com.project.bugtracker.entity.AttachedFileTicket;
import com.project.bugtracker.entity.Categories;
import com.project.bugtracker.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface StatusRepository extends JpaRepository<Status, Long> {
    Optional<Status> findByName(String name);

    Set<Status> findAllByOrderByIdAsc();
}
