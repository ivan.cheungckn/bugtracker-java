package com.project.bugtracker.dao;

import com.project.bugtracker.entity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface CategoriesRepository extends JpaRepository<Categories, Long> {
    Set<Categories> findAllByOrderByIdAsc();

    Optional<Categories> findByName(String name);
}
