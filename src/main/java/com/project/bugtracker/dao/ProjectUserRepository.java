package com.project.bugtracker.dao;

import com.project.bugtracker.entity.Project;
import com.project.bugtracker.entity.ProjectStatus;
import com.project.bugtracker.entity.ProjectUser;
import com.project.bugtracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProjectUserRepository extends JpaRepository<ProjectUser,Long> {
    @Modifying
    @Query(nativeQuery = true,  value = "DELETE FROM project_user WHERE project_id = :projectId AND user_id = :userId")
    public int deleteByUserAndProject(@Param("projectId") Long projectId, @Param("userId") Long userId);

    public Optional<ProjectUser> findByUserAndProject(User user, Project project);

    public Optional<List<ProjectUser>> findAllByUser(User user);

    public Optional<ProjectUser> findByUser(User user);

    public Optional<List<ProjectUser>> findAllByProjectIn(List<Project> projects);

}
