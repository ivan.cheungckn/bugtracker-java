package com.project.bugtracker.dao;

import com.project.bugtracker.entity.ProjectRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRoleRepository extends JpaRepository<ProjectRole, Long> {

}
