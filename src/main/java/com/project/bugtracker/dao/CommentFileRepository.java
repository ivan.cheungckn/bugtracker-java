package com.project.bugtracker.dao;

import com.project.bugtracker.entity.CommentFile;
import com.project.bugtracker.entity.ProjectRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentFileRepository extends JpaRepository<CommentFile, Long> {

}
