package com.project.bugtracker.dao;

import com.project.bugtracker.entity.Comment;
import com.project.bugtracker.entity.CommentFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface
CommentRepository extends JpaRepository<Comment, Long> {
}
