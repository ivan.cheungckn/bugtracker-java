package com.project.bugtracker.dao;

import com.project.bugtracker.auth.MyUserDetail;
import com.project.bugtracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUsername(String username);

    Optional<List<User>> findAllByIdIsNotIn(List<Long> Ids);

}
