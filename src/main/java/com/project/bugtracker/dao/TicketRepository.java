package com.project.bugtracker.dao;

import com.project.bugtracker.entity.Project;
import com.project.bugtracker.entity.Ticket;
import com.project.bugtracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Set<Ticket> findByTicketAssignedByOrderByCreatedAtDesc(@Param("user") User user);

    Set<Ticket> findByUserOrderByCreatedAtDesc(@Param("user") User user);

    Set<Ticket> findByUserAndProjectOrderByCreatedAtDesc(@Param("user") User user, @Param("project") Project project);

    Set<Ticket> findAllByProjectIn(List<Project> projects);

    Optional<Ticket> findByUserAndId(User user, long Id);
}
