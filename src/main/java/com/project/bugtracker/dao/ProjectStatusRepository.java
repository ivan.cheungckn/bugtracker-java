package com.project.bugtracker.dao;

import com.project.bugtracker.entity.ProjectRole;
import com.project.bugtracker.entity.ProjectStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProjectStatusRepository extends JpaRepository<ProjectStatus, Long> {
    Optional<ProjectStatus> findByName(String name);
}
