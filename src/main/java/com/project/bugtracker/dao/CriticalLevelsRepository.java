package com.project.bugtracker.dao;

import com.project.bugtracker.entity.AttachedFileTicket;
import com.project.bugtracker.entity.Categories;
import com.project.bugtracker.entity.CriticalLevels;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface CriticalLevelsRepository extends JpaRepository<CriticalLevels, Long> {
    Set<CriticalLevels> findAllByOrderByIdAsc();

    Optional<CriticalLevels> findByName(String name);
}
