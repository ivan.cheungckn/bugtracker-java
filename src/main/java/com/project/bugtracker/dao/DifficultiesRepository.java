package com.project.bugtracker.dao;

import com.project.bugtracker.entity.AttachedFileTicket;
import com.project.bugtracker.entity.Difficulties;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DifficultiesRepository extends JpaRepository<Difficulties, Long> {

}
