package com.project.bugtracker.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class SaveRepository {
    @Value("${upload.photo.path}")
    private String saveFolderPath;

    public String saveFile(MultipartFile file, String category) throws Exception {
        byte[] bytes = file.getBytes();
        Path path = Paths.get(saveFolderPath + System.currentTimeMillis() + '-'+ file.getOriginalFilename());
        Files.write(path, bytes);
        System.out.println(path);
        return System.currentTimeMillis() + category + file.getOriginalFilename();
    }
}
