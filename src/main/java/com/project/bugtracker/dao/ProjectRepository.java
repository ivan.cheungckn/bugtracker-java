package com.project.bugtracker.dao;

import com.project.bugtracker.entity.Project;
import com.project.bugtracker.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query(value = "SELECT 'user'.id as 'memberId', user.name, project_role.name as 'role', project_role.id as 'roleId' FROM project_user INNER JOIN project_role ON project_user.role_id = project_role.id INNER JOIN 'user' ON project_user.user_id = 'user'.id WHERE project_user.project_id = :projectId AND 'user'.id = :userId", nativeQuery = true)
    List<User> findProjectMemberRole (@Param("userId") long userId, @Param("projectId") long projectId);

    Optional<Project> findByProjectName (String projectName);

    Set<Project> findAllByOrderByCreatedAtAsc();

//    @Query(value = "INSERT INTO project_user (user_id, project_id, role_id) VALUES (:userId, :projectId, :roleId) RETURNING id ", nativeQuery = true)
//    Integer addMember(@Param("userId") int userId, @Param("projectId") int projectId, @Param("roleId") int roleId);
}
