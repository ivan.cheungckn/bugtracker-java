package com.project.bugtracker.dao;

import com.project.bugtracker.entity.SystemLevel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SystemLevelRepository extends JpaRepository<SystemLevel,Long> {
    Optional<SystemLevel> findByName(String name);
}
