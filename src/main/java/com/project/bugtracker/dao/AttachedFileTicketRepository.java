package com.project.bugtracker.dao;

import com.project.bugtracker.entity.AttachedFileTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface AttachedFileTicketRepository extends JpaRepository<AttachedFileTicket, Long> {
    @Query(nativeQuery = true, value = "SELECT ticket_id, file_name FROM attached_file_ticket WHERE ticket_id IN :ticketIds")
    List<AttachedFileTicket> getFilesByTicketIds(@Param("ticketIds") Set<Long> ticketIds);
}
