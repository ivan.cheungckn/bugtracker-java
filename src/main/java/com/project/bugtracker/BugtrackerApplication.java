package com.project.bugtracker;

import com.project.bugtracker.dao.ProjectRepository;
import com.project.bugtracker.dao.RankRepository;
import com.project.bugtracker.dao.SystemLevelRepository;
import com.project.bugtracker.dao.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = {UserRepository.class, ProjectRepository.class, RankRepository.class, SystemLevelRepository.class})
public class BugtrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BugtrackerApplication.class, args);
    }

}
