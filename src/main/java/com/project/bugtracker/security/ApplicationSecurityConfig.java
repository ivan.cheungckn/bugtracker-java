package com.project.bugtracker.security;

import com.project.bugtracker.auth.ApplicationUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    @Qualifier("userService")
    UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()
//                .antMatchers("/js/login","/js/register").permitAll()
//                .antMatchers("/css/login","/css/register").permitAll()
                .antMatchers("/private/**").hasAnyRole(ApplicationUserRole.MANAGER.name(), ApplicationUserRole.ADMIN.name(), ApplicationUserRole.STAFF.name())
//                .antMatchers("/").hasAnyRole(ApplicationUserRole.MANAGER.name(), ApplicationUserRole.ADMIN.name(), ApplicationUserRole.STAFF.name())
                .and()
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/authenticateTheUser").successHandler(authenticationSuccessHandler)
                .permitAll();

    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }
}
