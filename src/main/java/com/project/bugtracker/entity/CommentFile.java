package com.project.bugtracker.entity;

import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "comment_file")
public class CommentFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_id")
    private Comment comment;

    @Column(name = "file_name")
    private String fileName;

    public CommentFile() {
    }

    public CommentFile(Comment comment, @Nullable String fileName) {
        this.comment = comment;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "CommentFile{" +
                "id=" + id +
                ", comment=" + comment +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
