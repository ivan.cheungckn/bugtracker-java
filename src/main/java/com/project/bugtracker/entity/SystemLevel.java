package com.project.bugtracker.entity;

import javax.persistence.*;

@Entity
@Table(name = "system_level")
public class SystemLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    public SystemLevel() {
    }

    public SystemLevel(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SystemLevel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
