package com.project.bugtracker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
//    @JsonProperty("projectId")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
//    @JsonProperty("owner")
    private User user;

    @Column(name = "project_name", nullable = false, unique = true)
//    @JsonProperty("projectName")
    private String projectName;

    @Column(name = "description", columnDefinition = "text")
//    @JsonProperty("description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
//    @JsonProperty("status")
    private ProjectStatus projectStatus;

    @Column(name = "created_at", updatable=false)
    @CreationTimestamp
//    @JsonProperty("createdAt")
    private Timestamp createdAt;

    @Column(name = "updated_at")
//    @JsonProperty("updatedAt")
    @UpdateTimestamp
    private Timestamp updatedAt;

    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JsonProperty("tickets")
    private Set<Ticket> tickets;

    @OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
//    @JsonProperty("members")
    private Set<ProjectUser> members;

    public Project() {
    }

    public Project(User user, String projectName, String description, ProjectStatus projectStatus) {
        this.user = user;
        this.projectName = projectName;
        this.description = description;
        this.projectStatus = projectStatus;
    }

    public Project(User user, String projectName, ProjectStatus projectStatus) {
        this.user = user;
        this.projectName = projectName;
        this.projectStatus = projectStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectStatus getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(ProjectStatus projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Set<ProjectUser> getMembers() {
        return members;
    }

    public void setMembers(Set<ProjectUser> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", user=" + user +
                ", projectName='" + projectName + '\'' +
                ", description='" + description + '\'' +
                ", projectStatus=" + projectStatus +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", tickets=" + tickets +
                ", members=" + members +
                '}';
    }
}
