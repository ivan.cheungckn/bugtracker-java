package com.project.bugtracker.entity;

import org.hibernate.annotations.Type;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "`user`")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false, columnDefinition = "text")
    private String password;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "last_login")
    @Type(type="timestamp")
    private Date lastLogin;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "system_level_id")
    private SystemLevel systemLevel;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rank_id")
    private Rank rank;

    @Column(name = "icon", columnDefinition = "text")
    private String icon;

    public User() {
    }

    public User(String username, String password, String name, @Nullable Date lastLogin, @Nullable SystemLevel systemLevel, @Nullable Rank rank, @Nullable String icon) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastLogin = lastLogin;
        this.systemLevel = systemLevel;
        this.rank = rank;
        this.icon = icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public com.project.bugtracker.entity.SystemLevel getSystemLevel() {
        return systemLevel;
    }

    public void setSystemLevel(com.project.bugtracker.entity.SystemLevel systemLevel) {
        systemLevel = systemLevel;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", lastLogin=" + lastLogin +
                ", systemLevel=" + systemLevel +
                ", rank=" + rank +
                ", icon='" + icon + '\'' +
                '}';
    }
}
