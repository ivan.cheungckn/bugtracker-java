package com.project.bugtracker.entity;

import javax.persistence.*;

@Entity
@Table(name = "attached_file_ticket")
public class AttachedFileTicket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    public AttachedFileTicket() {
    }

    public AttachedFileTicket(String fileName, Ticket ticket) {
        this.fileName = fileName;
        this.ticket = ticket;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public String toString() {
        return "AttachedFileTicket{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", ticket=" + ticket +
                '}';
    }
}
