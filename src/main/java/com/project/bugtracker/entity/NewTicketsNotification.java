package com.project.bugtracker.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "new_tickets_notification")
public class NewTicketsNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(optional = false)
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @ManyToOne(optional = false)
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "project_name", nullable = false)
    private String projectName;

    @Column(name = "ticket_title", nullable = false)
    private String ticketTitle;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "is_read", nullable = false)
    private boolean isRead;

    @Column(name = "ticket_created_at")
    private Timestamp ticketCreatedAt;

    @Column(name = "created_at", updatable=false)
    @CreationTimestamp
    private Timestamp createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Timestamp updatedAt;

    public NewTicketsNotification() {
    }

    public NewTicketsNotification(Ticket ticket, Project project, String projectName, String ticketTitle, User user, boolean isRead) {
        this.ticket = ticket;
        this.project = project;
        this.projectName = projectName;
        this.ticketTitle = ticketTitle;
        this.user = user;
        this.isRead = isRead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTicketTitle() {
        return ticketTitle;
    }

    public void setTicketTitle(String ticketTitle) {
        this.ticketTitle = ticketTitle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Timestamp getTicketCreatedAt() {
        return ticketCreatedAt;
    }

    public void setTicketCreatedAt(Timestamp ticketCreatedAt) {
        this.ticketCreatedAt = ticketCreatedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "NewTicketsNotification{" +
                "id=" + id +
                ", ticket=" + ticket +
                ", project=" + project +
                ", projectName='" + projectName + '\'' +
                ", ticketTitle='" + ticketTitle + '\'' +
                ", user=" + user +
                ", isRead=" + isRead +
                ", ticketCreatedAt=" + ticketCreatedAt +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
