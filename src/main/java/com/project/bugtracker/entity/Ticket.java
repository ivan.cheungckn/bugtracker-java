package com.project.bugtracker.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "difficulty_id")
    private Difficulties difficulty;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "history")
    private Timestamp history;

    @ManyToOne
    @JoinColumn(name = "critical_level_id")
    private CriticalLevels criticalLevel;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Categories category;

    @Column(name = "due_date")
    private Timestamp dueDate;

    @ManyToOne()
    @JoinColumn(name = "ticket_assign_to_id")
    private User ticketAssignToUser;

    @Column(name = "ticket_assign_time")
    private Timestamp ticketAssignTime;

    @Column(name = "created_at", updatable=false)
    @CreationTimestamp
    private Timestamp createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Timestamp updatedAt;

    @ManyToOne
    @JoinColumn(name = "ticket_assigned_by_id")
    private User ticketAssignedBy;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Comment> commentSet;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL)
    private List<AttachedFileTicket> attachedFileTicketList;

    public Ticket() {
    }

    public Ticket(User user, Project project, String title, String description, Status status, CriticalLevels criticalLevel, Categories category) {
        this.user = user;
        this.project = project;
        this.title = title;
        this.description = description;
        this.status = status;
        this.criticalLevel = criticalLevel;
        this.category = category;
    }

    public Set<Comment> getCommentSet() {
        return commentSet;
    }

    public void setCommentSet(Set<Comment> commentSet) {
        this.commentSet = commentSet;
    }

    public List<AttachedFileTicket> getAttachedFileTicketList() {
        return attachedFileTicketList;
    }

    public void setAttachedFileTicketList(List<AttachedFileTicket> attachedFileTicketSet) {
        this.attachedFileTicketList = attachedFileTicketSet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Difficulties getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulties difficulty) {
        this.difficulty = difficulty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Timestamp getHistory() {
        return history;
    }

    public void setHistory(Timestamp history) {
        this.history = history;
    }

    public CriticalLevels getCriticalLevel() {
        return criticalLevel;
    }

    public void setCriticalLevel(CriticalLevels criticalLevel) {
        this.criticalLevel = criticalLevel;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public Timestamp getDueDate() {
        return dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    public User getTicketAssignToUser() {
        return ticketAssignToUser;
    }

    public void setTicketAssignToUser(User ticketAssignToUser) {
        this.ticketAssignToUser = ticketAssignToUser;
    }

    public Timestamp getTicketAssignTime() {
        return ticketAssignTime;
    }

    public void setTicketAssignTime(Timestamp ticketAssignTime) {
        this.ticketAssignTime = ticketAssignTime;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getTicketAssignedBy() {
        return ticketAssignedBy;
    }

    public void setTicketAssignedBy(User ticketAssignedBy) {
        this.ticketAssignedBy = ticketAssignedBy;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", user=" + user.getId() +
                ", project=" + project.getId() +
                ", difficulty=" + difficulty +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", history=" + history +
                ", criticalLevel=" + criticalLevel +
                ", category=" + category +
                ", dueDate=" + dueDate +
                ", ticketAssignToUser=" + ticketAssignToUser +
                ", ticketAssignTime=" + ticketAssignTime +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", ticketAssignedBy=" + ticketAssignedBy+
                '}';
    }
}

