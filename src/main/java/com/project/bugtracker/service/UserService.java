package com.project.bugtracker.service;
import com.project.bugtracker.auth.MyUserDetail;
import com.project.bugtracker.dao.*;
import com.project.bugtracker.entity.*;
import com.project.bugtracker.exception.RangeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private UserRepository userRepository;
    private SystemLevelRepository systemLevelRepository;
    private RankRepository rankRepository;
    private SaveRepository saveRepository;
    private ProjectUserRepository projectUserRepository;
    private ProjectRepository projectRepository;

    @Autowired
    public UserService(SystemLevelRepository systemLevelRepository,
                       UserRepository userRepository,
                       RankRepository rankRepository,
                       SaveRepository saveRepository,
                       ProjectUserRepository projectUserRepository,
                       ProjectRepository projectRepository){
        this.systemLevelRepository = systemLevelRepository;
        this.userRepository = userRepository;
        this.rankRepository = rankRepository;
        this.saveRepository = saveRepository;
        this.projectUserRepository = projectUserRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername((username));

        user.orElseThrow(()-> new UsernameNotFoundException(String.format("Username %s not found", username)));
        return user.map(MyUserDetail::new).get();
    }

    @Transactional
    public User saveUser(User user){
        User savedUser = userRepository.saveAndFlush(user);
        return savedUser;
    }

    @Transactional
    public Optional<SystemLevel> getSystemLevelByName(String name){
        Optional<SystemLevel> systemLevel =  systemLevelRepository.findByName(name);
        return systemLevel;
    }

    @Transactional
    public List<ProjectUser> getUserProjects(Long userId){
        User user = userRepository.findById(userId).orElseThrow(()->new RangeException("User not found"));
        List<ProjectUser> projectUser = projectUserRepository.findAllByUser(user).orElseThrow(()-> new RangeException("Project not found"));
        return projectUser;
    }

    @Transactional
    public Optional<Rank> getRankByName(String name){
        Optional<Rank> rank =  rankRepository.findByName(name);
        return rank;
    }

    @Transactional
    public String saveIcon(MultipartFile file) throws Exception {
        String saveLocation =  saveRepository.saveFile(file,"icon");
        return saveLocation;
    }

    @Transactional
    public User getCurrentUserById(Long userId){
        User user = userRepository.findById(userId).orElseThrow(()-> new RangeException("Fail to find current user"));
        return user;
    }

    @Transactional
    public List<User> getUsersWhoAreNotInTheProject(Long projectId){
        Project project = projectRepository.findById(projectId).orElseThrow(()-> new RangeException("Invalid projectId"));
        List<Project> projectList = new ArrayList<Project>();
        projectList.add(project);
        List<ProjectUser> projectUserList = projectUserRepository.findAllByProjectIn(projectList).get();
        List<Long> usersIdsInProject = projectUserList.stream().map(u->u.getUser().getId()).collect(Collectors.toList());
        List<User> usersNotInProject = userRepository.findAllByIdIsNotIn(usersIdsInProject).orElseThrow(()->new RangeException("Fail to load users not in project"));
        return usersNotInProject;
    }

    @Transactional
    public List<User> getAllUserProfile(){
        List<User> userList = userRepository.findAll();
        return userList;
    }

    @Transactional
    public User getUserProfile(Long Id){
        User user = userRepository.findById(Id).orElseThrow(()->new RangeException("User not found"));
        return user;
    }

}

