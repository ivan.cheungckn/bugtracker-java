package com.project.bugtracker.service;

import com.project.bugtracker.dao.*;
import com.project.bugtracker.entity.*;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.exception.RangeException;
import com.project.bugtracker.modal.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    private ProjectRepository projectRepository;
    private UserRepository userRepository;
    private ProjectStatusRepository projectStatusRepository;
    private ProjectRoleRepository projectRoleRepository;
    private ProjectUserRepository projectUserRepository;

    @Autowired
    public ProjectService(ProjectRepository theProjectRepository,
                          UserRepository theUserRepository,
                          ProjectStatusRepository theProjectStatusRepository,
                          ProjectRoleRepository theProjectRoleRepository,
                          ProjectUserRepository theProjectUserRepository){
        this.projectRepository = theProjectRepository;
        this.userRepository = theUserRepository;
        this.projectStatusRepository = theProjectStatusRepository;
        this.projectRoleRepository = theProjectRoleRepository;
        this.projectUserRepository = theProjectUserRepository;
    }

    @Transactional
    public Project getProjectById(long Id) throws ProjectNotFoundException{
        Optional<Project> project =  projectRepository.findById(Id);
        if (project.isPresent()){
            return project.get();
        }else {
            throw new ProjectNotFoundException("Project is not found!");
        }
    }

    @Transactional
    public List<User> getProjectMemberByProjectId(long Id) throws ProjectNotFoundException{
        Optional<Project> project =  projectRepository.findById(Id);
        if (project.isPresent()){
            return project.get().getMembers().stream().map((member)->member.getUser()).collect(Collectors.toList());
        }else {
            throw new ProjectNotFoundException("Project is not found!");
        }
    }

    @Transactional
    public Set<ProjectUser> getProjectUsersByProjectId(long Id) throws ProjectNotFoundException{
        Optional<Project> project =  projectRepository.findById(Id);
        if (project.isPresent()){
            return project.get().getMembers();
        }else {
            throw new ProjectNotFoundException("Project is not found!");
        }
    }

    @Transactional
    public ProjectUser getMemberRole (long userId, long projectId) throws RangeException{
        if (!(userId > 0) || !(projectId > 0)){
            throw new RangeException("Invalid project id, the id must be an integer and > 0");
        }
        Optional<Project> project =  projectRepository.findById(projectId);
        if (project.isPresent()){
            ProjectUser projectUser =  project.get().getMembers().stream().filter(member-> member.getId() == userId).findFirst()
                    .orElseThrow(()->new RangeException("No access right to the project!"));
            return projectUser;
        }else {
            throw new ProjectNotFoundException("Project is not found!");
        }
    }

    @Transactional
    public long createProject(ProjectForm projectForm) throws RangeException{
        if (!(projectForm.getUserId()>0) || !(projectForm.getStatusId()>0)){
            throw new RangeException("Invalid statusId or userId");
        }
        if (projectForm.getDescription().trim().length() == 0 || projectForm.getProjectName().trim().length() == 0){
            throw new RangeException("Project Name and Description cannot be empty");
        }
        User user = userRepository.findById(projectForm.getUserId()).orElseThrow(()->new RangeException("No User"));
        ProjectStatus projectStatus = projectStatusRepository.findById(projectForm.getStatusId()).orElseThrow(()->new RangeException("Invalid status id"));

        Project project = new Project(user, projectForm.getProjectName(), projectForm.getDescription(), projectStatus);
        Project savedProject = projectRepository.saveAndFlush(project);
        return savedProject.getId();
    }

    @Transactional
    public boolean updateProject(UpdateProjectForm updateProjectForm) throws RangeException{
        if (!(updateProjectForm.getProjectId()>0) || !(updateProjectForm.getStatusId()>0)){
            throw new RangeException("Invalid statusId or projectId");
        }
        if (updateProjectForm.getDescription().trim().length() == 0 || updateProjectForm.getProjectName().trim().length() == 0){
            throw new RangeException("Project Name and Description cannot be empty");
        }
        Project project = projectRepository.findById(updateProjectForm.getProjectId())
                                                .orElseThrow(()->new RangeException("Project is not found!"));
        ProjectStatus projectStatus = projectStatusRepository.findById(updateProjectForm.getStatusId())
                                                                .orElseThrow(()->new RangeException("Status is not found!"));
        project.setProjectName(updateProjectForm.getProjectName());
        project.setProjectStatus(projectStatus);
        project.setDescription(updateProjectForm.getDescription());
        projectRepository.saveAndFlush(project);
        return true;
    }

    @Transactional
    public boolean deleteProject(long projectId) throws RangeException{
        if (projectId <= 0){
            throw new RangeException("Id must be an integer and > 0");
        }
        projectRepository.deleteById(projectId);
        return true;
    }

    @Transactional
    public List<User> getMembersByProjectId(long projectId){
        if (projectId <= 0){
            throw new RangeException("Id must be an integer and > 0");
        }
        Project project = projectRepository.findById(projectId).orElseThrow(()->new RangeException("Project is not found!"));
        List<User> users = new ArrayList<User>();
        for (ProjectUser projectUser:project.getMembers()){
            users.add(projectUser.getUser());
        }
        return users;
    }

    @Transactional
    public long addMember(long userId, long projectId, long roleId){
        if (!( userId > 0 ) || !( projectId > 0 ) || !( roleId > 0 )){
            throw new RangeException("Invalid Id(s)");
        }
        User user = userRepository.findById(userId).orElseThrow(()->new RangeException("No user"));
        Project project = projectRepository.findById(projectId).orElseThrow(()->new RangeException("No project"));
        ProjectRole projectRole = projectRoleRepository.findById(roleId).orElseThrow(()->new RangeException("No ProjectRole"));
        ProjectUser projectUser = new ProjectUser(user, project, projectRole);
        ProjectUser savedProjectUser = projectUserRepository.saveAndFlush(projectUser);
        return savedProjectUser.getId();
    }

    @Transactional
    public boolean removeMember(List<Member> members){
        for (Member member:members){
            if (!( member.getProjectId() > 0 ) || !( member.getMemberId() > 0 )){
                throw new RangeException("Invalid Id(s)");
            }
            int deletedRows = projectUserRepository.deleteByUserAndProject(member.getProjectId(), member.getMemberId());
            if (deletedRows == 0){
                throw new RangeException("Project/Project Member does not exist");
            }
        }
        return true;
    }

    @Transactional
    public boolean updateMemberRole(List<UpdateMemberForm> memberForms){
        for (UpdateMemberForm memberForm: memberForms){
            if (!( memberForm.getProjectId() > 0 ) || !( memberForm.getMemberId() > 0 )){
                throw new RangeException("Invalid Id(s)");
            }
            User user = userRepository.findById(memberForm.getMemberId()).orElseThrow(()->new RangeException("Project/Project Member are not found in database"));
            Project project = projectRepository.findById(memberForm.getProjectId()).orElseThrow(()->new RangeException("Project/Project Member are not found in database"));
            ProjectUser projectUser = projectUserRepository.findByUserAndProject(user, project).orElseThrow(()->new RangeException("Project/Project Member are not found in database"));
            ProjectRole projectRole = projectRoleRepository.findById(memberForm.getRoleId()).orElseThrow(()->new RangeException("Invalid project role"));
            projectUser.setRole(projectRole);
            projectUserRepository.saveAndFlush(projectUser);
        }
        return true;
    }

    @Transactional
    public List<ProjectStatus> getStatus(){
        List<ProjectStatus> projectStatusList = projectStatusRepository.findAll();
        return projectStatusList;
    }

    @Transactional
    public List<ProjectRole> getRole(){
        List<ProjectRole> projectRoles = projectRoleRepository.findAll();
        return projectRoles;
    }

}
