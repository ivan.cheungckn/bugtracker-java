package com.project.bugtracker.service;

import com.project.bugtracker.dao.*;
import com.project.bugtracker.entity.*;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.exception.RangeException;
import com.project.bugtracker.modal.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TicketService {
    TicketRepository ticketRepository;
    UserRepository userRepository;
    ProjectRepository projectRepository;
    ProjectUserRepository projectUserRepository;
    SaveRepository saveRepository;
    DifficultiesRepository difficultiesRepository;
    CriticalLevelsRepository criticalLevelsRepository;
    CategoriesRepository categoriesRepository;
    StatusRepository statusRepository;
    AttachedFileTicketRepository attachedFileTicketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository,
                         UserRepository userRepository,
                         ProjectRepository projectRepository,
                         ProjectUserRepository projectUserRepository,
                         SaveRepository saveRepository,
                         DifficultiesRepository difficultiesRepository,
                         CriticalLevelsRepository criticalLevelsRepository,
                         CategoriesRepository categoriesRepository,
                         StatusRepository statusRepository,
                         AttachedFileTicketRepository attachedFileTicketRepository) {
        this.ticketRepository = ticketRepository;
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.projectUserRepository = projectUserRepository;
        this.saveRepository = saveRepository;
        this.difficultiesRepository = difficultiesRepository;
        this.criticalLevelsRepository = criticalLevelsRepository;
        this.categoriesRepository = categoriesRepository;
        this.statusRepository = statusRepository;
        this.attachedFileTicketRepository = attachedFileTicketRepository;
    }

    @Transactional
    public Set<Ticket> getTicket(long userId, String target, String projectName, Long ticketId){
        Set<Ticket> tickets = null;
        Ticket ticket = null;
        User user = userRepository.findById(userId).orElseThrow(()->new RangeException("User not found!"));
        switch (target){
            case "assignTo":
                tickets = ticketRepository.findByTicketAssignedByOrderByCreatedAtDesc(user);
                break;
            case "createdBy":
                tickets = ticketRepository.findByUserOrderByCreatedAtDesc(user);
                break;
            case "oneProjectTickets_Iin":
                Project projectForOne = projectRepository.findByProjectName(projectName).orElseThrow(()->new RangeException("Project not found!"));
                projectUserRepository.findByUserAndProject(user,projectForOne).orElseThrow(()->new RangeException("Project not found!"));
                tickets = ticketRepository.findByUserAndProjectOrderByCreatedAtDesc(user, projectForOne);
                break;
            case "allProjectsTickets_Iin":
                System.out.println(1);
                List<ProjectUser> projectUsers = projectUserRepository.findAllByUser(user).orElseThrow(()->new RangeException("Project not found!"));
                System.out.println(2);
                List<Project> projects =  projectUsers.stream().map(ProjectUser::getProject).collect(Collectors.toList());
                System.out.println(3);
                tickets = ticketRepository.findAllByProjectIn(projects);
                System.out.println(4);
                break;
            case "byID":
                ProjectUser projectUser = projectUserRepository.findByUser(user).orElseThrow(()->new RangeException("You are not in the project!"));
                ticket = ticketRepository.findById(ticketId).orElseThrow(()->new RangeException("Ticket not found!"));
                tickets.add(ticket);
                break;
        }
        return tickets;
    }

    @Transactional
    public long createTicket(long userId, String title, String description, Long criticalLevelId, Long categoryId, Long projectId, @Nullable List<MultipartFile> files) throws Exception {
        User user = userRepository.findById(userId).orElseThrow(()->new RangeException("User not found!"));
        Project project = projectRepository.findById(projectId).orElseThrow(()->new RangeException("Project not found!"));
        CriticalLevels criticalLevel = criticalLevelsRepository.findById(criticalLevelId).orElseThrow(()->new RangeException("Critical Level not found!"));
        Categories category = categoriesRepository.findById(categoryId).orElseThrow(()->new RangeException("Category not found!"));
        Status status = statusRepository.findByName("unassigned").orElseThrow(()->new RangeException("Status not found!"));

        Ticket ticket = new Ticket(user, project, title, description, status, criticalLevel, category);

        List<AttachedFileTicket> attachedFileTicketList = new ArrayList<AttachedFileTicket>();
        for (MultipartFile file: files){
            String path = saveRepository.saveFile(file, "ticket");
            attachedFileTicketList.add(new AttachedFileTicket(path, ticket));
        }
        ticket.setAttachedFileTicketList(attachedFileTicketList);
        Ticket savedTicket = ticketRepository.saveAndFlush(ticket);
        return savedTicket.getId();
    }

    @Transactional
    public boolean deleteTicket(long ticketId){
        ticketRepository.deleteById(ticketId);
        return true;
    }

    @Transactional
    public boolean isTicketOwner(long userId, long ticketId){
        User user = userRepository.findById(userId).orElseThrow(()-> new RangeException("User not found!"));
        if (ticketRepository.findByUserAndId(user, ticketId).isPresent()){
            return true;
        }else{
            return false;
        }
    }

    @Transactional
    public Set<TicketStatusInfo> getStatus(){
        Set<Status> statuses = statusRepository.findAllByOrderByIdAsc();
        Set<TicketStatusInfo> ticketStatusInfos = new HashSet<TicketStatusInfo>();
        for (Status status: statuses){
            ticketStatusInfos.add(new TicketStatusInfo(status.getId(), status.getName()));
        }
        return ticketStatusInfos;
    }

    @Transactional
    public Set<TicketCategoriesInfo> getCategories(){
        Set<Categories> categories = categoriesRepository.findAllByOrderByIdAsc();
        Set<TicketCategoriesInfo> ticketCategoriesInfos = new HashSet<TicketCategoriesInfo>();
        for (Categories category: categories){
            ticketCategoriesInfos.add(new TicketCategoriesInfo(category.getId(), category.getName()));
        }
        return ticketCategoriesInfos;
    }

    @Transactional
    public Set<TicketCriticalLevelsInfo> getCriticalLevels(){
        Set<CriticalLevels> criticalLevels = criticalLevelsRepository.findAllByOrderByIdAsc();
        Set<TicketCriticalLevelsInfo> ticketCriticalLevelsInfos = new HashSet<TicketCriticalLevelsInfo>();
        for (CriticalLevels criticalLevel: criticalLevels){
            ticketCriticalLevelsInfos.add(new TicketCriticalLevelsInfo(criticalLevel.getId(), criticalLevel.getName()));
        }
        return ticketCriticalLevelsInfos;
    }

    @Transactional
    public Set<TicketProjectInfo> getProjects(){
        Set<Project> projects = projectRepository.findAllByOrderByCreatedAtAsc();
        Set<TicketProjectInfo> ticketProjectInfos = new HashSet<TicketProjectInfo>();
        for (Project project: projects){
            ticketProjectInfos.add(new TicketProjectInfo(project.getId(), project.getProjectName()));
        }
        return ticketProjectInfos;
    }

    @Transactional
    public Set<TicketUserInfo> getUsers(){
        List<User> users = userRepository.findAll();
        Set<TicketUserInfo> userInfos = new HashSet<TicketUserInfo>();
        for (User user: users){
           userInfos.add(new TicketUserInfo(user.getId(), user.getName(), user.getUsername()));
        }
        return userInfos;
    }

    @Transactional
    public List<AttachedFileTicket> getAttachedTicketFiles(Set<Long> ticketIds){
        return (attachedFileTicketRepository.getFilesByTicketIds(ticketIds));
    }

    @Transactional
    public ProjectUser getProjectRole (long userId, long ticketId){
        if (!(userId > 0) || !(ticketId > 0)){
            throw new RangeException("Invalid project id, the id must be an integer and > 0");
        }
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(()->new RangeException("Ticket not found!"));
        Optional<Project> project =  projectRepository.findById(ticket.getProject().getId());
        if (project.isPresent()){
            ProjectUser projectUser =  project.get().getMembers().stream().filter(member-> member.getId() == userId).findFirst()
                    .orElseThrow(()->new RangeException("No access right to the project!"));
            return projectUser;
        }else {
            throw new ProjectNotFoundException("Project is not found!");
        }
    }

    @Transactional
    public void editTicket(TicketEditInfo editTicketForm, boolean isAssignedChanged, long ticketId, boolean isAssignToNull, @Nullable Long assignById){
        Categories category = categoriesRepository.findByName(editTicketForm.getTicketCategory()).orElseThrow(()->new RangeException("Category not found!"));
        CriticalLevels criticalLevel = criticalLevelsRepository.findByName(editTicketForm.getTicketCriticalLevel()).orElseThrow(()->new RangeException("Critical Level not found!"));
        Status status = statusRepository.findByName(editTicketForm.getStatus()).orElseThrow(()->new RangeException("Status not found!"));
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(()->new RangeException("ticket not found!"));
        ticket.setCategory(category);
        ticket.setCriticalLevel(criticalLevel);
        ticket.setDueDate(editTicketForm.getDueDate());
        ticket.setStatus(status);
        ticket.setTitle(editTicketForm.getTicketTitle());
        ticket.setDescription(editTicketForm.getTicketDescription());
        if (isAssignToNull){
            ticket.setTicketAssignToUser(null);
            ticket.setTicketAssignedBy(null);
            ticket.setTicketAssignTime(null);
            ticketRepository.saveAndFlush(ticket);
        }else if (isAssignedChanged){
            User assignToUser = userRepository.findById(editTicketForm.getTicketAssignToId()).orElseThrow(()->new RangeException("Ticket Assign To User not found!"));
            User assignByUser = userRepository.findById(editTicketForm.getTicketAssignById()).orElseThrow(()->new RangeException("Ticket Assign By User not found!"));
            ticket.setTicketAssignToUser(assignToUser);
            ticket.setTicketAssignedBy(assignByUser);
            ticket.setTicketAssignTime(new Timestamp(System.currentTimeMillis()));
        }
    }

    @Transactional
    public TicketEditInfo getEditTicketInfo(Long ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(()->new RangeException("Ticket not found!"));
        TicketEditInfo ticketEditInfo = null;
        if (ticket.getTicketAssignedBy() != null){
            ticketEditInfo = new TicketEditInfo(ticket.getCategory().getName(), ticket.getCriticalLevel().getName(), ticket.getTitle(), ticket.getDescription(), null, ticket.getStatus().getName(), ticket.getDueDate(), ticket.getTicketAssignToUser().getId(), ticket.getTicketAssignedBy().getId());
        }else{
            ticketEditInfo = new TicketEditInfo(ticket.getCategory().getName(), ticket.getCriticalLevel().getName(), ticket.getTitle(), ticket.getDescription(), null, ticket.getStatus().getName(), ticket.getDueDate(), null, null);
        }
        return ticketEditInfo;
    }




}
