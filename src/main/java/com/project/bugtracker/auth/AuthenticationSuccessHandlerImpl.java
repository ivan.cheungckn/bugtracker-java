package com.project.bugtracker.auth;

import com.project.bugtracker.dao.UserRepository;
import com.project.bugtracker.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    @Autowired
    HttpSession session; //autowiring session
//    @Autowired
//    UserRepository repository; //autowire the user repo
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public AuthenticationSuccessHandlerImpl() {
    }

    private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        // TODO Auto-generated method stub
        long userId = -1;
        if(authentication.getPrincipal() instanceof Principal) {
//            userId = ((Principal)authentication.getPrincipal()).getName();

        }else {
            userId = ((MyUserDetail)authentication.getPrincipal()).getUserId();
        }
        logger.info("userId: " + userId);
        //HttpSession session = request.getSession();
        session.setAttribute("userId", userId);
        redirectStrategy.sendRedirect(request, response, "/");
    }

}