package com.project.bugtracker.auth;

import com.project.bugtracker.modal.RightMap;

public enum TicketEditRight {
    SENIOR_RIGHT(new RightMap(true, true, true, true, true, true, true, true, true)),
    OWNER_RIGHT(new RightMap(true, true, true, false, true, true, false, false, false)),
    BE_ASSIGNED_RIGHT(new RightMap(true, true, true, false, true, true, true, false, false)),
    DEVELOPER_RIGHT(new RightMap(true, true, true, false, true, true, false, false, false));

    private RightMap rightMap;

    TicketEditRight(RightMap rightMap) {
        this.rightMap = rightMap;
    }

    public RightMap getRightMap() {
        return rightMap;
    }

    public void setRightMap(RightMap rightMap) {
        this.rightMap = rightMap;
    }
}
