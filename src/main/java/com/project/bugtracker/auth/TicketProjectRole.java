package com.project.bugtracker.auth;

public enum TicketProjectRole {
    JUNIOR("junior developer"),
    OWNER("owner"),
    SENIOR("senior developer"),
    PM("project manager");

    private String role;

    TicketProjectRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
