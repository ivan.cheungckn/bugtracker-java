package com.project.bugtracker.auth;

public enum  ApplicationUserRole {
    ADMIN,
    MANAGER,
    STAFF
}
