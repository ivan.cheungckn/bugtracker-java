package com.project.bugtracker.service;

import com.project.bugtracker.dao.*;
import com.project.bugtracker.entity.Project;
import com.project.bugtracker.entity.ProjectUser;
import com.project.bugtracker.entity.User;
import com.project.bugtracker.exception.ProjectNotFoundException;
import com.project.bugtracker.exception.RangeException;
import com.project.bugtracker.modal.Member;
import com.project.bugtracker.modal.ProjectForm;
import com.project.bugtracker.modal.UpdateMemberForm;
import com.project.bugtracker.modal.UpdateProjectForm;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.context.jdbc.SqlConfig.TransactionMode.ISOLATED;

@SpringBootTest
//@ContextConfiguration("/applicationContext.xml")
//@EnableJpaRepositories(basePackageClasses = {UserRepository.class, ProjectRepository.class})
@Sql(scripts = {"/seed/ProjectInitialData.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,config = @SqlConfig(transactionMode = ISOLATED))
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD) // The spring application context will be considered "dirty" before each test method, and will be rebuilt. It means that
// your autowired beans will not carry any data between test cases.
class ProjectServiceTest {
    @Autowired
    ProjectService projectService;
    @Autowired
    ProjectUserRepository projectUserRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProjectStatusRepository projectStatusRepository;
    @Autowired
    CommentFileRepository commentFileRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    AttachedFileTicketRepository attachedFileTicketRepository;
    @Autowired
    TicketRepository ticketRepository;


    @Test
    @DisplayName("getProjectMemberByProjectId - normal")
    void getProjectMemberByProjectIdNormal() {
        List<User> actual = projectService.getProjectMemberByProjectId(1);
        User user1 = new User("testUserName", "testPassword", "abcName", null, null, null, null);
        User user2 = new User("number2", "number2", "i am 2", null, null, null, null);
        user1.setId(1);
        user2.setId(2);
        List<User> expected = new ArrayList<User>();
        expected.add(user1);
        expected.add(user2);
        assertEquals(expected.size(),actual.size());
        List<User> testUser1 = actual.stream().filter(user-> user.getId()==1).collect(Collectors.toList());
        assertEquals(1, testUser1.size());
        assertEquals(1, testUser1.get(0).getId());
        List<User> testUser2 = actual.stream().filter(user-> user.getId()==2).collect(Collectors.toList());
        assertEquals(1, testUser2.size());
        assertEquals(2, testUser2.get(0).getId());

    }

    @Test
    @DisplayName("getProjectMemberByProjectId - no member/no project id")
    void getProjectMemberByProjectIdNoProjectIdOrMember() {
        List<User> actual = projectService.getProjectMemberByProjectId(2);
        System.out.println(actual);
        assertTrue(0 == actual.size());
    }

    @Test
    @DisplayName("addMember - normal")
    void addMemberNormal(){
        long projectId = 2;
        long Id = projectService.addMember(2, projectId, 3);
        assertEquals(3, Id);
        List<User> users = projectService.getMembersByProjectId(projectId);
        User actualUser = users.stream().filter(user->user.getId()==2).findFirst().get();
        assertEquals(2, actualUser.getId());
    }

    @Test
    @DisplayName("addMember - duplicate member in the same project")
    void addMemberDuplicatedMember(){
        assertThrows(DataIntegrityViolationException.class, ()->projectService.addMember(2, 1, 2));
    }

    @Test
    @DisplayName("addMember - invalid user")
    void addMemberInvalidUser(){
        assertThrows(RangeException.class, ()->projectService.addMember(10, 2, 2));
    }

    @Test
    @DisplayName("addMember - invalid role")
    void addMemberInvalidRole(){
        assertThrows(RangeException.class, ()->projectService.addMember(2, 2, 10));
    }

    @Test
    @DisplayName("addMember - invalid project")
    void addMemberInvalidProject(){
        assertThrows(RangeException.class, ()->projectService.addMember(2, 10, 2));
    }

    @Test
    @DisplayName("remove Member - normal")
    void removeMemberNormal(){
        List<User> users = projectService.getMembersByProjectId(1);
        User filterUser = users.stream().filter(u->u.getId()==2).findFirst().get();
        Member member = new Member(1, filterUser.getId());
        List<Member> members = new ArrayList<Member>();
        members.add(member);
        assertTrue(users.size()==2);
        projectService.removeMember(members);
        List<User> afterUsers = projectService.getMembersByProjectId(1);
        assertTrue(afterUsers.size()==1);
    }

    @Test
    @DisplayName("remove Member - Project or Member does not exist in database")
    void removeMemberProjectorMemberNotInDB(){
        Member member = new Member(100, 2);
        List<Member> members = new ArrayList<Member>();
        members.add(member);
        assertThrows(RangeException.class,()->projectService.removeMember(members));
        Project project = projectService.getProjectById(2);
        assertTrue(project.getId()==2);
    }

    @Test
    @DisplayName("update Member role - normal")
    void updateMemberRole(){
        UpdateMemberForm updateDataForm = new UpdateMemberForm(2,3,1);

        List<UpdateMemberForm> forms = new ArrayList<UpdateMemberForm>();
        forms.add(updateDataForm);
        projectService.updateMemberRole(forms);
        List<User> users = projectService.getMembersByProjectId(1);
        Project project = projectService.getProjectById(1);
        User user = users.stream().filter(u->u.getId()==2).findFirst().get();
        ProjectUser projectUser = projectUserRepository.findByUserAndProject(user, project).orElseThrow(()->new RangeException("Project/User are not found in database"));
        assertTrue(2==projectUser.getUser().getId());
        assertTrue(3==projectUser.getRole().getId());
        assertTrue(1==projectUser.getProject().getId());
    }

    @Test
    @DisplayName("update Member role - Project/Project Member are not found in database")
    void updateMemberRoleNoProjectorProjectMemberInDB(){
        UpdateMemberForm updateDataForm = new UpdateMemberForm(20,3,1);
        UpdateMemberForm updateDataForm2 = new UpdateMemberForm(1,1,20);
        List<UpdateMemberForm> forms = new ArrayList<UpdateMemberForm>();
        List<UpdateMemberForm> forms2 = new ArrayList<UpdateMemberForm>();
        forms.add(updateDataForm);
        forms2.add(updateDataForm2);
//        forms.add(updateDataForm);
//        projectService.updateMemberRole(forms);
//        List<User> users = projectService.getMembersByProjectId(1);
//        Project project = projectService.getProjectById(1);
//        User user = users.stream().filter(u->u.getId()==2).findFirst().get();
//        ProjectUser projectUser = projectUserRepository.findByUserAndProject(user, project);
//        assertTrue(2==projectUser.getUser().getId());
//        assertTrue(3==projectUser.getRole().getId());
//        assertTrue(1==projectUser.getProject().getId());
        assertThrows(RangeException.class, ()->projectService.updateMemberRole(forms));
        assertThrows(RangeException.class, ()->projectService.updateMemberRole(forms2));
    }

    @Test
    @DisplayName("update Member role - invalid role")
    void updateMemberRoleInvalidRole(){
        UpdateMemberForm updateDataForm = new UpdateMemberForm(2,10,1);
        List<UpdateMemberForm> forms = new ArrayList<UpdateMemberForm>();
        forms.add(updateDataForm);
        assertThrows(RangeException.class, ()->projectService.updateMemberRole(forms));
    }

    @Test
    @DisplayName("get project info by project id - normal")
    void getProjectInfoByProjectId(){
        Project project = projectService.getProjectById(1);
        assertEquals(1,project.getId());
        assertEquals("abcName",project.getUser().getName());
        assertEquals("testPJNAME",project.getProjectName());
        assertEquals("TPD",project.getDescription());
        assertEquals("development",project.getProjectStatus().getName());
        assertEquals(3,project.getTickets().size());
        assertEquals(2,project.getMembers().size());
    }

    @Test
    @DisplayName("get project info by project id - project not exist")
    void getProjectInfoByProjectIdProjectNotExist(){
        assertThrows(ProjectNotFoundException.class,()->projectService.getProjectById(100));
    }

    @Test
    @DisplayName("create project - normal")
    void createProjectNormal(){
        long statusId = projectStatusRepository.findByName("development").get().getId();
        ProjectForm projectForm = new ProjectForm(1, "testing 2", "testing2 description", statusId );
        projectService.createProject(projectForm);
        Project project = projectService.getProjectById(3);
        assertEquals(3, project.getId());
        assertEquals("testing 2", project.getProjectName());
        assertEquals("testing2 description", project.getDescription());
        assertEquals(statusId, project.getProjectStatus().getId());
    }

    @Test
    @DisplayName("create project - empty project")
    void createProjectEmptyProjectName(){
        long statusId = projectStatusRepository.findByName("development").get().getId();
        ProjectForm projectForm = new ProjectForm(1, "", "testing2 description", statusId );
        assertThrows(RangeException.class, ()->projectService.createProject(projectForm));

    }

    @Test
    @DisplayName("create project - empty description")
    void createProjectEmptyDescription(){
        long statusId = projectStatusRepository.findByName("development").get().getId();
        ProjectForm projectForm = new ProjectForm(1, "testing 2", "", statusId );
        assertThrows(RangeException.class, ()->projectService.createProject(projectForm));

    }

    @Test
    @DisplayName("update project - normal")
    void updateProjectNormal(){
        long projectId = 2;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        UpdateProjectForm updateProjectForm = new UpdateProjectForm(projectId, "update project name", "update project description", statusId);
        boolean result =  projectService.updateProject(updateProjectForm);
        assertTrue(result);
        Project project = projectService.getProjectById(projectId);
        assertEquals("update project name", project.getProjectName());
        assertEquals("update project description", project.getDescription());
        assertEquals("development", project.getProjectStatus().getName());
    }

    @Test
    @DisplayName("update project - project does not exist in database")
    void updateProjectNotInDB(){
        long projectId = 10;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        UpdateProjectForm updateProjectForm = new UpdateProjectForm(projectId, "update project name", "update project description", statusId);
        assertThrows(RangeException.class, ()->projectService.updateProject(updateProjectForm));
    }

    @Test
    @DisplayName("update project - blank project name")
    void updateProjectBlankProjectName(){
        long projectId = 2;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        UpdateProjectForm updateProjectForm = new UpdateProjectForm(projectId, "", "update project description", statusId);
        assertThrows(RangeException.class, ()->projectService.updateProject(updateProjectForm));
    }

    @Test
    @DisplayName("update project - blank description")
    void updateProjectBlankDescription(){
        long projectId = 2;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        UpdateProjectForm updateProjectForm = new UpdateProjectForm(projectId, "update project name", "", statusId);
        assertThrows(RangeException.class, ()->projectService.updateProject(updateProjectForm));
    }

    @Test
    @DisplayName("update project - duplicate project name")
    void updateProjectDuplicateProjectName(){
        long projectId = 2;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        UpdateProjectForm updateProjectForm = new UpdateProjectForm(projectId, "testPJNAME", "testing", statusId);
        assertThrows(DataIntegrityViolationException.class, ()->projectService.updateProject(updateProjectForm));
    }

    @Test
    @DisplayName("delete project - normal")
    void deleteProjectNormal(){
        long projectId = 1;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        boolean result = projectService.deleteProject(projectId);
        assertTrue(result);
        assertEquals(2, commentFileRepository.findAll().size());
        assertEquals(2, commentRepository.findAll().size());
        assertEquals(2, attachedFileTicketRepository.findAll().size());
        assertEquals(2, ticketRepository.findAll().size());
        assertEquals(0, projectUserRepository.findAll().size());
    }

    @Test
    @DisplayName("delete project - project not exist")
    void deleteProjectNotExist(){
        long projectId = 99;
        long statusId = projectStatusRepository.findByName("development").get().getId();
        assertThrows(EmptyResultDataAccessException.class, ()->projectService.deleteProject(projectId));
    }

}